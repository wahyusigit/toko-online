<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function(){
	dd(Request::server('HTTP_REFERER'));
	dd(URL::previous());
});

Route::get('/',['uses'=>'HomepageController@index'])->name('indexHomepage');

Route::prefix('/produk')->group(function(){
	Route::get('/', 'HomepageController@produk')->name('indexProdukHomepage');
	Route::get('/{id_jenis}/all', 'HomepageController@produkCategory')->name('indexProdukCategoryHomepage');
	Route::get('/detail/{id_produk}', 'HomepageController@produkDetail')->name('indexProdukDetailHomepage');
});
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tentang-kami', 'HomepageController@tentangKami')->name('tentangKami');
Route::get('/cara-pemesanan', 'HomepageController@caraPemesanan')->name('caraPemesanan');
Route::get('/cara-pembayaran', 'HomepageController@caraPembayaran')->name('caraPembayaran');
Route::get('/hubungi-kami', 'HomepageController@hubungiKami')->name('hubungiKami');

Route::get('/checkout', ['uses'=>'CartController@checkout'])->name('checkout');
Route::get('/completeCheckout', ['uses'=>'CartController@completeCheckout'])->name('completeCheckout');
Route::post('/completeCheckout', ['uses'=>'CartController@postCompleteCheckout'])->name('postCompleteCheckout');
Route::get('/ajaxAlamatSama', ['uses'=>'CartController@ajaxAlamatSama'])->name('ajaxAlamatSama');

Auth::routes();
// Route::get('/register', function(){
// 	return redirect(route('indexHomepage'));
// });

Route::get('/logout',function(){
	Auth::logout();
	return redirect(route('indexHomepage'));
})->name('logout');

Route::group(['prefix'=>'keranjang'], function(){
	Route::get('/',['uses'=>'CartController@index'])->name('indexCart');
	Route::get('/add/{id_produk}',['uses'=>'CartController@add'])->name('addCart');
	Route::get('/remove/{rowId}',['uses'=>'CartController@remove'])->name('removeCart');
	Route::get('/destroy',['uses'=>'CartController@destroy'])->name('destroyCart');
});

Route::group(['prefix'=>'toko', 'middleware' => ['auth','role:toko']], function(){
	Route::get('/',['uses'=>'Toko\TokoController@index'])->name('indexToko');
	Route::get('/profile',['uses'=>'Toko\TokoController@profile'])->name('profileToko');
	Route::post('/profile',['uses'=>'Toko\TokoController@updateProfile'])->name('updateProfileToko');

	Route::group(['prefix'=>'produk'], function(){
		Route::get('/', ['uses'=>'Toko\ProdukController@index'])->name('indexProdukToko');
		Route::get('/show/{id_produk}', ['uses'=>'Toko\ProdukController@show'])->name('showProdukToko');
		Route::get('/add', ['uses'=>'Toko\ProdukController@add'])->name('addProdukToko');
		Route::post('/add', ['uses'=>'Toko\ProdukController@postAdd'])->name('postAddProdukToko');
		Route::get('/edit/{id}', ['uses'=>'Toko\ProdukController@edit'])->name('editProdukToko');
		Route::post('/update/{id}', ['uses'=>'Toko\ProdukController@update'])->name('updateProdukToko');
		Route::get('/delete/{id}', ['uses'=>'Toko\ProdukController@delete'])->name('deleteProdukToko');
		// Route::group(['prefix'=>'jenis'], function(){
		// 	Route::get('/', ['uses'=>'Toko\ProdukJenisController@index'])->name('indexProdukJenisToko');
		// 	Route::get('/add', ['uses'=>'Toko\ProdukJenisController@add'])->name('addProdukJenisToko');
		// 	Route::post('/add', ['uses'=>'Toko\ProdukJenisController@postAdd'])->name('postAddProdukJenisToko');
		// 	Route::get('/edit/{id}', ['uses'=>'Toko\ProdukJenisController@edit'])->name('editProdukJenisToko');
		// 	Route::post('/update/{id}', ['uses'=>'Toko\ProdukJenisController@update'])->name('updateProdukJenisToko');
		// 	Route::get('/delete/{id}', ['uses'=>'Toko\ProdukJenisController@delete'])->name('deleteProdukJenisToko');
		// });
	});

	Route::group(['prefix'=>'pesanan'], function(){
		Route::get('/',['uses'=>'Toko\PesananController@index'])->name('indexPesananToko');
		Route::get('/detail/{id_pesanan_toko}',['uses'=>'Toko\PesananController@detail'])->name('detailPesananToko');
	});

	Route::group(['prefix'=>'pengiriman'], function(){
		Route::get('/',['uses'=>'Toko\PengirimanController@index'])->name('indexPengirimanToko');
		// Route::get('/add/{id_pesanan_toko}',['uses'=>'Toko\PengirimanController@add'])->name('addPengirimanToko');
		Route::post('/add/{id_pesanan_toko}',['uses'=>'Toko\PengirimanController@postAdd'])->name('postAddPengirimanToko');
		Route::get('/detail/{id_kirim}',['uses'=>'Toko\PengirimanController@detail'])->name('detailPengirimanToko');
		Route::get('/edit/{id_kirim}',['uses'=>'Toko\PengirimanController@edit'])->name('editPengirimanToko');
		Route::post('/update/{id_kirim}',['uses'=>'Toko\PengirimanController@update'])->name('updatePengirimanToko');
		// Route::get('/delete/{id_kirim}',['uses'=>'Toko\PengirimanController@update'])->name('updatePengirimanToko');
	});
});

Route::group(['prefix'=>'customer', 'middleware' => ['auth','role:customer']], function(){
	Route::get('/',['uses'=>'CustomerController@index'])->name('indexCustomer');
	Route::get('/profile',['uses'=>'CustomerController@profile'])->name('profileCustomer');
	Route::post('/profile',['uses'=>'CustomerController@updateProfile'])->name('updateProfileCustomer');

	Route::group(['prefix'=>'pesanan'], function(){
		Route::get('/',['uses'=>'Customer\PesananController@index'])->name('indexPesananCustomer');
		Route::get('/delete/{id_pesan}',['uses'=>'Customer\PesananController@delete'])->name('deletePesananCustomer');
		Route::get('/detail/{id_pesan}',['uses'=>'Customer\PesananController@detail'])->name('pesananDetailCustomer');
		Route::get('/detail/delete/{id_pesan}/{id_det}',['uses'=>'Customer\PesananController@deleteDetail'])->name('deletePesananDetailCustomer');
		Route::get('/konfirmasi/{id_pesan}','Customer\PesananController@konfirmasi')->name('konfirmasiCustomer');
		Route::post('/konfirmasi/{id_pesan}','Customer\PesananController@postKonfirmasi')->name('postKonfirmasiCustomer');
	});

	Route::group(['prefix'=>'pengiriman'], function(){
		Route::get('/',['uses'=>'Customer\PengirimanController@index'])->name('indexPengirimanCustomer');
		// Route::get('/detail/{id_kirim}',['uses'=>'Customer\PengirimanController@detail'])->name('detailPengirimanCustomer');
		Route::get('/konfirmasi/{id_det}',['uses'=>'Customer\PengirimanController@konfirmasi'])->name('konfirmasiPenerimaanCustomer');
	});

	Route::group(['prefix'=>'testimoni'], function(){
		Route::get('/', ['uses'=>'Customer\TestimoniController@index'])->name('indexTestimoniCustomer');
		Route::get('/add', ['uses'=>'Customer\TestimoniController@add'])->name('addTestimoniCustomer');
		Route::get('/add2/{id_det}', ['uses'=>'Customer\TestimoniController@add2'])->name('add2TestimoniCustomer');
		Route::post('/add2/{id_det}', ['uses'=>'Customer\TestimoniController@postAdd2'])->name('postAdd2TestimoniCustomer');
		Route::get('show/{id}', ['uses'=>'Customer\TestimoniController@show'])->name('showTestimoniCustomer');
		Route::get('edit/{id}', ['uses'=>'Customer\TestimoniController@edit'])->name('editTestimoniCustomer');
		Route::post('update/{id}', ['uses'=>'Customer\TestimoniController@update'])->name('updateTestimoniCustomer');
		Route::get('delete/{id}', ['uses'=>'TestimoniController@delete'])->name('deleteTestimoniCustomer');	
	});
});



Route::group(['prefix'=>'admin', 'middleware' => ['auth','role:admin']], function(){
	Route::get('/', ['uses'=>'Admin\AdminController@index'])->name('indexAdmin');

	Route::group(['prefix'=>'slider'], function(){
		Route::get('/', ['uses'=>'SliderController@index'])->name('indexSliderAdmin');
		Route::get('add', ['uses'=>'SliderController@add'])->name('addSliderAdmin');
		Route::post('add', ['uses'=>'SliderController@postAdd'])->name('postAddSliderAdmin');
		Route::get('edit/{id}', ['uses'=>'SliderController@edit'])->name('editSliderAdmin');
		Route::post('update/{id}', ['uses'=>'SliderController@update'])->name('updateSliderAdmin');
		Route::get('delete/{id}', ['uses'=>'SliderController@delete'])->name('deleteSliderAdmin');	
	});

	Route::group(['prefix'=>'halaman'], function(){
		Route::get('/{slug?}', ['uses'=>'Admin\HalamanController@index'])->name('indexHalamanAdmin');
		Route::post('/{id_halaman}', ['uses'=>'Admin\HalamanController@updateHalaman'])->name('updateHalamanAdmin');
	});

	Route::group(['prefix'=>'data'], function(){
		Route::get('/{slug}', ['uses'=>'UserController@index'])->name('indexUserAdmin');
		Route::get('{slug}/add', ['uses'=>'UserController@add'])->name('addUserAdmin');
		Route::post('{slug}/add', ['uses'=>'UserController@postAdd'])->name('postAddUserAdmin');
		Route::get('{slug}/edit/{id}', ['uses'=>'UserController@edit'])->name('editUserAdmin');
		Route::post('{slug}/update/{id}', ['uses'=>'UserController@update'])->name('updateUserAdmin');
		Route::get('{slug}/delete/{id}', ['uses'=>'UserController@delete'])->name('deleteUserAdmin');	
	});

	// Route::group(['prefix'=>'data-admin'], function(){
	// 	Route::get('/', ['uses'=>'Admin\DataAdminController@index'])->name('indexDataAdmin');
	// 	Route::get('add', ['uses'=>'Admin\DataAdminController@add'])->name('addDataAdmin');
	// 	Route::post('add', ['uses'=>'Admin\DataAdminController@postAdd'])->name('postAddDataAdmin');
	// 	Route::get('edit/{id}', ['uses'=>'Admin\DataAdminController@edit'])->name('editDataAdmin');
	// 	Route::post('update/{id}', ['uses'=>'Admin\DataAdminController@update'])->name('updateDataAdmin');
	// 	Route::get('delete/{id}', ['uses'=>'Admin\DataAdminController@delete'])->name('deleteDataAdmin');	
	// });

	// Route::group(['prefix'=>'data-toko'], function(){
	// 	Route::get('/', ['uses'=>'Admin\DataTokoController@index'])->name('indexDataTokoAdmin');
	// 	Route::get('add', ['uses'=>'Admin\DataTokoController@add'])->name('addDataTokoAdmin');
	// 	Route::post('add', ['uses'=>'Admin\DataTokoController@postAdd'])->name('postAddDataTokoAdmin');
	// 	Route::get('edit/{id}', ['uses'=>'Admin\DataTokoController@edit'])->name('editDataTokoAdmin');
	// 	Route::post('update/{id}', ['uses'=>'Admin\DataTokoController@update'])->name('updateDataTokoAdmin');
	// 	Route::get('delete/{id}', ['uses'=>'Admin\DataTokoController@delete'])->name('deleteDataTokoAdmin');	
	// });
	
	// Route::group(['prefix'=>'data-customer'], function(){
	// 	Route::get('/', ['uses'=>'Admin\DataCustomerController@index'])->name('indexDataCustomerAdmin');
	// 	Route::get('add', ['uses'=>'Admin\DataCustomerController@add'])->name('addDataCustomerAdmin');
	// 	Route::post('add', ['uses'=>'Admin\DataCustomerController@postAdd'])->name('postAddDataCustomerAdmin');
	// 	Route::get('edit/{id}', ['uses'=>'Admin\DataCustomerController@edit'])->name('editDataCustomerAdmin');
	// 	Route::post('update/{id}', ['uses'=>'Admin\DataCustomerController@update'])->name('updateDataCustomerAdmin');
	// 	Route::get('delete/{id}', ['uses'=>'Admin\DataCustomerController@delete'])->name('deleteDataCustomerAdmin');	
	// });

	Route::group(['prefix'=>'produk'], function(){
		Route::get('/', ['uses'=>'Admin\ProdukController@index'])->name('indexProdukAdmin');
		Route::get('/show/{id_produk}', ['uses'=>'Admin\ProdukController@show'])->name('showProdukAdmin');
		Route::get('/add', ['uses'=>'Admin\ProdukController@add'])->name('addProdukAdmin');
		Route::post('/add', ['uses'=>'Admin\ProdukController@postAdd'])->name('postAddProdukAdmin');
		Route::get('/edit/{id}', ['uses'=>'Admin\ProdukController@edit'])->name('editProdukAdmin');
		Route::post('/update/{id}', ['uses'=>'Admin\ProdukController@update'])->name('updateProdukAdmin');
		Route::get('/delete/{id}', ['uses'=>'Admin\ProdukController@delete'])->name('deleteProdukAdmin');
		Route::group(['prefix'=>'jenis'], function(){
			Route::get('/', ['uses'=>'Admin\ProdukJenisController@index'])->name('indexProdukJenisAdmin');
			Route::get('/add', ['uses'=>'Admin\ProdukJenisController@add'])->name('addProdukJenisAdmin');
			Route::post('/add', ['uses'=>'Admin\ProdukJenisController@postAdd'])->name('postAddProdukJenisAdmin');
			Route::get('/edit/{id}', ['uses'=>'Admin\ProdukJenisController@edit'])->name('editProdukJenisAdmin');
			Route::post('/update/{id}', ['uses'=>'Admin\ProdukJenisController@update'])->name('updateProdukJenisAdmin');
			Route::get('/delete/{id}', ['uses'=>'Admin\ProdukJenisController@delete'])->name('deleteProdukJenisAdmin');
		});
	});
	
	Route::group(['prefix'=>'jarak'], function(){
		Route::get('/', ['uses'=>'Admin\JarakController@index'])->name('indexJarakAdmin');
		Route::get('add', ['uses'=>'Admin\JarakController@add'])->name('addJarakAdmin');
		Route::post('add', ['uses'=>'Admin\JarakController@postAdd'])->name('postAddJarakAdmin');
		Route::get('edit/{id}', ['uses'=>'Admin\JarakController@edit'])->name('editJarakAdmin');
		Route::post('update/{id}', ['uses'=>'Admin\JarakController@update'])->name('updateJarakAdmin');
		Route::get('delete/{id}', ['uses'=>'Admin\JarakController@delete'])->name('deleteJarakAdmin');	
	});

	Route::group(['prefix'=>'pesan'], function(){
		Route::get('/', ['uses'=>'Admin\PesanController@index'])->name('indexPesanAdmin');

		Route::group(['prefix'=>'pesanan'], function(){
			Route::get('/',['uses'=>'Admin\PesananController@index'])->name('indexPesananAdmin');
			Route::get('/delete/{id_pesan}',['uses'=>'Admin\PesananController@delete'])->name('deletePesananAdmin');
			Route::get('/detail/{id_pesan}',['uses'=>'Admin\PesananController@detail'])->name('pesananDetailAdmin');
			Route::get('/detail/delete/{id_pesan}/{id_det}',['uses'=>'Admin\PesananController@deleteDetail'])->name('deletePesananDetailAdmin');
		});

		// Route::group(['prefix'=>'konfirmasi'], function(){
		// 	Route::get('/',['uses'=>'Admin\KonfirmasiController@index'])->name('indexKonfirmasiAdmin');
			// Route::get('/verifikasi/{id_pesan}',['uses'=>'Admin\KonfirmasiController@verifikasiPembayaran'])->name('verifikasiPembayaranAdmin');
		// 	Route::post('/verifikasi/{id_pesan}',['uses'=>'Admin\KonfirmasiController@postVerifikasiPembayaran'])->name('postVerifikasiPembayaranAdmin');
		// });
		
		Route::group(['prefix'=>'pembayaran'], function(){
			Route::get('/', ['uses'=>'Admin\PembayaranController@index'])->name('indexPembayaranAdmin');
			Route::get('show/{id_bayar}', ['uses'=>'Admin\PembayaranController@show'])->name('showPembayaranAdmin');	
			Route::get('edit/{id_bayar}', ['uses'=>'Admin\PembayaranController@edit'])->name('editPembayaranAdmin');
			Route::post('update/{id_bayar}', ['uses'=>'Admin\PembayaranController@update'])->name('updatePembayaranAdmin');
			Route::get('delete/{id_bayar}', ['uses'=>'Admin\PembayaranController@delete'])->name('deletePembayaranAdmin');	

			Route::get('/verifikasi/{id_pesan}',['uses'=>'Admin\PembayaranController@verifikasiPembayaran'])->name('verifikasiPembayaranAdmin');
			Route::post('/verifikasi/{id_pesan}',['uses'=>'Admin\PembayaranController@postVerifikasiPembayaran'])->name('postVerifikasiPembayaranAdmin');
		// });
		});

		Route::group(['prefix'=>'pengiriman'], function(){
			Route::get('/', ['uses'=>'Admin\PengirimanController@index'])->name('indexPengirimanAdmin');
			// Route::get('/detail/{id_kirim}', ['uses'=>'Admin\PengirimanController@detail'])->name('detailPengirimanAdmin');
			// Route::get('add', ['uses'=>'Admin\PengirimanController@add'])->name('addPengirimanAdmin');
			// Route::get('add/kirim/{id_pesan}', ['uses'=>'Admin\PengirimanController@addKirim'])->name('addKirimPengirimanAdmin');
			// Route::post('add/kirim/{id_bayar}', ['uses'=>'Admin\PengirimanController@postAddKirim'])->name('postAddKirimPengirimanAdmin');
			// Route::get('edit/{id_kirim}', ['uses'=>'Admin\PengirimanController@edit'])->name('editPengirimanAdmin');
			// Route::post('update/{id_kirim}', ['uses'=>'Admin\PengirimanController@update'])->name('updatePengirimanAdmin');
		});
	});

	Route::group(['prefix'=>'testimoni'], function(){
		Route::get('/', ['uses'=>'Admin\TestimoniController@index'])->name('indexTestimoniAdmin');
		Route::get('show/{id}', ['uses'=>'Admin\TestimoniController@show'])->name('showTestimoniAdmin');
		Route::get('edit/{id}', ['uses'=>'Admin\TestimoniController@edit'])->name('editTestimoniAdmin');
		Route::post('update/{id}', ['uses'=>'Admin\TestimoniController@update'])->name('updateTestimoniAdmin');
		Route::get('delete/{id}', ['uses'=>'Admin\TestimoniController@delete'])->name('deleteTestimoniAdmin');	
	});
});


