Route::group(['prefix'=>'produk'], function(){
		Route::get('/', ['uses'=>'Toko\ProdukController@index'])->name('indexProdukAdmin');
		Route::get('/show/{id_produk}', ['uses'=>'Toko\ProdukController@show'])->name('showProdukAdmin');
		Route::get('/add', ['uses'=>'Toko\ProdukController@add'])->name('addProdukAdmin');
		Route::post('/add', ['uses'=>'Toko\ProdukController@postAdd'])->name('postAddProdukAdmin');
		Route::get('/edit/{id}', ['uses'=>'Toko\ProdukController@edit'])->name('editProdukAdmin');
		Route::post('/update/{id}', ['uses'=>'Toko\ProdukController@update'])->name('updateProdukAdmin');
		Route::get('/delete/{id}', ['uses'=>'Toko\ProdukController@delete'])->name('deleteProdukAdmin');
		Route::group(['prefix'=>'jenis'], function(){
			Route::get('/', ['uses'=>'Toko\ProdukJenisController@index'])->name('indexProdukJenisAdmin');
			Route::get('/add', ['uses'=>'Toko\ProdukJenisController@add'])->name('addProdukJenisAdmin');
			Route::post('/add', ['uses'=>'Toko\ProdukJenisController@postAdd'])->name('postAddProdukJenisAdmin');
			Route::get('/edit/{id}', ['uses'=>'Toko\ProdukJenisController@edit'])->name('editProdukJenisAdmin');
			Route::post('/update/{id}', ['uses'=>'Toko\ProdukJenisController@update'])->name('updateProdukJenisAdmin');
			Route::get('/delete/{id}', ['uses'=>'Toko\ProdukJenisController@delete'])->name('deleteProdukJenisAdmin');
		});
	});