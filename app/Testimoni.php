<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimoni extends Model
{
	protected $primaryKey = 'id_testimoni';

    public function produk(){
    	return $this->belongsTo(Produk::class,'id_produk','id_produk');
    }

    public function customer(){
    	return $this->hasOne(User::class,'id','id_customer');
    }
}
