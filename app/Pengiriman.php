<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
	public $incrementing = false;
    protected $primaryKey = 'id_kirim';

    public function pembayaran(){
    	return $this->hasOne(Pembayaran::class,'id_bayar','id_bayar');
    }

    public function pesanan(){
    	return $this->hasOne(Pesanan::class,'id_pesan','id_pesan');
    }

    public function pesanantoko(){
    	return $this->hasOne(PesananToko::class,'id_pesan','id_pesan');
    }

    public function detail(){
        return $this->hasMany(PengirimanDetail::class,'id_kirim','id_kirim');
    }
}
