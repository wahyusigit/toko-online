<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukJenis extends Model
{
	protected $primaryKey = 'id_jenis';

    public function produk(){
    	return $this->hasMany(Produk::class,'id_jenis','id_jenis');
    }
}
