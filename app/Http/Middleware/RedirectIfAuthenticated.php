<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->hasRole('customer')) {
                return redirect(route('indexAdmin'));
                // return $next($request);
            } else if (Auth::user()->hasRole('toko')) {
                return redirect(route('indexToko'));
                // return $next($request);
            } else if (Auth::user()->hasRole('admin')) {
                return redirect(route('indexCustomer'));
                // return $next($request);
            } else {
                return $next($request);
            }
        }

        return $next($request);
    }
}
