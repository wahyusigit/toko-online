<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next)
    // {
    //     return $next($request);
    // }

    public function handle($request, Closure $next, $role)
    {
        if (! $request->user()->hasRole($role)) {
            if (Auth::user()->hasRole('customer')) {
                return redirect(route('indexCustomer'));
            } else if (Auth::user()->hasRole('toko')) {
                return redirect(route('indexToko'));
            } else if (Auth::user()->hasRole('admin')) {
                return redirect(route('indexAdmin'));
            } else {
                return redirect(route('logout'));
            }
        }
        return $next($request);
    }
}
