<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;
use App\User;

class UserController extends Controller
{
    public function index($slug){
        $users = Role::where('name',$slug)->first()->user()->paginate(10);
    	return view('pages.admin.user.index', compact('slug','users'));
    }

    public function add($slug){
    	return view('pages.admin.user.add',compact('slug'));
    }

    public function postAdd(Request $req, $slug){
        $role_id = Role::where('name',$slug)->first()->id;

    	$user = new User();
        $user->role_id = $role_id;
        $user->username = $req->username;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = bcrypt($req->password);
        $user->save();
        
        flash('Data Admin Berhasil Ditambahkan...')->success();

        return redirect(route('indexUserAdmin', $slug));
    }

    public function edit($slug, $id){
    	$user = User::find($id);
    	return view('pages.admin.user.edit', compact('slug','user'));
    }

    public function update($slug, Request $req, $id){
        $user = User::find($req->id);
        $user -> username = $req->username;
        $user -> name = $req->name;
        $user -> email = $req->email;

        if (is_null($req->password) === false) {
            $user -> password = bcrypt($req->password);   
        }

        if($user -> save()){
            flash('Data Admin Berhasil Diubah...')->success();
        } else {
            flash('Data Admin Tidak Berhasil Diubah...')->error();
        }

        return redirect(route('indexUserAdmin', $slug));
    }

    public function delete($slug, $id){
        if(User::find($id)->delete()){
            flash('Data Admin Berhasil Dihapus...')->success();
        } else {
            flash('Data Admin Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexUserAdmin', $slug));
    }

    public function gantiPassword($slug, Request $req){
    	$user = User::find($req->id);
    	$user -> password = Hash::make($req->password);
    	$user -> save();
    }
}
