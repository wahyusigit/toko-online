<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slider;
use App\Produk;
use App\ProdukJenis;
use App\Halaman;
use App\Testimoni;

class HomepageController extends Controller
{
    public function index(){
    	$sliders = Slider::all();
    	$produks = Produk::all()->take(12);
        $produk_jenis = ProdukJenis::all();
    	return view('homepage', compact('sliders','produks','produk_jenis'));
    }

    public function tentangKami(){
    	$halaman = Halaman::where('slug', 'tentang-kami')->first();
        $produk_jenis = ProdukJenis::all();
    	return view('halaman', compact('halaman','produk_jenis'));
    }

    public function caraPemesanan(){
    	$halaman = Halaman::where('slug', 'cara-pemesanan')->first();
        $produk_jenis = ProdukJenis::all();
    	return view('halaman', compact('halaman','produk_jenis'));
    }

    public function caraPembayaran(){
    	$halaman = Halaman::where('slug', 'cara-pembayaran')->first();
        $produk_jenis = ProdukJenis::all();
    	return view('halaman', compact('halaman','produk_jenis'));
    }

    public function hubungiKami(){
    	$halaman = Halaman::where('slug', 'hubungi-kami')->first();
        $produk_jenis = ProdukJenis::all();
    	return view('halaman', compact('halaman','produk_jenis'));
    }

    public function produk(){
        $produks = Produk::all();
        $produk_jenis = ProdukJenis::all();
        return view('produk',compact('produk_jenis'));
    }

    public function produkCategory($id_jenis){
        $jenis = ProdukJenis::find($id_jenis);
        $produks = Produk::where('id_jenis',$id_jenis)->paginate(12);
        $produk_jenis = ProdukJenis::all();
        return view('produk_category',compact('produk_jenis','jenis','produks'));
    }

    public function produkDetail($id_produk){
        $produk = Produk::find($id_produk);
        $produk_jenis = ProdukJenis::all();
        $ulasans = Testimoni::where('id_produk',$id_produk)->get();
        $rekomendasi = Produk::where('id_toko',$produk->id_toko)->where('id_produk','!=',$produk->id_produk)->get()->take(4);
        return view('produk_detail',compact('produk','produk_jenis','ulasans','rekomendasi'));
    }
}
