<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slider;
use App\Produk;
use App\ProdukJenis;
use Image;

class SliderController extends Controller
{
    public function index(){
        $sliders = Slider::paginate(10);
        return view('pages.admin.slider.index', compact('sliders'));
    }

    public function show($id_slider){
        $slider = Slider::find($id_slider);
        return view('pages.admin.slider.show', compact('slider'));
    }

    public function add(){
        $produks = Produk::all();
        $jenis = ProdukJenis::all();
        return view('pages.admin.slider.add', compact('produks','jenis'));
    }

    public function postAdd(Request $req){
        $file = $req->file('image_slider');
        $image_path = 'img/slider/' . str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName());
        $image = Image::make($file->getRealPath())->crop(1140, 400)->save($image_path);

        $slider = new Slider();
        $slider->id_produk = $req->id_produk;
        $slider->judul_slider = $req->judul_slider;
        $slider->deskripsi_slider = $req->deskripsi_slider;
        $slider->image_slider = $image_path;
        if ($slider->save()) {
            flash('Data Slider Berhasil Ditambahkan...')->success();
        } else {
            flash('Data Slider Tidak Berhasil Ditambahkan...')->error();
        }

        return redirect(route('indexSliderAdmin'));
    }

    public function edit($id){
        $produks = Produk::all();
        $jenis = ProdukJenis::all();
        $slider = Slider::find($id);
        return view('pages.admin.slider.edit', compact('slider','produks','jenis'));
    }

    public function update(Request $req, $id){
        if (is_null($req->image_slider) === false) {
            $file = $req->file('image_slider');
            $image_slider = $file->move('img/slider/' , str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName()));
        }

        $slider = Slider::find($id);
        $slider->id_produk = $req->id_produk;
        $slider->judul_slider = $req->judul_slider;
        $slider->deskripsi_slider = $req->deskripsi_slider;

        if (is_null($req->image_slider) === false) {
            $slider->image_slider = $image_slider;
        }

        if ($slider->save()) {
            flash('Data Slider Berhasil Ditambahkan...')->success();
        } else {
            flash('Data Slider Tidak Berhasil Ditambahkan...')->error();
        }

        return redirect(route('indexSliderAdmin'));
    }

    public function delete($id){
        if(Slider::find($id)->delete()){
            flash('Data Slider Berhasil Dihapus...')->success();
        } else {
            flash('Data Slider Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexSliderAdmin'));
    }   
}
