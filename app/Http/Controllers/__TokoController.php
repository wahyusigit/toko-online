<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Pesanan;
use App\PesananDetail;
use App\Konfirmasi;

use URL; 
use Auth;

class TokoController extends Controller
{
    public function index(){
    	return view('pages.toko.index');
    }

    public function profile(){
    	$toko = User::find(Auth::user()->id);
    	return view('pages.toko.profile', compact('toko'));
    }

    public function updateProfile(Request $req){
    	$toko = User::find(Auth::user()->id);
		$toko->name = $req->name;
		$toko->username = $req->username;
		$toko->email = $req->email;
		if (is_null($req->password) === false) {
            $toko -> password = bcrypt($req->password);   
        }
		$toko->alamat = $req->alamat;
		$toko->kota = $req->kota;
		$toko->provinsi = $req->provinsi;
		$toko->kode_pos = $req->kode_pos;
		$toko->telepon = $req->telepon;
		if ($toko->save()) {
			flash('Profile Berhasil Diubah...')->success();
		} else {
			flash('Profile Tidak Berhasil Diubah...')->error();
		}

		return redirect(route('profileToko'));
    }
}
