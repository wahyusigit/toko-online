<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Halaman;

use URL;

class HalamanController extends Controller
{
    public function index($slug = 'tentang-kami'){
    	$halaman = Halaman::where('slug',$slug)->first();
    	$halamans = Halaman::all();
    	return view('pages.admin.halaman.index', compact('halamans','halaman'));
    }

    public function updateHalaman(Request $req, $id_halaman){
    	$halaman = Halaman::find($id_halaman);
    	$halaman->konten = $req->konten;
    	$halaman->save();

    	return redirect(route('indexHalamanAdmin', $halaman->slug));
    }
}
