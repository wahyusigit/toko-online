<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pesanan;
use App\PesananDetail;
use App\Konfirmasi; 
use App\Pembayaran;

use Auth;
use URL;
use Carbon\Carbon;

class KonfirmasiController extends Controller
{
    public function index(){
      $belum_konfirmasis = Konfirmasi::where('status_verifikasi','belum')->paginate(10);
      $sudah_konfirmasis = Konfirmasi::where('status_verifikasi','sudah')->paginate(10);
    	return view('pages.admin.pesan.konfirmasi.index',compact('belum_konfirmasis','sudah_konfirmasis'));
    }

    public function verifikasiPembayaran($id_pesan){
      $konfirmasi = Konfirmasi::where('id_pesan',$id_pesan)->first();

      $pesanan = Pesanan::find($id_pesan);
      $pesanan_details = $pesanan->detail()->get();
      return view('pages.admin.pesan.konfirmasi.verifikasi',compact('pesanan','pesanan_details','konfirmasi'));
    }

    public function postVerifikasiPembayaran(Request $req, $id_pesan){
      $konfirmasi = Konfirmasi::where('id_pesan',$id_pesan)->first();
      $konfirmasi->status_verifikasi = 'sudah';
      $konfirmasi->save();

      $pesanan = Pesanan::find($id_pesan);
      $pesanan->status_bayar = 'sudah';
      $pesanan->save();

      $carbon = Carbon::now();

      if (is_null(Pembayaran::where('id_pesan',$id_pesan)->first())) {
        $number = Pembayaran::whereDate('created_at',date('Y-m-d'))->get()->count();
        $id_bayar = "BYR" . date('my-his') . ($number+1);
        $pembayaran = new Pembayaran();
        $pembayaran->id_bayar = $id_bayar;
      } else {
        $pembayaran = Pembayaran::where('id_pesan',$id_pesan)->first();
      }
      // $pembayaran = Pembayaran::firstOrNew(['id_pesan'=>$id_pesan]);
      $pembayaran->id_pesan = $id_pesan;
      $pembayaran->tanggal_bayar = $carbon;
      $pembayaran->no_bukti = $req->no_bukti;
      $pembayaran->jumlah_bayar = $req->jumlah_bayar;
      $pembayaran->status_kirim = 'belum';
      $pembayaran->save();

      return redirect(route('indexPembayaranAdmin'));
    }

  //   public function add(){
  //       $pembayarans = Pembayaran::paginate(10);
  //       return view('pages.admin.pesan.konfirmasi.add',compact('pembayarans'));
  //   }

  //   public function postAdd(Request $req){
  //       $pesanan = Pesanan::find($req->id_pesan);
  //       $pesanan->status_bayar = 'sudah';
  //       $pesanan->
  //       $konfirmasi = new Konfirmasi();
  //       $konfirmasi -> id_pesan = $req->id_pesan;
  //       $konfirmasi -> tanggal_konfirmasi = $req->tanggal_konfirmasi;
  //       $konfirmasi -> no_bukti = $req->no_bukti;
  //       $konfirmasi -> jumlah_bayar = $req->jumlah_bayar;
  //       $konfirmasi -> status_baca = $req->status_baca;
  //       $konfirmasi -> keterangan = $req->keterangan;
  //       $konfirmasi -> status_baca = '0';
  //       $konfirmasi -> save();

  //       return redirect(route('indexKonfirmasiAdmin'));
  //   }

  //   public function delete($id_konf){
  //       $konfirmasi = Konfirmasi::find($id_konf)->delete();
  //       return redirect(route('indexKonfirmasiAdmin'));   
  //   }

  //   public function konfirmasi($id_pesan){
  //   	$konfirmasi = Konfirmasi::where('id_pesan',$id_pesan)->first();
  //   	$pesanan = Pesanan::find($id_pesan);
  //   	$pesanan_details = $pesanan->detail()->get();
  //   	return view('pages.admin.pesan.konfirmasi.complete',compact('pesanan','pesanan_details','konfirmasi'));
  //   }

  //   public function postKonfirmasi(Request $req, $id_pesan){
  //   	$konfirmasi = new Konfirmasi();
		// $konfirmasi->id_pesan = $req->id_pesan;
		// $konfirmasi->tanggal_konfirmasi = $req->tanggal_konfirmasi;
		// $konfirmasi->no_bukti = $req->no_bukti;
		// $konfirmasi->jumlah_bayar = $req->jumlah_bayar;
		// $konfirmasi->status_baca = '0';
		// $konfirmasi->keterangan = $req->keterangan;
		// $konfirmasi->save();

		// return redirect(route('indexKonfirmasiCustomer'));
  //   }
}
