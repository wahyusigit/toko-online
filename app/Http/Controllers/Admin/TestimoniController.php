<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Testimoni;

class TestimoniController extends Controller
{
    public function index(){
    	$belum_dibaca = Testimoni::where('status_baca','belum')->count();
    	$testimonis = Testimoni::orderBy('created_at','desc')->paginate(10);
    	return view('pages.admin.testimoni.index', compact('testimonis','belum_dibaca'));
    }

    public function show($id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni);
    	$testimoni->status_baca = '1';
    	$testimoni->save();
    	
    	return view('pages.admin.testimoni.show', compact('testimoni'));
    }

    public function edit($id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni);
    	$testimoni->status_baca = '1';
    	$testimoni->save();

    	return view('pages.admin.testimoni.edit', compact('testimoni'));
    }

    public function update(Request $req, $id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni);
    	$testimoni->ulasan = $req->ulasan;
    	$testimoni->save();
    	return redirect(route('indexTestimoniAdmin'));
    }

    public function delete($id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni)->delete();
    	return redirect(route('indexTestimoniAdmin'));
    }
}
