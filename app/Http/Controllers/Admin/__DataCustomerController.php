<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Permission;

class DataCustomerController extends Controller
{
    public function index(){
    	$customer = Role::where('name','customer')->first()->user()->paginate(10);
    	return view('pages.admin.datacustomer.index', compact('customer'));
    }

    public function add(){
    	return view('pages.admin.datacustomer.add');
    }

    public function postAdd(Request $req){
        $role_id = Role::where('name','customer')->first()->id;

    	$customer = new User();
        $customer -> username = $req->username;
        $customer -> name = $req->name;
        $customer -> email = $req->email;
        $customer -> password = bcrypt($req->password);
        $customer -> save();
        $customer->roles()->attach($role_id);
        flash('Data Customer Berhasil Ditambahkan...')->success();

        return redirect(route('indexDataCustomerAdmin'));
    }

    public function edit($id){
    	$customer = User::find($id);
    	return view('pages.admin.datacustomer.edit', compact('customer'));
    }

    public function update(Request $req, $id){
        $customer = User::find($req->id);
        $customer -> username = $req->username;
        $customer -> name = $req->name;
        $customer -> email = $req->email;

        if (is_null($req->password) === false) {
            $customer -> password = bcrypt($req->password);   
        }

        if($customer -> save()){
            flash('Data Customer Berhasil Diubah...')->success();
        } else {
            flash('Data Customer Tidak Berhasil Diubah...')->error();
        }

        return redirect(route('indexDataCustomerAdmin'));
    }

    public function delete($id){
        if(User::find($id)->delete()){
            flash('Data Customer Berhasil Dihapus...')->success();
        } else {
            flash('Data Customer Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexDataCustomerAdmin'));
    }

    public function gantiPassword(Request $req){
    	$customer = User::find($req->id);
    	$customer -> password = Hash::make($req->password);
    	$customer -> save();
    }
}
