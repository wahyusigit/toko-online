<?php

namespace App\Http\Controllers\Admin; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pembayaran;
use App\Pesanan;
use App\PesananToko;
use App\PesananTokoDetail;

use Carbon\Carbon;
use Image;
use DB;

class PembayaranController extends Controller
{
    public function index(){
    	$belum_verifikasis = Pembayaran::where('status_verifikasi','belum')->paginate(10);
      	$sudah_verifikasis = Pembayaran::where('status_verifikasi','sudah')->paginate(10);
    	$pembayarans = Pembayaran::orderBy('updated_at','desc')->paginate(10);
    	return view('pages.admin.pesan.pembayaran.index',compact('pembayarans','belum_verifikasis','sudah_verifikasis'));
    }

     public function verifikasiPembayaran($id_pesan){
      // $pembayaran = Pembayaran::where('id_pesan',$id_pesan)->first();

      $pesanan = Pesanan::find($id_pesan);
      $pesanan_details = $pesanan->detail()->get();
      return view('pages.admin.pesan.pembayaran.verifikasi',compact('pesanan','pesanan_details'));
    }

    public function postVerifikasiPembayaran(Request $req, $id_pesan){
      $carbon = Carbon::now();

      if (is_null(Pembayaran::where('id_pesan',$id_pesan)->first())) {
        $number = Pembayaran::whereDate('created_at',date('Y-m-d'))->get()->count();
        $id_bayar = "BYR" . date('my-his') . ($number+1);
        $pembayaran = new Pembayaran();
        $pembayaran->id_bayar = $id_bayar;
      } else {
        $pembayaran = Pembayaran::where('id_pesan',$id_pesan)->first();
      }
      // dd($pembayaran);
      // $pembayaran = Pembayaran::firstOrNew(['id_pesan'=>$id_pesan]);
      $pembayaran->id_pesan = $id_pesan;
      $pembayaran->tanggal_verifikasi = $carbon;
      $pembayaran->no_bukti = $req->no_bukti;
      $pembayaran->jumlah_bayar = $req->jumlah_bayar;
      $pembayaran->status_verifikasi = 'sudah';
      $pembayaran->save();

      // foreach ($carts as $cart) {
      $pesanan = Pesanan::find($id_pesan);
      foreach ($pesanan->detail as $detail) {
        $pesanan_toko = PesananToko::where('id_pesan',$id_pesan)->where('id_toko',$detail->id_toko)->first();
        if (is_null($pesanan_toko)) {
          $pesanan_toko = new PesananToko();
          $pesanan_toko->id_pesan = $id_pesan;
          $pesanan_toko->id_toko = $detail->id_toko;
          $pesanan_toko->id_customer = $detail->pesanan->id_customer;
          $pesanan_toko->ongkos_kirim = 0;
          $pesanan_toko->total_akhir = 0;
          $pesanan_toko->save();
        }

        $pesanan_toko_detail = new PesananTokoDetail();
        $pesanan_toko_detail->id_pesanan_toko = $pesanan_toko->id_pesanan_toko;
        $pesanan_toko_detail->id_det = $detail->id_det;
        $pesanan_toko_detail->subtotal_beli = $detail->subtotal;
        $pesanan_toko_detail->save();
      }
      $this->hitungUlang($id_pesan);

      return redirect(route('indexPembayaranAdmin'));
    }

    public function show($id_bayar){
      $pembayaran = Pembayaran::find($id_bayar);
      $pesanan = $pembayaran->pesanan;
      return view('pages.admin.pesan.pembayaran.show',compact('pembayaran','pesanan'));
    }

    public function edit($id_bayar){
      $pembayaran = Pembayaran::find($id_bayar);
      $pesanan = $pembayaran->pesanan;
      return view('pages.admin.pesan.pembayaran.edit',compact('pembayaran','pesanan'));
    }

    public function update(Request $req, $id_bayar){
      if (is_null($req->file('image_no_bukti')) === false) {
        $file = $req->file('image_no_bukti');
        $image_path = 'img/pembayaran/' . str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName());
        $image = Image::make($file->getRealPath())->fit(700, 520)->save($image_path);  
      }

      $pembayaran = Pembayaran::find($id_bayar);
      $pembayaran->id_bayar = $req->id_bayar;
      $pembayaran->id_pesan = $req->id_pesan;
      // $pembayaran->tanggal_transfer = $req->tanggal_transfer;
      $pembayaran->tanggal_verifikasi = $req->tanggal_verifikasi;
      $pembayaran->no_bukti = $req->no_bukti;
      
      if (is_null($req->file('image_no_bukti')) === false) {
        $pembayaran->image_no_bukti = $image_path;
      }

      $pembayaran->jumlah_bayar = $req->jumlah_bayar;
      $pembayaran->keterangan = $req->keterangan;
      $pembayaran->save();

      return redirect(route('indexPembayaranAdmin'));
    }

    public function delete($id_bayar){
      $pembayaran = Pembayaran::find($id_bayar);
      $pembayaran->status_verifikasi = 'belum';
      $pembayaran->tanggal_verifikasi = null;
      $pembayaran->save();

      return redirect(route('indexPembayaranAdmin'));
    }

    public function hitungUlang($id_pesan){

      $pesanan_tokos = PesananToko::where('id_pesan',$id_pesan)->get();
      foreach ($pesanan_tokos as $pesanan) {
        $pesanan_toko = PesananToko::find($pesanan->id_pesanan_toko);
        $ongkos_perkilo = Pesanan::find($id_pesan)->jarak->ongkos;
        $berat_gram = DB::table('pesanans')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->where('pesanans.id_pesan',$id_pesan)
            ->where('pesanan_details.id_toko',$pesanan_toko->id_toko)
            ->sum(DB::raw('produks.berat_prod * pesanan_details.jumlah_barang'));

        $subtotal = DB::table('pesanans')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->where('pesanans.id_pesan',$id_pesan)
            ->where('pesanan_details.id_toko',$pesanan_toko->id_toko)
            ->sum('pesanan_details.subtotal');

        $min = 300;
        $max = 1300;

        if ($berat_gram == 0) {
            $berat_kilo = 0;
        } else if ($berat_gram < $min) {
            $berat_kilo = 1;
        } else {
            for($i=1;$i<=1000;$i++){
                if ($berat_gram >= $min && $berat_gram <= $max) {
                    $berat_kilo = $i;
                    break;
                }
                $min = $min + 1000;
                $max = $max + 1000;
            }
        }
        $ongkos_kirim = $berat_kilo * $ongkos_perkilo;

        $pesanan_toko->ongkos_kirim = $ongkos_kirim;
        $pesanan_toko->total_akhir = $ongkos_kirim + $subtotal;
        $pesanan_toko->save();
      }
    }
}
