<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pesanan;
use App\PesananDetail;

use URL;
use Auth;
use DB;

class PesananController extends Controller
{
    public function index(){
    	$pesanans = Pesanan::orderBy('updated_at','desc')->paginate(10);
    	return view('pages.admin.pesan.pesanan.index', compact('pesanans'));
    }

    public function delete($id_pesan){
    	$pesanans = Pesanan::find($id_pesan)->delete();
    	return redirect(URL::previous());
    }

    public function detail($id_pesan){
    	$pesanan = Pesanan::find($id_pesan);
    	return view('pages.admin.pesan.pesanan.detail', compact('pesanan'));
    }

    public function deleteDetail($id_pesan, $id_det){
    	$pesanan_detail = PesananDetail::find($id_det)->delete();
        $this->hitungUlang($id_pesan);
    	return redirect(URL::previous());
    }

    // untuk Hitung Ulang Pembayaran
    public function hitungUlang($id_pesan){
        $ongkos_perkilo = Pesanan::find($id_pesan)->jarak->ongkos;
        $berat_gram = DB::table('pesanans')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->where('pesanans.id_pesan',$id_pesan)
            ->sum('produks.berat_prod');

        $subtotal = DB::table('pesanans')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->where('pesanans.id_pesan',$id_pesan)
            ->sum('pesanan_details.subtotal');

        $min = 300;
        $max = 1300;

        if ($berat_gram == 0) {
            $berat_kilo = 0;
        } else if ($berat_gram < $min) {
            $berat_kilo = 1;
        } else {
            for($i=1;$i<=1000;$i++){
                if ($berat_gram >= $min && $berat_gram <= $max) {
                    $berat_kilo = $i;
                    break;
                }
                $min = $min + 1000;
                $max = $max + 1000;
            }
        }
        $ongkos_kirim = $berat_kilo * $ongkos_perkilo;

        $pesanan = Pesanan::find($id_pesan);
        $pesanan->ongkos_kirim = $ongkos_kirim;
        $pesanan->total_bayar = $ongkos_kirim + $subtotal;
        $pesanan->save();
    }
}
