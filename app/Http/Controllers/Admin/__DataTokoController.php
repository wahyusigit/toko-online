<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Permission;

class DataTokoController extends Controller
{
    public function index(){
    	$toko = Role::where('name','toko')->first()->user()->paginate(10);
    	return view('pages.admin.datatoko.index', compact('toko'));
    }

    public function add(){
    	return view('pages.admin.datatoko.add');
    }

    public function postAdd(Request $req){
        $role_id = Role::where('name','toko')->first()->id;

    	$toko = new User();
        $toko -> username = $req->username;
        $toko -> name = $req->name;
        $toko -> email = $req->email;
        $toko -> password = bcrypt($req->password);
        $toko -> save();
        $toko->roles()->attach($role_id);
        flash('Data Toko Berhasil Ditambahkan...')->success();

        return redirect(route('indexDataTokoAdmin'));
    }

    public function edit($id){
    	$toko = User::find($id);
    	return view('pages.admin.datatoko.edit', compact('toko'));
    }

    public function update(Request $req, $id){
        $toko = User::find($req->id);
        $toko -> username = $req->username;
        $toko -> name = $req->name;
        $toko -> email = $req->email;

        if (is_null($req->password) === false) {
            $toko -> password = bcrypt($req->password);   
        }

        if($toko -> save()){
            flash('Data Toko Berhasil Diubah...')->success();
        } else {
            flash('Data Toko Tidak Berhasil Diubah...')->error();
        }

        return redirect(route('indexDataTokoAdmin'));
    }

    public function delete($id){
        if(User::find($id)->delete()){
            flash('Data Toko Berhasil Dihapus...')->success();
        } else {
            flash('Data Toko Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexDataTokoAdmin'));
    }

    public function gantiPassword(Request $req){
    	$toko = User::find($req->id);
    	$toko -> password = Hash::make($req->password);
    	$toko -> save();
    }
}
