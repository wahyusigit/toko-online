<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Jarak;
class JarakController extends Controller
{
    public function index(){
    	$jaraks = Jarak::paginate(10);
    	return view('pages.admin.jarak.index', compact('jaraks'));
    }

    public function add(){
    	return view('pages.admin.jarak.add');
    }

    public function postAdd(Request $req){
    	$jarak = new Jarak();
    	$jarak -> tujuan = $req->tujuan;
    	$jarak -> ongkos = $req->ongkos;
    	$jarak -> save();

    	return redirect(route('indexJarakAdmin'));
    }

    public function edit($id){
        $jarak = Jarak::find($id);
        return view('pages.admin.jarak.edit', compact('jarak'));
    }

    public function update(Request $req, $id){
        $jarak = Jarak::find($id);
        $jarak -> tujuan = $req->tujuan;
        $jarak -> ongkos = $req->ongkos;
        if($jarak -> save()){
            flash('Data Produk Jenis Berhasil Diubah...')->success();
        } else {
            flash('Data Produk Jenis Tidak Berhasil Diubah...')->error();
        }

        return redirect(route('indexJarakAdmin'));
    }

    public function delete($id){
        if(Jarak::find($id)->delete()){
            flash('Data Produk Jenis Berhasil Dihapus...')->success();
        } else {
            flash('Data Produk Jenis Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexJarakAdmin'));
    }
}
