<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;

class DataAdminController extends Controller
{
    public function index(){
        $admin = Role::where('name','admin')->first()->user()->paginate(10);
    	return view('pages.admin.dataadmin.index', compact('admin'));
    }

    public function add(){
    	return view('pages.admin.dataadmin.add');
    }

    public function postAdd(Request $req){
        $role_id = Role::where('name','admin')->first()->id;

    	$admin = new User();
        $admin -> username = $req->username;
        $admin -> name = $req->name;
        $admin -> email = $req->email;
        $admin -> password = bcrypt($req->password);
        $admin -> save();
        $admin->roles()->attach($role_id);
        // if ( & ) {
            flash('Data Admin Berhasil Ditambahkan...')->success();
        // } else {
            // flash('Data Admin Tidak Berhasil Ditambahkan...')->error();
        // }

        return redirect(route('indexDataAdmin'));
    }

    public function edit($id){
    	$admin = User::find($id);
    	return view('pages.admin.dataadmin.edit', compact('admin'));
    }

    public function update(Request $req, $id){
        $admin = User::find($req->id);
        $admin -> username = $req->username;
        $admin -> name = $req->name;
        $admin -> email = $req->email;

        if (is_null($req->password) === false) {
            $admin -> password = bcrypt($req->password);   
        }

        if($admin -> save()){
            flash('Data Admin Berhasil Diubah...')->success();
        } else {
            flash('Data Admin Tidak Berhasil Diubah...')->error();
        }

        return redirect(route('indexDataAdmin'));
    }

    public function delete($id){
        if(User::find($id)->delete()){
            flash('Data Admin Berhasil Dihapus...')->success();
        } else {
            flash('Data Admin Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexDataAdmin'));
    }

    public function gantiPassword(Request $req){
    	$admin = User::find($req->id);
    	$admin -> password = Hash::make($req->password);
    	$admin -> save();
    }
}