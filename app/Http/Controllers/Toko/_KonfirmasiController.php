<?php

namespace App\Http\Controllers\Toko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pesanan;
use App\PesananDetail;
use App\Konfirmasi;

use Auth;
use URL;

class KonfirmasiController extends Controller
{
    public function index(){
    	$pesanans = Pesanan::where('id_user',Auth::user()->id)->get();
    	return view('pages.toko.konfirmasi.index',compact('pesanans'));
    }

    public function konfirmasi($id_pesan){
    	$konfirmasi = Konfirmasi::where('id_pesan',$id_pesan)->first();
    	$pesanan = Pesanan::find($id_pesan);
    	$pesanan_details = $pesanan->detail()->get();
    	return view('pages.toko.konfirmasi.complete',compact('pesanan','pesanan_details','konfirmasi'));
    }

    public function postKonfirmasi(Request $req, $id_pesan){
    	$konfirmasi = new Konfirmasi();
		$konfirmasi->id_pesan = $req->id_pesan;
		$konfirmasi->tanggal_konfirmasi = $req->tanggal_konfirmasi;
		$konfirmasi->no_bukti = $req->no_bukti;
		$konfirmasi->jumlah_bayar = $req->jumlah_bayar;
		$konfirmasi->status_baca = '0';
		$konfirmasi->keterangan = $req->keterangan;
		$konfirmasi->save();

		return redirect(route('indexKonfirmasiCustomer'));

    }
}
