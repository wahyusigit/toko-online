<?php

namespace App\Http\Controllers\Toko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ProdukJenis;

class ProdukJenisController extends Controller
{
    public function index(){
    	$jenis = ProdukJenis::all();
    	return view('pages.admin.produk.jenis.index', compact('jenis'));
    }

    public function add(){
    	return view('pages.admin.produk.jenis.add');
    }

    public function postAdd(Request $req){
    	$jenis = new ProdukJenis();
    	$jenis -> nama_jenis = $req->nama_jenis;
    	$jenis -> save();

    	return redirect(route('indexProdukJenisAdmin'));
    }

    public function edit($id){
        $jenis = ProdukJenis::find($id);
        return view('pages.admin.produk.jenis.edit', compact('jenis'));
    }

    public function update(Request $req, $id){
        $jenis = ProdukJenis::find($id);
        $jenis -> nama_jenis = $req->nama_jenis;

        if($jenis -> save()){
            flash('Data Produk Jenis Berhasil Diubah...')->success();
        } else {
            flash('Data Produk Jenis Tidak Berhasil Diubah...')->error();
        }

        return redirect(route('indexProdukJenisAdmin'));
    }

    public function delete($id){
        if(ProdukJenis::find($id)->delete()){
            flash('Data Produk Jenis Berhasil Dihapus...')->success();
        } else {
            flash('Data Produk Jenis Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexProdukJenisAdmin'));
    }
}
