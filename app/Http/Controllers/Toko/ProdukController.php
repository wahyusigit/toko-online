<?php

namespace App\Http\Controllers\Toko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Produk;
use App\ProdukJenis;
use Image;
use Response;
use Auth;

class ProdukController extends Controller
{
    public function index(){
    	$produks = Produk::where('id_toko', Auth::user()->id)->get();
    	return view('pages.toko.produk.index', compact('produks'));
    }

    public function show($id_produk){
        $produk = Produk::find($id_produk);
        return view('pages.toko.produk.show', compact('produk'));
    }

    public function add(){
    	$jenis = ProdukJenis::all();
    	// $tokos = User::whereHas('roles', function($q){$q->where('name', 'toko');})->get();
        $tokos = Role::where('name','toko')->first()->user()->get();
    	return view('pages.toko.produk.add', compact('jenis','tokos'));
    }

    public function postAdd(Request $req){
    	$file = $req->file('image_prod');
        $image_prod = $file->move('img/produk/' , str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName()));

	    $produk = new Produk();
	    $produk->id_jenis = $req->id_jenis;

    	if(Auth::user()->hasRole('admin')){
    		$produk->id_toko = $req->id_toko;
    	} else if(Auth::user()->hasRole('toko')){
    		$produk->id_toko = Auth::user()->id;
    	}

	    $produk->nama_prod = $req->nama_prod;
	    $produk->harga_prod = $req->harga_prod;
	    $produk->berat_prod = $req->berat_prod;
	    $produk->keterangan_prod = $req->keterangan_prod;
	    $produk->image_prod = $image_prod;
	    $produk->status_prod = $req->status_prod;
	    if ($produk->save()) {
	    	flash('Produk Berhasil Ditambahkan...')->success();
	    } else {
	    	flash('Produk Tidak Berhasil Ditambahkan...')->error();
	    }

        return redirect(route('indexProdukToko'));
    }

    public function edit($id){
    	$produk = Produk::find($id);
        $jenis = ProdukJenis::all();
    	return view('pages.toko.produk.edit', compact('produk','jenis'));
    }

    public function update(Request $req, $id){
        if (is_null($req->image_prod) === false) {
            $file = $req->file('image_prod');
            $image_prod = $file->move('img/produk/' , str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName()));
        }

        $produk = Produk::find($id);
        $produk->id_jenis = $req->id_jenis;

        if(Auth::user()->hasRole('admin')){
            $produk->id_toko = $req->id_toko;
        } else if(Auth::user()->hasRole('toko')){
            $produk->id_toko = Auth::user()->id;
        }

        $produk->nama_prod = $req->nama_prod;
        $produk->harga_prod = $req->harga_prod;
        $produk->berat_prod = $req->berat_prod;
        $produk->keterangan_prod = $req->keterangan_prod;
        if (is_null($req->image_prod) === false) {
            $produk->image_prod = $image_prod;
        }
        $produk->status_prod = $req->status_prod;
        if ($produk->save()) {
            flash('Produk Berhasil Ditambahkan...')->success();
        } else {
            flash('Produk Tidak Berhasil Ditambahkan...')->error();
        }

        return redirect(route('indexProdukToko'));
    }

    public function delete($id){
        if(Produk::find($id)->delete()){
            flash('Produk Berhasil Dihapus...')->success();
        } else {
            flash('Produk Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexProdukToko'));
    }
}
