<?php

namespace App\Http\Controllers\Toko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pesanan;
use App\PesananToko;
use App\PesananTokoDetail;
use App\Pembayaran;
use App\PesananDetail;
use App\Produk;


use DB;
use URL;
use Auth;

class PesananController extends Controller
{
    public $ongkos_kirim;
    public $total_bayar;

    public function index(){
        $pesanan_tokos = PesananToko::where('id_toko',Auth::user()->id)->where('status_kirim','belum')->get();
        return view('pages.toko.pesanan.index', compact('pesanan_tokos'));

    }

    public function detail($id_pesanan_toko){
        $pesanan_toko = PesananToko::find($id_pesanan_toko);
    	// $pesanan = Pesanan::find($id_pesan);
     //    $this->hitungUlang($id_pesan);

     //    $ongkos_kirim = $this->ongkos_kirim;
     //    $total_bayar = $this->total_bayar;

    	return view('pages.toko.pesanan.detail', compact('pesanan_toko'));
    }

    public function hitungUlang($id_pesan){
        $ongkos_perkilo = Pesanan::find($id_pesan)->jarak->ongkos;
        $berat_gram = DB::table('pesanans')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->join('pembayarans','pembayarans.id_pesan','pesanans.id_pesan')
            ->where('pembayarans.status_verifikasi','sudah')
            ->where('pesanans.id_pesan',$id_pesan)
            ->where('pesanan_details.id_toko',Auth::user()->id)
            ->sum('produks.berat_prod');

        $subtotal = DB::table('pesanans')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->join('pembayarans','pembayarans.id_pesan','pesanans.id_pesan')
            ->where('pembayarans.status_verifikasi','sudah')
            ->where('pesanans.id_pesan',$id_pesan)
            ->where('pesanan_details.id_toko',Auth::user()->id)
            ->sum('pesanan_details.subtotal');

        $min = 300;
        $max = 1300;

        if ($berat_gram == 0) {
            $berat_kilo = 0;
        } else if ($berat_gram < $min) {
            $berat_kilo = 1;
        } else {
            for($i=1;$i<=1000;$i++){
                if ($berat_gram >= $min && $berat_gram <= $max) {
                    $berat_kilo = $i;
                    break;
                }
                $min = $min + 1000;
                $max = $max + 1000;
            }
        }
        $ongkos_kirim = $berat_kilo * $ongkos_perkilo;

        $this->ongkos_kirim = $ongkos_kirim;
        $this->total_bayar = $ongkos_kirim + $subtotal;
    }
}
