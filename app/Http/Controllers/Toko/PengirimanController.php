<?php

namespace App\Http\Controllers\Toko;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pengiriman;
use App\PengirimanDetail;
use App\PesananToko;
use App\PesananDetail;

use DB;
use Auth;
use Carbon\Carbon;

class PengirimanController extends Controller
{
    public function index(){
    	$pengirimans = Pengiriman::where('id_toko', Auth::user()->id)->paginate(10);
    	return view('pages.toko.pengiriman.index', compact('pengirimans'));
    }

    public function postAdd(Request $req, $id_pesanan_toko){
        $pesanan_toko = PesananToko::find($id_pesanan_toko);
        

        $number = Pengiriman::whereDate('created_at',date('Y-m-d'))->get()->count();
        $id_kirim = "KRM" . date('my-his') . ($number+1);

        $pengiriman = new Pengiriman();
        $pengiriman->id_kirim = $id_kirim;
        $pengiriman->id_pesan = $pesanan_toko->id_pesan;
        $pengiriman->id_toko = $pesanan_toko->id_toko;
        $pengiriman->id_customer = $pesanan_toko->id_customer;
        $pengiriman->tanggal_kirim = $req->tanggal_kirim;
        $pengiriman->no_resi = $req->no_resi;
        $pengiriman->status_diterima = 'belum';
        $pengiriman->save();

        $pesanan_toko->status_kirim = 'sudah';
        $pesanan_toko->save();

        foreach ($pesanan_toko->detail as $detail) {
            $pengiriman_detail = new PengirimanDetail();
            $pengiriman_detail->id_kirim = $id_kirim;
            $pengiriman_detail->id_produk = $detail->detail->produk->id_produk;
            $pengiriman_detail->id_det = $detail->id_det;
            $pengiriman_detail->save();

            $pesanan_details = PesananDetail::find($detail->id_det);
            $pesanan_details->status_kirim = 'sudah';
            $pesanan_details->save();
        }

        return redirect(route('indexPengirimanToko'));
    }

    public function detail($id_kirim){
        $pengiriman = Pengiriman::find($id_kirim);
        return view('pages.toko.pengiriman.detail', compact('pengiriman'));
    }

    public function edit($id_kirim){
        $pengiriman = Pengiriman::find($id_kirim);
        return view('pages.toko.pengiriman.edit', compact('pengiriman'));
    }

    public function update(Request $req, $id_kirim){
        $pengiriman = Pengiriman::find($id_kirim);
        $pengiriman->no_resi = $req->no_resi;
        $pengiriman->save();

        return redirect(route('indexPengirimanToko'));
    }
}
