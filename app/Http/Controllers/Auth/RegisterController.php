<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'alamat' => 'required|string|max:150',
            'kota' => 'required|string|max:32',
            'provinsi' => 'required|string|max:32',
            'kode_pos' => 'required|string|max:6',
            'telepon' => 'required|string|max:12'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $username = strtolower(substr(preg_replace('/\s+/', '', $data['name'] ),0,6));
        $role_id = Role::where('name','customer')->first()->id;
        return User::create([
            'name'      => $data['name'],
            'username'  => $username,
            'role_id'   => $role_id,
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
            'alamat'    => $data['alamat'],
            'kota'      => $data['kota'],
            'provinsi'  => $data['provinsi'],
            'kode_pos'  => $data['kode_pos'],
            'telepon'   => $data['telepon']
        ]);
    }
}
