<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;
use URL;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

        /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    // public function authenticate()
    // {
    //     if (Auth::user()->hasRole('customer')) {
    //             return redirect(route('indexAdmin'));
    //         } else if (Auth::user()->hasRole('toko')) {
    //             return redirect(route('indexToko'));
    //         } else if (Auth::user()->hasRole('admin')) {
    //             return redirect(route('indexCustomer'));
    //         } else {
                
    //         }
    //     if (Auth::attempt(['email' => $email, 'password' => $password])) {
    //         if (Auth::user()->hasRole('customer')) {
    //             return redirect(route('indexAdmin'));
    //         } else if (Auth::user()->hasRole('toko')) {
    //             return redirect(route('indexToko'));
    //         } else if (Auth::user()->hasRole('admin')) {
    //             return redirect(route('indexCustomer'));
    //         } else {

    //         }
    //     }
    // }

    public function authenticated()
    {
        if (strpos( URL::previous(), 'checkout' ) !== false) {
            if ( Auth::user()->hasRole('customer') == true) {
                // jika dari checkout & customer    
                return redirect(route('completeCheckout'));
            } else if ( Auth::user()->hasRole('customer') == false) {
                // jika dari checkout & bukan customer  
                if (Auth::user()->hasRole('admin') == true) {
                    Auth::logout();
                    flash('Maaf, Pembelihan hanya bisa dilakukan oleh Customer...')->error();
                } else if (Auth::user()->hasRole('toko') == true) {
                    Auth::logout();
                    flash('Maaf, Pembelihan hanya bisa dilakukan oleh Customer...')->error();
                }  
            }
            return redirect('checkout');

        } else {
            if (Auth::user()->hasRole('customer')) {
                return redirect(route('indexCustomer'));
            } else if (Auth::user()->hasRole('toko')) {
                return redirect(route('indexToko'));
            } else if (Auth::user()->hasRole('admin')) {
                return redirect(route('indexAdmin'));
            } else {
                return redirect(route('logout'));
            }
            // jika bukan dari checkout
        }
    }
}
