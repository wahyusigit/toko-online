<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Pesanan;
use App\PesananDetail;

use URL;
use Auth;

class PesananController extends Controller
{
	private $data,$view_path;
	private $user_id;

	// public function userCheck(){
	// 	if (Auth::check()) {
	// 		if (Auth::user()->hasRole('admin')) {
	// 			$this->data['id_user'] = Auth::user()->id;
	// 			$this->data['view'] = 'admin';
	// 			return $this->data;
				
	// 		} else if (Auth::user()->hasRole('toko')) {
	// 			$this->data['id_user'] = Auth::user()->id;
	// 			$this->data['view'] = 'toko';
	// 			return $this->data;
	// 		} else if (Auth::user()->hasRole('customer')) {
	// 			$this->data['id_user'] = Auth::user()->id;
	// 			$this->data['view'] = 'customer';
	// 			return $this->data;
	// 		} else {
	// 			$this->data['id_user'] = Auth::user()->id;
	// 			$this->data['view'] = 'else';
	// 			return $this->data;
	// 		}	
	// 	}
	// }

	public function __construct(){
		$this->middleware(function ($request, $next) {
	        $this->user_id = Auth::user()->id;
	        if (Auth::user()->hasRole('admin')) {
	        	$this->view_path = 'admin.pesan.pesanan';	
	        } else if (Auth::user()->hasRole('toko')) {
	        	$this->view_path = 'toko.pesanan';	
	        } else if (Auth::user()->hasRole('customer')) {
	        	$this->view_path = 'customer.pesanan';	
	        }
	        
	        return $next($request);
	    });
	}

    public function index(){
    	if (Auth::user()->hasRole('admin')) {
    		$pesanans = Pesanan::all();
    	} else {
    		$pesanans = Pesanan::where('id_user',Auth::user()->id)->get();
    	}
    	return view('pages.' . $this->view_path . '.index', compact('pesanans'));
    }

    public function delete($id_pesan){
    	$pesanans = Pesanan::find($id_pesan)->delete();
    	return redirect(URL::previous());
    }

    public function detail($id_pesan){
    	$pesanan_details = Pesanan::find($id_pesan)->detail()->get();
    	return view('pages.' . $this->view_path . '.detail', compact('pesanan_details'));
    }

    public function deleteDetail($id_det){
    	$pesanan_detail = PesananDetail::find($id_det)->delete();

    	return redirect(URL::previous());
    }
}
