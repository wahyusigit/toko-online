<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Konfirmasi;
use App\Pesanan;

use Auth;
class KonfirmasiController extends Controller
{
    public function index(){
    	$pesanans = Pesanan::where('id_user',Auth::user()->id)->get();
    	return view('pages.customer.konfirmasi.index',compact('pesanans'));
    }

    public function konfirmasi($id_pesan){
    	$konfirmasi = Konfirmasi::where('id_pesan',$id_pesan)->first();
    	$pesanan = Pesanan::find($id_pesan);
    	$pesanan_details = $pesanan->detail()->get();
    	return view('pages.customer.konfirmasi.konfirmasi',compact('pesanan','pesanan_details','konfirmasi'));
    }

    public function postKonfirmasi(Request $req, $id_pesan){
    	$konf = Konfirmasi::where('id_pesan', $id_pesan)->first();
    	if (is_null($konf)) {
    		$konfirmasi = new Konfirmasi();
    	} else {
    		$konfirmasi = $konf;
    	}

    	$konfirmasi->id_pesan = $id_pesan;
		$konfirmasi->tanggal_konfirmasi = $req->tanggal_konfirmasi;
		$konfirmasi->no_bukti = $req->no_bukti;
		$konfirmasi->jumlah_bayar = $req->jumlah_bayar;
		$konfirmasi->keterangan = $req->keterangan;
		$konfirmasi->save();

		return redirect(route('indexPesananCustomer'));

    }
}
