<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Testimoni;
use App\PesananDetail;
use App\Produk;

use Auth;
use DB;
use Carbon\Carbon;

class TestimoniController extends Controller
{
    public function index(){
    	$testimonis = Testimoni::where('id_customer',Auth::user()->id)->paginate(10);
    	return view('pages.customer.testimoni.index', compact('testimonis'));
    }

    public function add(){
    	$pesanans = DB::table('pesanans')
    				->join('users','users.id','pesanans.id_customer')
    				->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
    				->join('produks','produks.id_produk','pesanan_details.id_produk')
    				->where('pesanans.id_customer',Auth::user()->id)
    				->where('pesanan_details.status_testimoni','belum')
                    ->where('pesanan_details.status_diterima','sudah')
    				->get();
    	return view('pages.customer.testimoni.add',compact('pesanans'));
    }

    public function add2($id_det){
    	$testimoni = PesananDetail::find($id_det);
    	return view('pages.customer.testimoni.add2',compact('testimoni'));
    }

	public function postAdd2(Request $req, $id_det){
		$pesanan_details = PesananDetail::find($id_det);
		$pesanan_details->status_testimoni = 'sudah';
		$pesanan_details->save();
		
    	$testimoni = new Testimoni();
		$testimoni->id_customer = Auth::user()->id;
		$testimoni->id_produk = $req->id_produk;
		$testimoni->ulasan = $req->ulasan;
		$testimoni->tanggal = Carbon::now();
		$testimoni->tingkat_kepuasan = $req->tingkat_kepuasan;
		$testimoni->save();

        // Hitung Ulang Rating Produk
        $rating = Testimoni::where('id_produk', $testimoni->id_produk);
        $rating_total = $rating->sum('tingkat_kepuasan');
        $rating_count = $rating->count();
        if ($rating_count <= 1) {
            $rating_star = $req->tingkat_kepuasan;
        } else {
            $rating_star = round($rating_total / $rating_count);    
        }

        $produk = Produk::find($testimoni->id_produk);
        $produk->rating_prod = $rating_star;
        $produk->save();

		return redirect(route('indexTestimoniCustomer'));
    }

    public function show($id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni);
    	$testimoni->status_baca = '1';
    	$testimoni->save();
    	
    	return view('pages.customer.testimoni.show', compact('testimoni'));
    }

    public function edit($id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni);
    	$testimoni->status_baca = '1';
    	$testimoni->save();

    	return view('pages.customer.testimoni.edit', compact('testimoni'));
    }

    public function update(Request $req, $id_testimoni){
    	$testimoni = Testimoni::find($id_testimoni);
    	$testimoni->tingkat_kepuasan = $req->tingkat_kepuasan;
    	$testimoni->save();
    	return redirect(route('indexTestimoniCustomer'));
    }

    public function delete($id_testimoni){
    	$testimonis = Testimoni::find($id_testimoni)->delete();
    	return redirect(route('indexTestimoniCustomer'));
    }
}
