<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pengiriman;
use App\Pesanan;
use App\PesananDetail;

use Carbon\Carbon;
use DB;
use Auth;

class PengirimanController extends Controller
{
    public function index(){
        // $pesanans = Pesanan::where('id_customer',Auth::user()->id)->get();
        $sudah_dikirim = DB::table('pesanans')
            ->join('pembayarans','pembayarans.id_pesan','pesanans.id_pesan')
            ->join('pengirimen','pengirimen.id_pesan','pesanans.id_pesan')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->join('users','users.id','pengirimen.id_toko')
            ->where('pesanans.id_customer',Auth::user()->id)
            ->where('pembayarans.status_verifikasi','sudah')
            ->where('pesanan_details.status_kirim','sudah')
            ->where('pesanan_details.status_diterima','belum')
            ->select('pesanans.id_pesan','pesanans.tanggal_pesan','pengirimen.tanggal_kirim','pengirimen.id_kirim','pengirimen.no_resi','pesanan_details.status_diterima','pesanan_details.jumlah_barang','pesanan_details.subtotal','produks.nama_prod','produks.harga_prod','produks.image_prod','users.name as pengirim','pesanan_details.id_det')
            ->get();

        $belum_dikirim = DB::table('pesanans')
            ->join('pembayarans','pembayarans.id_pesan','pesanans.id_pesan')
            ->join('pengirimen','pengirimen.id_pesan','pesanans.id_pesan')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->join('users','users.id','pengirimen.id_toko')
            ->where('pesanans.id_customer',Auth::user()->id)
            ->where('pembayarans.status_verifikasi','sudah')
            ->where('pesanan_details.status_kirim','belum')
            ->where('pesanan_details.status_diterima','belum')
            ->select('pesanans.id_pesan','pesanans.tanggal_pesan','pengirimen.tanggal_kirim','pengirimen.id_kirim','pengirimen.no_resi','pesanan_details.status_diterima','pesanan_details.jumlah_barang','pesanan_details.subtotal','produks.nama_prod','produks.harga_prod','produks.image_prod','users.name as pengirim')
            ->get();

        $sudah_diterima = DB::table('pesanans')
            ->join('pembayarans','pembayarans.id_pesan','pesanans.id_pesan')
            ->join('pengirimen','pengirimen.id_pesan','pesanans.id_pesan')
            ->join('pesanan_details','pesanan_details.id_pesan','pesanans.id_pesan')
            ->join('produks','produks.id_produk','pesanan_details.id_produk')
            ->join('users','users.id','pengirimen.id_toko')
            ->where('pesanans.id_customer',Auth::user()->id)
            ->where('pembayarans.status_verifikasi','sudah')
            ->where('pesanan_details.status_kirim','sudah')
            ->where('pesanan_details.status_diterima','sudah')
            ->select('pesanans.id_pesan','pesanans.tanggal_pesan','pengirimen.tanggal_kirim','pengirimen.id_kirim','pengirimen.no_resi','pesanan_details.status_diterima','pesanan_details.jumlah_barang','pesanan_details.subtotal','produks.nama_prod','produks.harga_prod','produks.image_prod','users.name as pengirim')
            ->get();

        return view('pages.customer.pengiriman.index', compact('sudah_dikirim','belum_dikirim','sudah_diterima'));
    }

    public function detail($id_kirim){
    	$pengiriman = Pengiriman::find($id_kirim);
    	return view('pages.customer.pengiriman.detail', compact('pengiriman'));
    }

    // Konfirmasi Penerimaan Barang yang Sudah Dikirim dari Toko
    public function konfirmasi($id_det){
    	$pengiriman = PesananDetail::find($id_det);
    	$pengiriman->status_diterima = 'sudah';
    	$pengiriman->save();
    	flash('Terimakasih Sudah Melakukan Konfirmasi Penerimaan Produk')->success();

        return redirect(route('add2TestimoniCustomer', $id_det));
    }
}
