<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pesanan;
use App\PesananDetail;
use App\Pembayaran;

use URL;
use Auth;
use DB;
use Image;

class PesananController extends Controller
{
    protected $berat_produk_akhir = 0;
    protected $subtotal_akhir = 0;

    public function index(){
    	$pesanans = Pesanan::where('id_customer',Auth::user()->id)->get();
    	return view('pages.customer.pesanan.index', compact('pesanans'));
    }

    public function delete($id_pesan){
    	$pesanans = Pesanan::find($id_pesan)->delete();
    	return redirect(URL::previous());
    }

    public function detail($id_pesan){
    	$pesanan = Pesanan::find($id_pesan);
    	return view('pages.customer.pesanan.detail', compact('pesanan'));
    }

    public function deleteDetail($id_pesan, $id_det){
    	$pesanan_detail = PesananDetail::find($id_det)->delete();
        $this->hitungUlang($id_pesan);
    	return redirect(URL::previous());
    }

    // untuk Hitung Ulang Pembayaran
    public function hitungUlang($id_pesan){
        
        $pesanan = Pesanan::find($id_pesan);
        foreach ($pesanan->detail as $key => $detail) {
            // Hitung Berat Produk
            $berat_per_produk = $detail->produk->berat_prod;
            $qty_produk = $detail->jumlah_barang;
            $berat_produk = $berat_per_produk * $qty_produk;
            $this->berat_produk_akhir = $this->berat_produk_akhir + $berat_produk;

            // Hitung Subtotal Produk
            $subtotal = $detail->harga_satuan * $detail->jumlah_barang;
            $this->subtotal_akhir = $this->subtotal_akhir + $subtotal;

            // Hitung Ulang Subtotal Pesanan Detail
            $pesanan_detail = PesananDetail::find($detail->id_det);
            $pesanan_detail->subtotal = $subtotal;
            $pesanan_detail->save();
        }

        $min = 300;
        $max = 1299;

        if ($this->berat_produk_akhir == 0) {
            $berat_kilo = 0;
        } else if ($this->berat_produk_akhir < $max) {
            $berat_kilo = 1;
        } else {
            for($i=1;$i<=1000;$i++){
                if ($this->berat_produk_akhir >= $min && $this->berat_produk_akhir <= $max) {
                    $berat_kilo = $i;
                    break;
                } else {
                    $min = $min + 1000;
                    $max = $max + 1000;
                }
            }
        }

        $ongkos_perkilo = Pesanan::find($id_pesan)->jarak->ongkos;
        $ongkos_kirim = $berat_kilo * $ongkos_perkilo;

        $pesanan = Pesanan::find($id_pesan);
        $pesanan->ongkos_kirim = $ongkos_kirim;
        $pesanan->total_bayar = $ongkos_kirim + $this->subtotal_akhir;
        $pesanan->save();
    }

    public function konfirmasi($id_pesan){
        $pesanan = Pesanan::find($id_pesan);
        $pesanan_details = $pesanan->detail()->get();
        return view('pages.customer.pesanan.konfirmasi',compact('pesanan','pesanan_details'));
    }

    public function postKonfirmasi(Request $req, $id_pesan){
        $pesanan = Pesanan::find($id_pesan);
        if ($req->jumlah_bayar < $pesanan->total_bayar) {
            flash('Maaf, Jumlah yang anda Transfer kurang dari Total Akhir Pembelian Anda. Silahkan cek kembali Jumlah Transfer dan pastikan Anda melakukan Transfer Pembayaran sesuai dengan Total Akhir Pembelian Anda. Terimakasih.')->error();
            return redirect(route('konfirmasiCustomer', $id_pesan));
        } else {
            if (is_null($req->file('image_no_bukti')) === false) {
                $file = $req->file('image_no_bukti');
                $image_path = 'img/pembayaran/' . str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName());
                $image = Image::make($file->getRealPath())->fit(700, 520)->save($image_path);
            }

            $pemb = Pembayaran::where('id_pesan', $id_pesan)->first();
            if (is_null($pemb)) {
                $number = Pembayaran::whereDate('created_at',date('Y-m-d'))->get()->count();
                $id_bayar = "BYR" . date('my-his') . ($number+1);
                $pembayaran = new Pembayaran();
                $pembayaran->id_bayar = $id_bayar;
            } else {
                $pembayaran = $pemb;
            }
            $pembayaran->id_pesan = $id_pesan;
            $pembayaran->tanggal_transfer = $req->tanggal_transfer;
            $pembayaran->no_bukti = $req->no_bukti;

            if (is_null($req->file('image_no_bukti')) === false) {
                $pembayaran->image_no_bukti = $image_path;
            }

            $pembayaran->jumlah_bayar = $req->jumlah_bayar;
            $pembayaran->keterangan = $req->keterangan;
            $pembayaran->save();

            flash('Terimakasih sudah melakukan Konfirmasi Pembayaran. Selanjutnya Kami akan melakukan Verifikasi Pembayaran Anda dan biasanya membutuhkan waktu paling lama 2 X 24Jam.')->success();
            return redirect(route('indexPesananCustomer'));
        }

        
    }
}
