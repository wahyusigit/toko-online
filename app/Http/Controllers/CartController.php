<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Produk;
use App\ProdukJenis;
use App\Jarak;
use App\Pesanan;
use App\PesananDetail;
use App\PesananToko;
use App\PesananTokoDetail;

use Auth;
use Cart;
use Carbon\Carbon;
use URL;

class CartController extends Controller
{
    public function index(){
        $produk_jenis = ProdukJenis::all();
        $carts = Cart::content();
        return view('cart', compact('carts','produk_jenis'));
    }

    public function add($id_produk){
        // Check Double Order Produk
        $carts = Cart::content();
        $id_produks = [];
        foreach ($carts as $cart) {
            array_push($id_produks,$cart->id);
        }

        // if (in_array($id_produk, $id_produks)){
        //     flash('Maaf, Produk yang Anda tambahkan sudah ada di dalam Keranjang')->error();
        //     return redirect(route('indexCart'));
        // } else {
            $produk = Produk::find($id_produk);
            Cart::add(
                        [   'id' => $produk->id_produk,
                            'name' => $produk->nama_prod,
                            'qty' => 1,
                            'price' => $produk->harga_prod,
                            'options' => ['berat_prod' => $produk->berat_prod]
                        ]
                    );
            return redirect(route('indexCart'));
        // }
    }

    public function update(){

    }

    public function remove($rowId){
        Cart::remove($rowId);
        return redirect(route('indexCart'));
    }

    public function destroy(){
        Cart::destroy();
        return redirect(route('indexCart'));
    }

    public function checkout(){
        $produk_jenis = ProdukJenis::all();
        if (Auth::check()) {
            if (Auth::user()->hasRole('customer')) {
                return redirect(route('completeCheckout','produk_jenis'));    
            } else {
                flash('Maaf, Hanya Customer yang dapat melakukan Pembelian. Terimakasih.')->error();
                Auth::logout();
                // return redirect(URL::previous());
                return redirect(route('checkout'));   
            }
            
        } else {
            $carts = Cart::content();
            return view('checkout', compact('carts','produk_jenis'));    
        }
    }

    public function completeCheckout(){
        $produk_jenis = ProdukJenis::all();
        $carts = Cart::content();
        $jaraks = Jarak::all();
        return view('completeCheckout', compact('carts','jaraks','produk_jenis'));
    }

    public function postCompleteCheckout(Request $req){
        // HITUNG TOTAL BERAT PRODUK W>S
        $berat_prod = 0;
        $carts = Cart::content();
        foreach ($carts as $key => $value) {
            $berat_prod = $berat_prod + ($value->options->berat_prod * $value->qty);
        }

        $carbon = Carbon::now();
        $number = Pesanan::whereDate('created_at',date('Y-m-d'))->get()->count();
        $id_pesan = "PSN" . date('my-his') . ($number+1);
        $id_customer = Auth::user()->id;
        $ongkos_perkilo = Jarak::find($req->id_jarak)->ongkos;
        $ongkos_kirim = $this->beratKirim($berat_prod) * $ongkos_perkilo;
        $total_bayar = str_replace(".","",Cart::total());

        $pesanan = new Pesanan();
        $pesanan -> id_pesan = $id_pesan;
        $pesanan -> id_customer = $id_customer;
        $pesanan -> id_jarak = $req -> id_jarak;
        $pesanan -> tanggal_pesan = $carbon;
        $pesanan -> keterangan_pesan = $req -> keterangan_pesan;
        $pesanan -> total_bayar = $total_bayar + $ongkos_kirim;
        $pesanan -> tujuan = $req -> tujuan;
        $pesanan -> kota = $req -> kota;
        $pesanan -> provinsi = $req -> provinsi;
        $pesanan -> kode_pos = $req -> kode_pos;
        $pesanan -> telepon = $req -> telepon;
        $pesanan -> ongkos_kirim = $ongkos_kirim;
        $pesanan -> save();

        $carts = Cart::content();

        foreach ($carts as $cart) {
            $id_toko = Produk::where('id_produk',$cart->id)->first()->toko->id;
            $pesanan_detail = new PesananDetail(); 
            $pesanan_detail -> id_pesan = $pesanan->id_pesan;
            $pesanan_detail -> id_produk = $cart->id;
            $pesanan_detail -> id_toko = $id_toko;
            // $pesanan_detail -> id_pesanan_toko = $pesanan_toko->id_pesanan_toko;
            $pesanan_detail -> harga_satuan = str_replace(".","",$cart->price);
            $pesanan_detail -> jumlah_barang = $cart->qty;  
            $pesanan_detail -> subtotal = $cart->qty * str_replace(".","",$cart->price);  
            $pesanan_detail -> save();
        }
        Cart::destroy();
        return redirect(route('postKonfirmasiCustomer',$pesanan->id_pesan));
        
    }

    public function ajaxAlamatSama(){
        // if (Request::ajax()) {
            $user = User::where('id',Auth::user()->id)->select('alamat','kota','provinsi','kode_pos','telepon')->first();
            return $user;
        // }
    }

    public function beratKirim($berat){
        $min = 300;
        $max = 1300;

        if ($berat < $min) {
            return 1;
        } else {
            for($i=1;$i<=1000;$i++){
                if ($berat >= $min && $berat <= $max) {
                    $b = $i;
                    break;
                }
                $min = $min + 1000;
                $max = $max + 1000;
            }
            return $b;
        }
    }

}
