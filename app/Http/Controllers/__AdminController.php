<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Produk;

class AdminController extends Controller
{
    public function index(){
    	return view('pages.admin.index');
    }

    public function halaman(){
    	return view('pages.admin.halaman');
    }

    public function dataAdmin(){
        // $admin = new Admin();

    	return view('pages.admin.dataadmin');
    }

    public function dataAdminAdd(){
        // $admin = new Admin();
        
        return view('pages.admin.dataadmin_add');
    }

    public function dataUser(){
    	return view('pages.admin.datauser');
    }

    public function produk(){
    	return view('pages.admin.produk');
    }

    public function jarak(){
    	return view('pages.admin.jarak');
    }

    public function pesan(){
    	return view('pages.admin.pesan');
    }
    public function pesanPembayaran(){
        return view('pages.admin.pesan_pembayaran');
    }
    public function pesanPemesanan(){
        return view('pages.admin.pesan_pemesanan');
    }
    public function pesanKonfirmasi(){
        return view('pages.admin.pesan_konfirmasi');
    }
    public function pesanPengiriman(){
        return view('pages.admin.pesan_pengiriman');
    }

    public function kontak(){
    	return view('pages.admin.kontak');
    }
}
