<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Role;
use App\Produk;
use App\ProdukJenis;

use Image;
use Response;
use Auth;

class ProdukController extends Controller
{
    public function index(){
    	if (Auth::user()->hasRole('admin')) {
    		$produks = Produk::paginate(20);
    	} elseif (Auth::user()->hasRole('toko')) {
    		$produks = Produk::where('id_user',Auth::user()->id)->paginate(20);
    	}
    	
    	return view('pages.produk.index', compact('produks'));
    }

    public function show($id_produk){
        $produk = Produk::find($id_produk);
        return view('pages.produk.show', compact('produk'));
    }

    public function add(){
    	$jenis = ProdukJenis::all();
        $tokos = Role::where('name','toko')->first()->user()->get();
    	return view('pages.produk.add', compact('jenis','tokos'));
    }

    public function postAdd(Request $req){
    	$file = $req->file('image_prod');
        $image_prod = $file->move('img/produk/' , str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName()));

	    $produk = new Produk();
	    $produk->id_jenis = $req->id_jenis;

    	if(Auth::user()->hasRole('admin')){
    		$produk->id_user = $req->id_user;
    	} else if(Auth::user()->hasRole('toko')){
    		$produk->id_user = Auth::user()->id;
    	}

	    $produk->nama_prod = $req->nama_prod;
	    $produk->harga_prod = $req->harga_prod;
	    $produk->berat_prod = $req->berat_prod;
	    $produk->keterangan_prod = $req->keterangan_prod;
	    $produk->image_prod = $image_prod;
	    $produk->status_prod = $req->status_prod;
	    if ($produk->save()) {
	    	flash('Data Produk Berhasil Ditambahkan...')->success();
	    } else {
	    	flash('Data Produk Tidak Berhasil Ditambahkan...')->error();
	    }

        return redirect(route('indexProdukAdmin'));
    }

    public function edit($id){
    	$produk = Produk::find($id);
    	return view('pages.produk.edit', compact('produk'));
    }

    public function update(Request $req, $id){
        if (is_null($req->image_prod) === false) {
            $file = $req->file('image_prod');
            $image_prod = $file->move('img/produk/' , str_replace(' ', '', date("Y-m-d") . "-" . $file->getClientOriginalName()));
        }

        $produk = Produk::find($id);
        $produk->id_jenis = $req->id_jenis;

        if(Auth::user()->hasRole('admin')){
            $produk->id_user = $req->id_user;
        } else if(Auth::user()->hasRole('toko')){
            $produk->id_user = Auth::user()->id;
        }

        $produk->nama_prod = $req->nama_prod;
        $produk->harga_prod = $req->harga_prod;
        $produk->berat_prod = $req->berat_prod;
        $produk->keterangan_prod = $req->keterangan_prod;
        if (is_null($req->image_prod) === false) {
            $produk->image_prod = $image_prod;
        }
        $produk->status_prod = $req->status_prod;
        if ($produk->save()) {
            flash('Data Produk Berhasil Ditambahkan...')->success();
        } else {
            flash('Data Produk Tidak Berhasil Ditambahkan...')->error();
        }

        return redirect(route('indexProdukAdmin'));
    }

    public function delete($id){
        if(User::find($id)->delete()){
            flash('Data Produk Berhasil Dihapus...')->success();
        } else {
            flash('Data Produk Tidak Berhasil Dihapus...')->error();
        }
        return redirect(route('indexDataAdmin'));
    }
}
