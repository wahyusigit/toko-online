<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Pesanan;
use App\PesananDetail;
// use App\Konfirmasi;

use URL; 

use Auth;

class CustomerController extends Controller
{
    public function index(){
    	return view('pages.customer.index');
    }

    public function profile(){
    	$customer = User::find(Auth::user()->id);
    	return view('pages.customer.profile', compact('customer'));
    }

    public function updateProfile(Request $req){
    	$customer = User::find(Auth::user()->id);
		$customer->name = $req->name;
		$customer->username = $req->username;
		$customer->email = $req->email;
		if (is_null($req->password) === false) {
            $customer -> password = bcrypt($req->password);   
        }
		$customer->alamat = $req->alamat;
		$customer->kota = $req->kota;
		$customer->provinsi = $req->provinsi;
		$customer->kode_pos = $req->kode_pos;
		$customer->telepon = $req->telepon;
		if ($customer->save()) {
			flash('Profile Berhasil Diubah...')->success();
		} else {
			flash('Profile Tidak Berhasil Diubah...')->error();
		}

		return redirect(route('profileCustomer'));
    }

    

    public function konfirmasi(){
    	
    }
}
