<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
	public $incrementing = false;
    protected $primaryKey = 'id_pesan';

    public function detail(){
    	return $this->hasMany(PesananDetail::class,'id_pesan','id_pesan');
    }

    public function konfirmasi(){
    	return $this->belongsTo(Konfirmasi::class,'id_pesan','id_pesan');
    }

    public function pembayaran(){
    	return $this->belongsTo(Pembayaran::class,'id_pesan','id_pesan');
    }

    public function customer(){
        return $this->belongsTo(User::class,'id_customer','id');
    }

    public function jarak(){
        return $this->belongsTo(Jarak::class,'id_jarak','id_jarak');
    }

    public function pengiriman(){
        return $this->hasOne(Pengiriman::class,'id_pesan','id_pesan');
    }
}
