<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
	protected $primaryKey = 'id_produk';

    public function jenis(){
    	return $this->belongsTo(ProdukJenis::class,'id_jenis','id_jenis');
    }

    public function toko(){
    	return $this->belongsTo(User::class,'id_toko','id');
    }

    public function pesananDetail(){
    	return $this->hasMany(PesananDetail::class,'id_produk','id_produk');
    }
}
