<?php

namespace App;

use Auth;
use App\Role;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password','role_id','alamat','kota', 'provinsi', 'kode_pos','telepon'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function hasRole($role_name){
        $role_id = Role::where('name', $role_name)->first()->id;
        if (Auth::user()->role_id == $role_id) {
            return true;
        } else {
            return false;
        }
        // return $this->belongsTo(Role::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }
}
