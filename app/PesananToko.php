<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesananToko extends Model
{
    protected $primaryKey = 'id_pesanan_toko';

    public function pesanan(){
        return $this->belongsTo(Pesanan::class,'id_pesan','id_pesan');
    }

    public function toko(){
        return $this->belongsTo(User::class,'id_toko','id');
    }

    public function detail(){
        return $this->hasMany(PesananTokoDetail::class,'id_pesanan_toko','id_pesanan_toko');
    }
}
