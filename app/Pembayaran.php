<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
	public $incrementing = false;
    protected $primaryKey = 'id_bayar';
    protected $fillable = ['tanggal_bayar','no_bukti','jumlah_bayar','status_kirim'];

    public function pesanan(){
    	return $this->belongsTo(Pesanan::class,'id_pesan','id_pesan');
    }

    public function pengiriman(){
    	return $this->belongsTo(Pengiriman::class,'id_bayar','id_bayar');
    }
}
