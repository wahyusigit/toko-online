<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesananDetail extends Model
{
    protected $primaryKey = 'id_det';

    public function produk(){
    	return $this->belongsTo(Produk::class,'id_produk','id_produk');
    }

    public function pesanan(){
    	return $this->belongsTo(Pesanan::class,'id_pesan','id_pesan');
    }
}
