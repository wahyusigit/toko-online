<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesananTokoDetail extends Model
{
    protected $primaryKey = 'id_pesanan_toko_det';

    public function detail(){
    	return $this->hasOne(PesananDetail::class,'id_det','id_det');
    }
}
