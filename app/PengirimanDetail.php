<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengirimanDetail extends Model
{
    public function detail(){
        return $this->belongsTo(PesananDetail::class,'id_det','id_det');
    }
}
