<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonis', function (Blueprint $table) {
            $table->increments('id_testimoni');
            $table->unsignedInteger('id_customer');
            $table->unsignedInteger('id_produk');
            $table->text('ulasan');
            $table->dateTime('tanggal');
            $table->enum('tingkat_kepuasan',[1,2,3,4,5])->default(3);
            $table->enum('status_baca',['sudah','belum'])->default('belum');
            $table->timestamps();

            $table->foreign('id_customer')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_produk')->references('id_produk')->on('produks')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonis');
    }
}
