<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produks', function (Blueprint $table) {
            $table->increments('id_produk');
            $table->unsignedInteger('id_jenis');
            $table->unsignedInteger('id_toko');
            $table->string('nama_prod',50);
            $table->integer('harga_prod');
            $table->integer('berat_prod');
            $table->text('keterangan_prod');
            $table->binary('image_prod');
            $table->tinyInteger('stok_prod')->default(1);
            $table->enum('status_prod',['ada','tidak'])->default('ada');
            $table->enum('rating_prod',[0,1,2,3,4,5])->default(0);
            $table->timestamps();
            
            $table->foreign('id_jenis')->references('id_jenis')->on('produk_jenis')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_toko')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produks');
    }
}
