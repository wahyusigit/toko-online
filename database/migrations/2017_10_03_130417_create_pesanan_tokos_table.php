<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananTokosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan_tokos', function (Blueprint $table) {
            $table->increments('id_pesanan_toko');
            $table->string('id_pesan',16);
            $table->unsignedInteger('id_toko');            
            $table->unsignedInteger('id_customer'); 
            $table->integer('ongkos_kirim');
            $table->integer('total_akhir');
            $table->enum('status_kirim',['sudah','belum'])->default('belum');
            $table->timestamps();

            $table->foreign('id_pesan')->references('id_pesan')->on('pesanans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_toko')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_customer')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan_tokos');
    }
}
