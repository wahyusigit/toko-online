<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanans', function (Blueprint $table) {
            // $table->increments('id');
            $table->string('id_pesan',16)->primary();
            $table->unsignedInteger('id_customer')->nullable();
            $table->unsignedInteger('id_jarak')->nullable();
            $table->date('tanggal_pesan');
            $table->string('keterangan_pesan',200)->nullable();
            $table->integer('total_bayar');
            $table->string('tujuan',100);
            $table->string('kota',32);
            $table->string('provinsi',32);
            $table->string('kode_pos',7);
            $table->string('telepon',14);
            $table->integer('ongkos_kirim');
            $table->timestamps();

            $table->foreign('id_customer')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_jarak')->references('id_jarak')->on('jaraks')->onDelete('cascade')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanans');
    }
}
