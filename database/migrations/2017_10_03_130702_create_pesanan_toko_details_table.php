<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananTokoDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan_toko_details', function (Blueprint $table) {
            $table->increments('id_pesanan_toko_det');
            $table->unsignedInteger('id_pesanan_toko');
            $table->unsignedInteger('id_det');
            $table->integer('subtotal_beli');
            $table->timestamps();

            $table->foreign('id_pesanan_toko')->references('id_pesanan_toko')->on('pesanan_tokos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_det')->references('id_det')->on('pesanan_details')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan_toko_details');
    }
}
