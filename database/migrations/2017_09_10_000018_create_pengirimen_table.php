<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengirimenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengirimen', function (Blueprint $table) {
            $table->string('id_kirim',16)->primary();
            $table->string('id_pesan',16);
            $table->unsignedInteger('id_toko');
            $table->unsignedInteger('id_customer');
            $table->date('tanggal_kirim');
            $table->date('tanggal_diterima')->nullable();
            $table->string('no_resi',32)->nullable();
            $table->enum('status_diterima',['sudah','belum'])->default('belum');
            $table->timestamps();

            $table->foreign('id_pesan')->references('id_pesan')->on('pesanans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_toko')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_customer')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengirimen');
    }
}
