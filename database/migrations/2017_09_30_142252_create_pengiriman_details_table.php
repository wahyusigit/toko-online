<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengirimanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman_details', function (Blueprint $table) {
            $table->increments('id_pengiriman_detail');
            $table->string('id_kirim',16);
            $table->unsignedInteger('id_produk');
            $table->unsignedInteger('id_det');
            $table->timestamps();

            $table->foreign('id_kirim')->references('id_kirim')->on('pengirimen')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_produk')->references('id_produk')->on('produks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_det')->references('id_det')->on('pesanan_details')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman_details');
    }
}
