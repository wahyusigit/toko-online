<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesananDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesanan_details', function (Blueprint $table) {
            $table->increments('id_det');
            $table->string('id_pesan',16);
            $table->unsignedInteger('id_produk');
            $table->unsignedInteger('id_toko');
            $table->string('id_kirim',10)->nullable();
            $table->integer('harga_satuan');
            $table->tinyInteger('jumlah_barang');
            $table->integer('subtotal');
            $table->enum('status_testimoni',['sudah','belum'])->default('belum');
            $table->enum('status_kirim',['sudah','belum'])->default('belum');
            $table->enum('status_diterima',['sudah','belum'])->default('belum');
            $table->timestamps();

            $table->foreign('id_pesan')->references('id_pesan')->on('pesanans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_produk')->references('id_produk')->on('produks')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_toko')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesanan_details');
    }
}
