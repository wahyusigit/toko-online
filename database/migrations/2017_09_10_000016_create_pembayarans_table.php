<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayarans', function (Blueprint $table) {
            // $table->increments('id_bayar');
            $table->string('id_bayar',16)->primary();
            $table->string('id_pesan',16)->unique();
            $table->enum('status_verifikasi',['sudah','belum'])->default('belum');
            $table->date('tanggal_transfer');
            $table->date('tanggal_verifikasi')->nullable();
            $table->string('no_bukti',16);
            $table->binary('image_no_bukti')->nullable();
            $table->integer('jumlah_bayar');
            $table->text('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('id_pesan')->references('id_pesan')->on('pesanans')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayarans');
    }
}
