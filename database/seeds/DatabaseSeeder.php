<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\Jarak;
use App\Produk;
use App\ProdukJenis;
use App\Halaman;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $halaman = new Halaman();
        $halaman -> slug = "cara-pemesanan";
        $halaman -> halaman = "cara pemesanan";
        $halaman -> konten = "Cara Pemesanan";
        $halaman -> save();
        
        $halaman = new Halaman();
        $halaman -> slug = "cara-pembayaran";
        $halaman -> halaman = "cara pembayaran";
        $halaman -> konten = "Cara Pembayaran";
        $halaman -> save();

        $halaman = new Halaman();
        $halaman -> slug = "tentang-kami";
        $halaman -> halaman = "tentang kami";
        $halaman -> konten = "Tentang Kami";
        $halaman -> save();

        $halaman = new Halaman();
        $halaman -> slug = "hubungi-kami";
        $halaman -> halaman = "hubungi kami";
        $halaman -> konten = "Hubungi Kami";
        $halaman -> save();
        
    	$role_admin = new Role();
    	$role_admin->name = 'admin';
    	$role_admin->description = 'Admin';
    	$role_admin->save();

    	$role_toko = new Role();
    	$role_toko->name = 'toko';
    	$role_toko->description = 'Toko';
    	$role_toko->save(); 

    	$role_customer = new Role();
    	$role_customer->name = 'customer';
    	$role_customer->description = 'Customer';
    	$role_customer->save();


        $user_admin = new User();
        $user_admin -> role_id = $role_admin->id;
        $user_admin -> username = "admin";
        $user_admin -> name = "admin";
        $user_admin -> email = "admin@admin.com";
        $user_admin -> password = bcrypt("admin");
        $user_admin -> save();

        $user_toko1 = new User();
        $user_toko1 -> role_id = $role_toko->id;
        $user_toko1 -> username = "toko1";
        $user_toko1 -> name = "toko1";
        $user_toko1 -> email = "toko1@toko1.com";
        $user_toko1 -> password = bcrypt("toko1");
        $user_toko1 -> save();

        $user_toko2 = new User();
        $user_toko2 -> role_id = $role_toko->id;
        $user_toko2 -> username = "toko2";
        $user_toko2 -> name = "toko2";
        $user_toko2 -> email = "toko2@toko2.com";
        $user_toko2 -> password = bcrypt("toko2");
        $user_toko2 -> save();

        $user_user1 = new User();
        $user_user1 -> role_id = $role_customer->id;
        $user_user1 -> username = "customer1";
        $user_user1 -> name = "customer1";
        $user_user1 -> email = "customer1@customer1.com";
        $user_user1 -> password = bcrypt("customer1");
        $user_user1 -> alamat = "Karang";
        $user_user1 -> kota = "Klaten";
        $user_user1 -> provinsi = "Jawa Tengah";
        $user_user1 -> kode_pos = "19818";
        $user_user1 -> telepon = "09128912928";
        $user_user1 -> save();

        $user_user2 = new User();
        $user_user2 -> role_id = $role_customer->id;
        $user_user2 -> username = "customer2";
        $user_user2 -> name = "customer2";
        $user_user2 -> email = "customer2@customer2.com";
        $user_user2 -> password = bcrypt("customer2");
        $user_user2 -> alamat = "Karang";
        $user_user2 -> kota = "Klaten";
        $user_user2 -> provinsi = "Jawa Tengah";
        $user_user2 -> kode_pos = "19818";
        $user_user2 -> telepon = "09128912928";
        $user_user2 -> save();

        

        for ($i = 1 ; $i <= 99 ; $i++) {
            $faker = Faker\Factory::create('id_ID');
            $customer = new User();
            $customer -> role_id = $role_customer->id;
            $customer -> username = $faker->userName;
            $customer -> name = $faker->name;
            $customer -> email = $faker->email;
            $customer -> password = bcrypt($faker->userName);
            $customer -> alamat = $faker->streetAddress;
            $customer -> kota = $faker->city;
            $customer -> provinsi = "kosong";
            $customer -> kode_pos = $faker->postcode;
            $customer -> telepon = $faker->e164PhoneNumber;
            $customer -> save();
        }

        $jaraks = [     
                    "Yogyakarta"=>2000,
                    "Bandung"=>5000,
                    "Padang"=>15000,
                    "Jakarta"=>10000,
                    "Surabaya"=>8000,
                    "Riau"=>2000,
                    "Papua"=>5000,
                    "riau"=>15000,
                    "Sulawesi Tenggara"=>10000,
                    "Surabaya"=>8000,
                    "Bali"=>5000,
                    "Banten"=>5000,
                    "DKI"=>5000,
                    "Jawa Barat"=>5000,
                    "Jawa Tengah"=>5000,
                    "Jawa Timur"=>5000,
                    "DIY"=>5000,
                    "NTB"=>5000
                ];

        foreach ($jaraks as $key => $value) {
            $j = new Jarak();
            $j -> tujuan = $key;
            $j -> ongkos = $value;
            $j -> save();
        }

        $produk_jenis = new ProdukJenis();
        $produk_jenis -> nama_jenis = "Batik";
        $produk_jenis -> save();

        $produk_jenis = new ProdukJenis();
        $produk_jenis -> nama_jenis = "Makanan";
        $produk_jenis -> save();

        // $produk_jenis = new ProdukJenis();
        // $produk_jenis -> nama_jenis = "Kalung";
        // $produk_jenis -> save();

    }
}
