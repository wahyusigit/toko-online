<table class="table table-striped">
	<thead> 
		<tr>
			<th class="col-md-4">Nama Produk</th>
			<th class="text-center">Berat (Gram)</th>
			<th class="text-center col-md-1">Qty</th>
			<th class="text-right col-md-2">Harga</th>
			<th class="text-right col-md-2">Subtotal</th>
			<th class="text-center col-md-1">Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($carts as $cart)
		<tr>
			<td class="text-left text-capitalize">{{ $cart->name }}</td>
			<td class="text-center">{{ $cart->options->berat_prod }}</td>
			<td class="text-center">{{ $cart->qty }}</td>
			<td class="text-right">{{ number_format($cart->price,0,",",".") }}</td>
			<td class="text-right">{{ number_format($cart->subtotal,0,",",".") }}</td>
			<td class="text-center">
				<a href="{{ route('removeCart', $cart->rowId) }}" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-trash"></i></a>
			</td>

		</tr>
		@endforeach
		<tr>
			<td colspan="4"></td>
			<td class="text-right"><b>Total Pembelian</b></td>
			<td class="text-right"><b>{{ Cart::total() }}</b></td>
			<td></td>
		</tr>
        <tr>
            <td colspan="7" class="text-right text-red"><b><i>Informasi : Total Pembelian belum termasuk Onkos Kirim</i></b></td>
        </tr>
	</tbody>
</table>