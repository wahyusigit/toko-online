<div class="col-item">
    <div class="photo">
    	<a href="{{ route('indexProdukDetailHomepage',$produk->id_produk) }}" class="hidden-sm">
        <img src="{{ asset($produk->image_prod) }}" class="img-responsive" alt="a" />
    	</a>
    </div>
    <div class="info">
        <div class="row">
            <div class="price col-md-12">
            	<a href="{{ route('indexProdukDetailHomepage',$produk->id_produk) }}" class="hidden-sm">
                <h5>{{ $produk->nama_prod }}</h5>
            	</a>
                <div class="row">
                	<div class="col-md-6">
                    <div class="price-text text-red"><b>Rp. {{ number_format($produk->harga_prod,0,",",".") }}</b></div>
                    </div>
                    <div class="col-md-6">
                    <div class="rating hidden-sm">
                        @for($i=1;$i<=5;$i++)
                        	@if($i <= $produk->rating_prod)
                            <i class="price-text-color fa fa-star"></i>
                            @else
                            <i class="fa fa-star"></i>
                            @endif
                        @endfor
                	</div>
                	</div>
            	</div>
            </div>
            
        </div>
        <div class="separator clear-left">
            <p class="btn-add text-succes">
                <i class="fa fa-shopping-cart text-green"></i><a href="{{ route('addCart', $produk->id_produk) }}" class="hidden-sm text-green">Beli</a></p>
            <p class="btn-details">
                <i class="fa fa-list"></i><a href="{{ route('indexProdukDetailHomepage',$produk->id_produk) }}" class="hidden-sm">Deskripsi</a></p>
        </div>
        <div class="clearfix">
        </div>
    </div>
</div>