@extends('layouts.landing')
@section('htmlheader_title', 'Produk ' . ucfirst($jenis->nama_jenis))
@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-3">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Kategori Produk</h3>
					</div>
					<div class="box-body">
						<div class="btn-group-vertical btn-block ">
						@foreach($produk_jenis as $jns)
                        @if($jns->produk->isEmpty() === false)
							<a href="{{ route('indexProdukCategoryHomepage', $jns->id_jenis) }}" class="btn btn-default btn-flat btn-sm">{{ $jns->nama_jenis }}</a>
                        @endif
						@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="box">
						<div class="box-header with-border">
							<div class="box-title">{{ $jenis->nama_jenis }}</div>
						</div>
						<div class="box-body">
						@if($produks->isEmpty())
							<h4 class="text-center">Data Produk dengan Kategori {{ $jenis->nama_jenis }} Tidak Ada</h4>
						@else
							@foreach($produks as $produk)
							<div class="col-md-4">
				                @include('partials.produk_item')
				            </div>
				        	@endforeach
			        	@endif
			        	</div>
                        <div class="box-footer">
                            <div class="pull-right">
                                {{ $produks->links() }}
                            </div>
                        </div>
			        </div>
				</div>
			</div>
		</div>
	</div>


<style type="text/css">
.col-item
{
    border: 1px solid #E1E1E1;
    border-radius: 5px;
    background: #FFF;
    margin-top: 10px;
    margin-bottom: 10px;
}
.col-item .photo img
{
    margin: 0 auto;
    width: 100%;
}

.col-item .info
{
    padding: 10px;
    border-radius: 0 0 5px 5px;
    margin-top: 1px;
}

.col-item:hover .info {
    background-color: #f8f8f8;
}
.col-item .price
{
    /*width: 50%;*/
    float: left;
    margin-top: 5px;
}

.col-item .price h5
{
    line-height: 20px;
    margin: 0;
}

.price-text {
	margin: 16px 0px;
}

.price-text-color
{
    color: #219FD1;
    margin: 16px 0px;
}

.col-item .info .rating
{
    color: #777;
}

.col-item .rating
{
    /*width: 50%;*/
    float: left;
    font-size: 17px;
    text-align: right;
    line-height: 52px;
    margin-bottom: 10px;
    height: 52px;
}

.col-item .separator
{
    border-top: 1px solid #E1E1E1;
}

.clear-left
{
    clear: left;
}

.col-item .separator p
{
    line-height: 20px;
    margin-bottom: 0;
    margin-top: 10px;
    text-align: center;
}

.col-item .separator p i
{
    margin-right: 5px;
}
.col-item .btn-add
{
    width: 50%;
    float: left;
}

.col-item .btn-add
{
    border-right: 1px solid #E1E1E1;
}

.col-item .btn-details
{
    width: 50%;
    float: left;
    padding-left: 10px;
}
.controls
{
    margin-top: 20px;
}
[data-slide="prev"]
{
    margin-right: 10px;
}
		

</style>
@endsection

