@extends('layouts.toko')
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Halaman Pemesanan</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <legend>Pesanan</legend>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col-md-5">Kode Pesan</th>
                                            <td><b>{{ $pesanan->id_pesan }}</b></td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal Pesan</th>
                                            <td><b>{{ $pesanan->tanggl_pesan }}</b></td>
                                        </tr>
                                        <tr>
                                            <th>Total Beli</th>
                                            <td><b>{{ $pesanan->total_bayar }}</b></td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <legend>Konfirmasi Bayar</legend>
                                <form action="{{ route('postKonfirmasiCustomer', $pesanan->id_pesan) }}" method="POST" role="form">
                                    {{ csrf_field() }}
                                    @if(is_null($pesanan->konfirmasi))
                                    <div class="form-group">
                                        <label for="">Tanggal Bayar</label>
                                        <input name="tanggal_konfirmasi" type="date" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">No. Bukti</label>
                                        <input name="no_bukti" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jumlah Transfer</label>
                                        <input name="jumlah_bayar" type="number" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Keterangan</label>
                                        <textarea name="keterangan" class="form-control"></textarea>
                                    </div>
                                    @else
                                    <div class="form-group">
                                        <label for="">Tanggal Bayar</label>
                                        <input name="tanggal_konfirmasi" type="date" class="form-control" value="{{ $konfirmasi->tanggal_konfirmasi }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">No. Bukti</label>
                                        <input name="no_bukti" type="text" class="form-control" value="{{ $konfirmasi->no_bukti }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Jumlah Transfer</label>
                                        <input name="jumlah_bayar" type="number" class="form-control" value="{{ $konfirmasi->jumlah_bayar }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Keterangan</label>
                                        <textarea name="keterangan" class="form-control">{{ $konfirmasi->keterangan }}</textarea>
                                    </div>
                                    @endif
                                    <button type="submit" class="btn btn-primary pull-right">Konfirmasi</button>
                                </form>
                            </div>
                        </div>
                        <hr>
                        <legend>Detail Pesanan</legend>
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-2 text-center">Nama Produk</th>
                                    <th class="col-md-2 text-center">Jumlah Beli</th>
                                    <th class="col-md-2 text-center">Harga Satuan</th>
                                    <th class="col-md-2 text-center">Subtotal</th>
                                    <th class="col-md-3 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pesanan_details as $no => $pesanan_detail)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-left"> {{ $pesanan_detail->produk->nama_prod }} </td>
                                    <td class="text-center"> {{ $pesanan_detail->harga_satuan }} </td>
                                    {{-- <td> {{ $pesanan_detail->keterangan_pesan }} </td> --}}
                                    <td class="text-center text-capitalize"> {{ $pesanan_detail->jumlah_barang }} </td>
                                    <td class="text-center text-capitalize"> {{ $pesanan_detail->harga_satuan * $pesanan_detail->jumlah_barang }} </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search"></i>  Lihat Gambar</a>
                                            <a href="{{ route('deletePesananDetailCustomer', $pesanan_detail->id_det) }}" class="btn btn-flat btn-danger btn-sm"><i class="fa fa-trash"></i>  Hapus</a>
                                            {{-- <button class="btn btn-flat btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-flat btn-danger btn-sm"><i class="fa fa-trash"></i></button> --}}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
@endsection
