@extends('layouts.toko')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Halaman Pemesanan</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-2 text-center">ID Pesan</th>
                                    <th class="col-md-2 text-center">Tanggal Pesan</th>
                                    <th class="col-md-2 text-center">Status<br>Konfirmasi</th>
                                    <th class="col-md-2 text-center">Total<br>Beli</th>
                                    <th class="col-md-2 text-center">Jumlah<br>Transfer</th>
                                    <th class="col-md-3 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pesanans as $no => $pesanan)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-left"> {{ $pesanan->id_pesan }} </td>
                                    <td class="text-center"> {{ $pesanan->tanggal_pesan }} </td>
                                    <td class="text-center text-capitalize">
                                        @if(is_null($pesanan->konfirmasi))
                                        Belum
                                        @else
                                        Sudah
                                        @endif
                                    </td>
                                    <td class="text-center"> {{ $pesanan->total_bayar }} </td>
                                    <td class="text-center">
                                        @if(is_null($pesanan->konfirmasi))
                                        -
                                        @else
                                        {{ $pesanan->konfirmasi->jumlah_bayar }}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('completeKonfirmasiCustomer', $pesanan->id_pesan) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search"></i>  Konfirmasi</a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <button type="button" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-sign-out"></i> Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
