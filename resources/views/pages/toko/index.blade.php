@extends('layouts.toko')
@section('htmlheader_title','Toko')
@section('main-content')
        @if(is_null($toko->no_rekening_toko))
        <div class="row">
            <div class="col-md-12">
                <div class="callout callout-info">
                    <h4>Informasi !</h4>
                    <p>Untuk mengubah Deskripsi, Nama Bank dan No Rekening silahkan menuju halaman Profile.</p>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header">
                        <div class="box-title">
                            Informasi Toko
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                              <label>Nama Pemilik Toko</label>
                              <input name="name" type="text" class="form-control text-capitalize" value="{{ $toko->name }}" readonly="readonly">
                            </div>
                            <div class="form-group">
                              <label>Deskripsi</label>
                              <textarea name="deskripsi_toko" type="text" class="form-control text-capitalize" readonly="readonly" rows="4">{{ $toko->deskripsi_toko }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Bank</label>
                                <input name="nama_bank" type="text" class="form-control text-capitalize" value="{{ $toko->nama_bank }}" readonly="readonly">
                            </div>
                            <div class="form-group">
                                <label>No. Rekening</label>
                                <input name="no_rekening_bank" type="text" class="form-control text-capitalize" value="{{ $toko->no_rekening_bank }}" readonly="readonly">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <button type="button" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-sign-out"></i> Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
