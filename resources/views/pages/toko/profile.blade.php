@extends('layouts.toko')
@section('htmlheader_title','Profile')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Data Toko</h3>
                    </div>
                    <form role="form" method="POST" action="{{ route('updateProfileToko') }}">
                    {{ csrf_field() }}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label>Nama Pemilik Toko</label>
                                      <input name="name" type="text" class="form-control text-capitalize" value="{{ $toko->name }}">
                                    </div>
                                    <div class="form-group">
                                      <label>Deskripsi</label>
                                      <textarea name="deskripsi_toko" type="text" class="form-control text-capitalize" rows="4">{{ $toko->deskripsi_toko }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Bank</label>
                                        <input name="nama_bank" type="text" class="form-control text-capitalize" value="{{ $toko->nama_bank }}">
                                    </div>
                                    <div class="form-group">
                                        <label>No. Rekening</label>
                                        <input name="no_rekening_bank" type="text" class="form-control text-capitalize" value="{{ $toko->no_rekening_bank }}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    {{-- <div class="form-group">
                                      <label>Nama lengkap</label>
                                      <input name="name" type="text" class="form-control text-capitalize" value="{{ $toko->name }}">
                                    </div> --}}
                                    <div class="form-group">
                                      <label>Username</label>
                                      <input name="username" type="text" class="form-control text-lowercase" value="{{ $toko->username }}">
                                    </div>
                                    <div class="form-group">
                                      <label>E-Mail</label>
                                      <input name="email" type="email" class="form-control text-lowercase" value="{{ $toko->email }}">
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <input id="password" type="password" class="form-control" disabled="disabled">
                                                <span class="input-group-btn">
                                                  <button id="enable_new_password" type="button" class="btn btn-info btn-flat">Password Baru</button>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Alamat</label>
                                        <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat">{{ $toko->alamat }}</textarea>
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Kota</label>
                                        <input id="kota" name="kota" type="text" class="form-control" value="{{ $toko->kota }}" placeholder="Kota">
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Provinsi</label>
                                        <input id="provinsi" name="provinsi" type="text" class="form-control" value="{{ $toko->provinsi }}" placeholder="Provinsi">
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Kode POS</label>
                                        <input id="kode_pos" name="kode_pos" type="text" class="form-control" value="{{ $toko->kode_pos }}" placeholder="Kode POS">
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label>Telepon</label>
                                        <input id="telepon" name="telepon" type="text" class="form-control" value="{{ $toko->telepon }}" placeholder="Telepon">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="pull-left">
                                <a href="{{ route('indexToko') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <button type="button" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-sign-out"></i> Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script')
<script type="text/javascript">
    $('body').on('click','#enable_new_password', function(){
        $("#password").prop('disabled', false);
    });
</script>
@endpush