@extends('layouts.toko')
@section('htmlheader_title','Detail Pengiriman')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pengiriman <span class="text-capitalize">{{ $pengiriman->pesanan->customer->name }} </span> ({{ $pengiriman->pesanan->id_pesan }})  </h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nama Customer</label>
                                            <input value="{{ $pengiriman->pesanan->customer->name }}" type="text" name="" class="form-control text-capitalize" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tanggal Pesan</label>
                                            <input value="{{ $pengiriman->pesanan->tanggal_pesan }}" type="text" name="" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tanggal Pembayaran</label>
                                            <input value="{{ $pengiriman->pesanan->pembayaran->tanggal_transfer }}" type="text" name="" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ID Kirim</label>
                                            <input value="{{ $pengiriman->id_kirim }}" type="text" name="" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ID Pesan</label>
                                            <input value="{{ $pengiriman->pesanan->id_pesan }}" type="text" name="" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tanggal Kirim</label>
                                            <input value="{{ $pengiriman->tanggal_kirim }}" type="text" name="" class="form-control" readonly="readonly">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Status Diterima</label>
                            <input value="{{ $pengiriman->status_diterima }}" type="text" name="" class="form-control text-capitalize" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label>No. Resi</label>
                            <input value="{{ $pengiriman->no_resi }}" type="text" name="" class="form-control" readonly="readonly">
                        </div>
                        <div class="form-group pull-right">
                            <a href="{{ route('editPengirimanToko', $pengiriman->id_kirim) }}" class="btn btn-primary btn-flat"><i class="fa fa-edit"></i>   Edit</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pengiriman Detail </h3>
                    </div>
                    <div class="box-body">
                        
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-5 text-center">Nama Produk</th>
                                    <th class="col-md-2 text-center">Jumlah<br>Beli</th>
                                    <th class="col-md-1 text-center">Harga<br>Satuan</th>
                                    <th class="col-md-2 text-center">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pengiriman->pesanantoko->detail as $no => $detail)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-left text-capitalize"> {{ $detail->detail->produk->nama_prod }} </td>
                                    <td class="text-center text-capitalize"> {{ $detail->detail->jumlah_barang }} </td>
                                    <td class="text-right"> {{ number_format($detail->detail->harga_satuan,0,',','.') }} </td>
                                    <td class="text-right"> {{ number_format($detail->detail->subtotal,0,',','.') }} </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="4" class="text-right"><b>Total Beli</b></td>
                                    <td class="text-right"><b>{{ number_format($pengiriman->pesanantoko->detail->sum('subtotal_beli'),0,',','.') }}</b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right"><b>Ongkos Kirim</b></td>
                                    <td class="text-right"><b>{{ number_format($pengiriman->pesanantoko->ongkos_kirim,0,',','.') }}</b></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right"><b>Total Akhir</b></td>
                                    <td class="text-right"><b>{{ number_format($pengiriman->pesanantoko->total_akhir,0,',','.') }}</b></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
