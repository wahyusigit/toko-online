@extends('layouts.toko')
@section('htmlheader_title','Pengiriman')
@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Halaman Pengiriman</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="col-md-2 text-center">Nama Customer</th>
                            <th class="text-center">ID Kirim</th>
                            <th class="text-center">ID Pesan</th>
                            <th class="text-center">Tanggal Kirim</th>
                            <th class="text-center">No. Resi</th>
                            <th class="col-md-3 text-center">Detail Produk</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pengirimans as $no => $pengiriman)
                        {{-- {{ dd($pengiriman->detail->count()) }} --}}
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-capitalize">{{ $pengiriman->pesanan->customer->name }}</td>
                            <td class="text-uppercase">{{ $pengiriman->id_kirim }}</td>
                            <td class="text-uppercase">{{ $pengiriman->id_pesan }}</td>
                            <td class="text-center">{{ $pengiriman->tanggal_kirim }}</td>
                            <td> {{ $pengiriman->no_resi }}</td>
                            <td>
                                @foreach($pengiriman->detail as $detail)
                                {{ $detail->detail->produk->nama_prod }} , {{ $detail->detail->jumlah_barang }}pcs , 

                                @if($detail->detail->status_diterima == 'sudah')
                                    <span class="label label-success">Sudah Diterima</span>
                                @else
                                    <span class="label label-danger">Belum Diterima</span>
                                @endif

                                <br><hr>
                                @endforeach
                            </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('detailPengirimanToko',$pengiriman->id_kirim) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-search"></i></a>
                                    <a href="{{ route('editPengirimanToko',$pengiriman->id_kirim) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a>
                                </div>
                            </td>
                            
                        </tr>
                        @endforeach
                </table>
                {{ $pengirimans->links() }}
            </div>
        </div>
    </div>
    <div class="col-md-3">
    </div>
</div>
@endsection