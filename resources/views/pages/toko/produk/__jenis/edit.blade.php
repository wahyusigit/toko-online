@extends('layouts.toko')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Jenis Produk</h3>
                    </div>
                    <div class="box-body">
                        <form role="form" method="POST" action="{{ route('updateProdukJenisAdmin', $jenis->id_jenis) }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-5">
                                      <label>Nama Jenis Produk</label>
                                      <input name="nama_jenis" type="text" class="form-control text-capitalize" value="{{ $jenis->nama_jenis }}">
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexProdukJenisAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <button type="submit" class="btn btn-default btn-flat btn-sm {{ Request::is('admin/data-admin/add') ? 'disabled' : '' }}">Tambah Jenis Produk</button>
                            <a href="{{ route('indexProdukAdmin') }}" class="btn btn-primary btn-flat btn-sm">Kembali ke Produk</a>
                            {{-- <button type="button" class="btn btn-default btn-flat btn-sm">Ganti Password</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
