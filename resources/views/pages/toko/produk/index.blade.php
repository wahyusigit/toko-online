@extends('layouts.toko')
@section('htmlheader_title','Produk')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Produk</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-3 text-center">Jenis</th>
                                    <th class="col-md-3 text-center">Produk</th>
                                    <th class="col-md-2 text-center">Harga</th>
                                    <th class="col-md-1 text-center">Berat<br>(Gram)</th>
                                    <th class="col-md-2 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($produks as $no => $produk)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-capitalize">{{ $produk->jenis->nama_jenis }}</td>
                                    <td class="text-capitalize">{{ $produk->nama_prod }}</td>
                                    <td class="text-right">{{ number_format($produk->harga_prod,0,",",".") }}</td>
                                    <td class="text-right">{{ number_format($produk->berat_prod,0,",",".") }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('showProdukToko',$produk->id_produk) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-search"></i></a>
                                            <a href="{{ route('editProdukToko',$produk->id_produk) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('deleteProdukToko',$produk->id_produk) }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addProdukToko') }}" class="btn btn-default btn-flat btn-sm">Tambah Data Produk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
