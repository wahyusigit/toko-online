@extends('layouts.toko')
@section('htmlheader_title','Pesanan')
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesanan</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="col-md-2 text-center">Nama Customer</th>
                                    <th class="col-md-2 text-center">ID Pesan</th>
                                    <th class="col-md-2 text-center">Tanggal<br>Pesan</th>
                                    <th class="col-md-2 text-center">Ongkos<br>Kirim</th>
                                    <th class="col-md-2 text-center">Total<br>Beli</th>
                                    <th class="col-md-2 text-center">Total<br>Akhir</th>
                                    <th class="col-md-2 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pesanan_tokos as $no => $pesanan)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-center text-capitalize"> {{ $pesanan->pesanan->customer->name }} </td>
                                    <td class="text-left"> {{ $pesanan->pesanan->id_pesan }} </td>
                                    <td class="text-center"> {{ $pesanan->pesanan->tanggal_pesan }} </td>
                                    {{-- <td class="text-center"> {{ $pesanan->pesanan->keterangan_pesan }} </td> --}}
                                    <td class="text-center"> {{ number_format($pesanan->ongkos_kirim,0,",",".") }} </td>
                                    <td class="text-center"> {{ number_format($pesanan->detail->sum('subtotal_beli'),0,",",".") }} </td>
                                    <td class="text-center"> {{ number_format($pesanan->total_akhir,0,",",".") }} </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('detailPesananToko', $pesanan->id_pesanan_toko) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
        </div>
@endsection
