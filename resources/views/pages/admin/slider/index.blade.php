@extends('layouts.admin')
@section('htmlheader_title','Data Slider')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Slider</h3>
                    </div>
                    <div class="box-body">
                        @foreach($sliders as $slider)
                        <div class="media">
                            <div class="media-left">
                                <a href="">
                                    <img src="{{ asset($slider->image_slider) }}" class="media-object" style="width: 150px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h4 style="margin-top: 0">{{ $slider->judul_slider }}</h4>

                                        <p>{{ $slider->deskripsi_slider }}</p>
                                        {{-- <p style="margin-bottom: 0">
                                            <i class="fa fa-shopping-cart margin-r5"></i> 41k+ purchases
                                        </p> --}}
                                        </div>
                                        <div class="col-md-2">
                                            <div class="btn-group-vertical btn-block">
                                                <a href="{{ route('editSliderAdmin', $slider->id_slider) }}" class="btn btn-default btn-flat btn-sm"><i class="fa fa-edit"></i>  Edit</a>
                                                <a href="{{ route('deleteSliderAdmin', $slider->id_slider) }}" class="btn btn-default btn-flat btn-sm"><i class="fa fa-trash"></i>  Hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                        {{ $sliders->links() }}
                    </div>
                    <hr>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addSliderAdmin') }}" class="btn btn-default btn-flat btn-sm">Tambah Slider</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
