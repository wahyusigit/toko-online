@extends('layouts.admin')
@section('htmlheader_title','Ubah Slider')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Slider</h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" role="form" method="POST" action="{{ route('updateSliderAdmin', $slider->id_slider) }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                      <label>Judul Slider</label>
                                      <input name="judul_slider" type="text" class="form-control text-capitalize" required="required" value="{{ $slider->judul_slider }}">
                                    </div>
                                    <div class="form-group col-md-3 col-md-offset-3">
                                      <label>Pilih Produk</label>
                                      <select class="form-control text-capitalize" name="id_produk">
                                          @foreach($produks as $no => $produk)
                                          <option class="text-capitalize" value="{{ $produk->id_produk }}">{{ $no+1 }}. {{ $produk->nama_prod }}</option>
                                          @endforeach
                                      </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                      <label>Deskripsi</label>
                                      <textarea rows="4" class="form-control" name="deskripsi_slider" required="required">{{ $slider->deskripsi_slider }}</textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Gambar Slider</label>
                                        <img class="img-responsive img-thumbnail" src="{{ asset($slider->image_slider) }}">
                                        <input name="image_slider" type="file" class="form-control">
                                        <p class="help-block">Siapkan Gambar Slider dengan Ukuran 1140px X 400px.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexSliderAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addSliderAdmin') }}" class="btn btn-default btn-flat btn-sm {{ Request::is('*produk/add') ? 'disabled' : '' }}">Tambah Slider</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script')
<script type="text/javascript">
    $('body').on('click','#enable_new_image_prod', function(){
        $("#image_prod").prop('disabled', false);
    });
</script>
@endpush