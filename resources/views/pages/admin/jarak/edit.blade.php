@extends('layouts.admin')
@section('htmlheader_title','Ubah Data Jarak Ongkos Kirim')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Jenis Produk</h3>
                    </div>
                    <div class="box-body">
                        <form role="form" method="POST" action="{{ route('updateJarakAdmin', $jarak->id_jarak) }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-9">
                                      <label>Tujuan</label>
                                      <input name="tujuan" type="text" class="form-control text-capitalize" value="{{ $jarak->tujuan }}" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>Harga</label>
                                      <input name="ongkos" type="number" class="form-control text-capitalize" value="{{ $jarak->ongkos }}" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexJarakAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('pages.admin.jarak.partial.controlpanel')
            </div>
        </div>
@endsection
