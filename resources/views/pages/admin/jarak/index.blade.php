@extends('layouts.admin')
@section('htmlheader_title','Data Jarak Ongkos Kirim')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Jarak</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-7 text-center">Tujuan</th>
                                    <th class="col-md-2 text-center">Ongkos</th>
                                    <th class="col-md-2 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($jaraks as $jarak)
                                <tr>
                                    <td class="text-center">{{ (($jaraks->currentPage() - 1 ) * $jaraks->perPage() ) + $loop->iteration }}</td>
                                    <td class="text-capitalize">{{ $jarak->tujuan }}</td>
                                    <td class="text-right">{{ number_format($jarak->ongkos,0,",",".") }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('editJarakAdmin',$jarak->id_jarak) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('deleteJarakAdmin',$jarak->id) }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $jaraks->links() }}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                @include('pages.admin.jarak.partial.controlpanel')
            </div>
        </div>
@endsection
