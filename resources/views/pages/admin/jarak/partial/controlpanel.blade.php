<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Control Panel</h3>
    </div>
    <div class="box-body">
        <div class="btn-group-vertical btn-block">
            <a href="{{ route('addJarakAdmin') }}" class="btn btn-default btn-flat btn-sm {{ Request::is('*jarak/add') ? 'disabled' : '' }}">Tambah Tujuan</a>
            {{-- <button type="button" class="btn btn-default btn-flat btn-sm">Ganti Password</button> --}}
        </div>
    </div>
</div>