@extends('layouts.admin')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Data Administrator</h3>
                    </div>
                    <div class="box-body">
                        <form role="form" method="POST" action="{{ route('postAddDataAdmin') }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-5">
                                      <label>Nama lengkap</label>
                                      <input name="name" type="text" class="form-control text-capitalize">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                      <label>Username</label>
                                      <input name="username" type="text" class="form-control text-lowercase">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                      <label>E-Mail</label>
                                      <input name="email" type="email" class="form-control text-lowercase">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                      <label>Password</label>
                                      <input name="password" type="password" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexDataAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <button type="submit" class="btn btn-default btn-flat btn-sm {{ Request::is('admin/data-admin/add') ? 'disabled' : '' }}">Tambah Administrator</button>
                            {{-- <button type="button" class="btn btn-default btn-flat btn-sm">Ganti Password</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
