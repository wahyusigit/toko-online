@extends('layouts.admin')
@section('htmlheader_title','Pesan')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Halaman Proses Pemesanan</h3>
                    </div>
                    <div class="box-body">
                        <div class="callout callout-info">
                            <h4>Pemesanan</h4>
                            <p>Menu Pemesanan digunakan untuk pengecekan Data Pemesanan Baru yang dilakukan oleh User juga proses input data bayar pemesanan secara manual oleh Admintrator.</p>
                        </div>
                        <div class="callout callout-info">
                            <h4>Pembayaran</h4>
                            <p>Menu Pembayaran digunakan untuk proses input data bayar pesanan user dan ubah status bayar user menjadi sudah bayar.</p>
                        </div>
                        <div class="callout callout-info">
                            <h4>Pengiriman</h4>
                            <p>Menu Pengiriman digunakan untuk proses input data pengiriman barang pesanan dan mengubah status kirim barang pesanan menjadi sudah kirim.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('indexPesananAdmin') }}" class="btn btn-default btn-flat btn-sm">Pemesanan</a>
                            <a href="{{ route('indexPembayaranAdmin') }}" class="btn btn-default btn-flat btn-sm">Pembayaran</a>
                            <a href="{{ route('indexPengirimanAdmin') }}" class="btn btn-default btn-flat btn-sm">Pengiriman</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
