@extends('layouts.admin')
@section('htmlheader_title','Pesanan Detail')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail Pemesanan - <b>{{ $pesanan->id_pesan }}</b> - <b class="text-capitalize">{{ $pesanan->customer->name }}</b></h3>
                    </div>
                    <div class="box-body">
                        @include('pages.partials.pesanan.table_detail')
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('indexPesananAdmin') }}" class="btn btn-default btn-flat btn-sm">Pemesanan</a>
                            <a href="{{ route('indexPembayaranAdmin') }}" class="btn btn-default btn-flat btn-sm">Pembayaran</a>
                            <a href="{{ route('indexPengirimanAdmin') }}" class="btn btn-default btn-flat btn-sm">Pengiriman</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
