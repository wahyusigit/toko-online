@extends('layouts.admin')
@section('htmlheader_title','Pengiriman')
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesanan</h3>
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Pesanan Yang Sudah Dikirim</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Pesanan Yang Belum Dikirim</a></li>
                                <li><a href="#tab_3" data-toggle="tab">Pesanan Yang Sudah Diterima</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <table class="table table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">Nama Pengirim</th>
                                                <th class="text-center">Tanggal<br>Kirim</th>
                                                <th class="text-center">No. Resi</th>
                                                <th class="text-center">Status<br>Diterima</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($sudah_dikirim as $no => $pesanan)
                                            <tr>
                                                <td class="text-center">{{ $no+1 }}</td>
                                                <td class="text-capitalize"> {{ $pesanan->nama_prod }} </td>
                                                <td class="text-center"> {{ $pesanan->jumlah_barang }} </td>
                                                <td class="text-capitalize"> {{ $pesanan->pengirim }} </td>
                                                <td class="text-center"> {{ $pesanan->tanggal_kirim }} </td>
                                                <td class="text-capitalize"> {{ $pesanan->no_resi }} </td>
                                                <td class="text-capitalize text-center"> 
                                                    @if($pesanan->status_diterima == 'sudah')
                                                    <span class="label label-success">Sudah</span> 
                                                    @else 
                                                    <span class="label label-danger">Belum</span> 
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{ route('konfirmasiPenerimaanCustomer', $pesanan->id_det) }}" class="btn btn-success btn-flat btn-sm">Konfirmasi Penerimaan</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab_2">
                                    <table class="table table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">Nama Toko</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($belum_dikirim as $no => $pesanan)
                                            <tr>
                                                <td class="text-center">{{ $no+1 }}</td>
                                                <td class="text-capitalize"> {{ $pesanan->nama_prod }} </td>
                                                <td class="text-center"> {{ $pesanan->jumlah_barang }} </td>
                                                <td class="text-capitalize"> {{ $pesanan->pengirim }} </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    <table class="table table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Nama Produk</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">Nama Pengirim</th>
                                                <th class="text-center">Tanggal<br>Kirim</th>
                                                <th class="text-center">No. Resi</th>
                                                <th class="text-center">Status<br>Diterima</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($sudah_diterima as $no => $pesanan)
                                            <tr>
                                                <td class="text-center">{{ $no+1 }}</td>
                                                <td class="text-capitalize"> {{ $pesanan->nama_prod }} </td>
                                                <td class="text-center"> {{ $pesanan->jumlah_barang }} </td>
                                                <td class="text-capitalize"> {{ $pesanan->pengirim }} </td>
                                                <td class="text-center"> {{ $pesanan->tanggal_kirim }} </td>
                                                <td class="text-capitalize"> {{ $pesanan->no_resi }} </td>
                                                <td class="text-capitalize text-center"> 
                                                    @if($pesanan->status_diterima == 'sudah')
                                                    <span class="label label-success">Sudah</span> 
                                                    @else 
                                                    <span class="label label-danger">Belum</span> 
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
        </div>
@endsection