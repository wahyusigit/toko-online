@extends('layouts.admin')
@section('htmlheader_title','Pembayaran')
@section('main-content')
<div class="row">
    <div class="col-md-9">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Pembayaran</h3>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Belum di Verifikasi</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Sudah di Verifikasi</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table class="table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th class="col-md-1 text-center">No</th>
                                        <th class="col-md-2 text-center">ID Pesan</th>
                                        <th class="col-md-2 text-center">Nama Customer</th>
                                        <th class="col-md-2 text-center">Tanggal<br>Konfirmasi</th>
                                        <th class="col-md-2 text-center">No. Bukti</th>
                                        <th class="col-md-2 text-center">Jumlah<br>Bayar</th>
                                        <th class="col-md-3 text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($belum_verifikasis as $pembayaran)
                                    <tr>
                                        <td class="text-center"> {{ (($belum_verifikasis->currentPage() - 1 ) * $belum_verifikasis->perPage() ) + $loop->iteration }} </td>
                                        <td class="text-left"> {{ $pembayaran->id_pesan }} </td>
                                        <td class="text-left text-capitalize"> {{ $pembayaran->pesanan->customer->name }} </td>
                                        <td class="text-center"> {{ date_format(date_create($pembayaran->tanggal_konfirmasi),"d-m-Y") }} </td>
                                        <td class="text-left"> {{ $pembayaran->no_bukti }} </td>
                                        <td class="text-right"> {{ number_format($pembayaran->jumlah_bayar,0,',','.') }} </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                @if(Auth::user()->hasRole('admin'))
                                                <a href="{{ route('verifikasiPembayaranAdmin', $pembayaran->id_pesan) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search">
                                                </i></a>
                                                @else
                                                <a href="" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search">
                                                </i></a>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $belum_verifikasis->links() }}
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <table class="table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th class="col-md-2 text-center">ID Bayar</th>
                                        <th class="col-md-2 text-center">ID Pesan</th>
                                        <th class="col-md-2 text-center">Nama Customer</th>
                                        <th class="col-md-2 text-center">No. Bukti</th>
                                        <th class="col-md-2 text-center">Jumlah<br>Bayar</th>
                                        <th class="col-md-2 text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sudah_verifikasis as $pembayaran)
                                    <tr>
                                        <td class="text-center"> {{ (($sudah_verifikasis->currentPage() - 1 ) * $sudah_verifikasis->perPage() ) + $loop->iteration }} </td>
                                        <td class="text-left"> {{ $pembayaran->id_bayar }} </td>
                                        <td class="text-left"> {{ $pembayaran->id_pesan }} </td>
                                        <td class="text-left text-capitalize"> {{ $pembayaran->pesanan->customer->name }} </td>
                                        <td class="text-left"> {{ $pembayaran->no_bukti }} </td>
                                        <td class="text-right"> {{ number_format($pembayaran->jumlah_bayar,0,',','.') }} </td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <a href="{{ route('showPembayaranAdmin', $pembayaran->id_bayar) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search">
                                                </i></a>
                                                <a href="{{ route('editPembayaranAdmin', $pembayaran->id_bayar) }}" class="btn btn-flat btn-primary btn-sm"><i class="fa fa-edit">
                                                </i></a>
                                                <a href="{{ route('deletePembayaranAdmin', $pembayaran->id_bayar) }}" class="btn btn-flat btn-danger btn-sm"><i class="fa fa-trash">
                                                </i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $sudah_verifikasis->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Control Panel</h3>
            </div>            
            <div class="box-body">
                <div class="btn-group-vertical btn-block">
                    <a href="{{ route('indexPesananAdmin') }}" class="btn btn-default btn-flat btn-sm">Pemesanan</a>
                    <a href="{{ route('indexPembayaranAdmin') }}" class="btn btn-default btn-flat btn-sm">Pembayaran</a>
                    <a href="{{ route('indexPengirimanAdmin') }}" class="btn btn-default btn-flat btn-sm">Pengiriman</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
