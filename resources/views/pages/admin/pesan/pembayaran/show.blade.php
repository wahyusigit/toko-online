@extends('layouts.admin')
@section('htmlheader_title','Detail Pembayaran')
@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pembayaran <b>{{ ucfirst($pembayaran->pesanan->customer->name) }}</b></h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ID Bayar</label>
                                    <input type="text" name="id_bayar" class="form-control" value="{{ $pembayaran->id_bayar }}" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ID Pesan</label>
                                    <input type="text" name="id_pesan" class="form-control" value="{{ $pembayaran->id_pesan }}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tanggal Transfer</label>
                                    <input type="date" name="tanggal_transfer" class="form-control" value="{{ $pembayaran->tanggal_transfer }}" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tanggal Verifikasi</label>
                                    <input type="date" name="tanggal_verifikasi" class="form-control" value="{{ $pembayaran->tanggal_verifikasi }}" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status Verifikasi</label>
                                    <input type="text" name="status_verifikasi" class="form-control" value="{{ $pembayaran->status_verifikasi }}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea name="keterangan" class="form-control" rows="4" readonly="readonly">{{ $pembayaran->keterangan }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. Bukti</label>
                                    <input type="text" name="no_bukti" class="form-control" value="{{ $pembayaran->no_bukti }}" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah Transfer</label>
                                    <input type="text" name="jumlah_bayar" class="form-control" value="{{ $pembayaran->jumlah_bayar }}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image Bukti Pembayaran</label>
                                    @if(is_null($pembayaran->image_no_bukti))
                                    <p class="text-red"><strong>Customer Tidak Mengunggah Bukti Pembayaran</strong></p>
                                    @else
                                    <img src="{{ asset($pembayaran->image_no_bukti) }}" class="img-thumbnail img-responsive col-md-12">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-left">
                    <a href="{{ route('indexPembayaranAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>   Kembali</a>
                </div>
                <div class="pull-right">
                    <a href="{{ route('editPembayaranAdmin', $pembayaran->id_bayar) }}" class="btn btn-success btn-flat"><i class="fa fa-edit"></i>  Edit</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pesanan</h3>
            </div>
            <div class="box-body">
                @include('pages.partials.pesanan.table_detail')
            </div>
        </div>
    </div>
</div>
@endsection
