@extends('layouts.admin')
@section('htmlheader_title','Ubah Pembayaran')
@section('main-content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form enctype="multipart/form-data" action="{{ route('updatePembayaranAdmin', $pembayaran->id_bayar) }}" method="POST">
                {{ csrf_field() }}
            <div class="box-header with-border">
                <h3 class="box-title">Ubah Pembayaran</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ID Bayar</label>
                                    <input type="text" name="id_bayar" class="form-control" value="{{ $pembayaran->id_bayar }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ID Pesan</label>
                                    <input type="text" name="id_pesan" class="form-control" value="{{ $pembayaran->id_pesan }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tanggal Transfer</label>
                                    <input type="date" name="tanggal_transfer" class="form-control" value="{{ $pembayaran->tanggal_transfer }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tanggal Verifikasi</label>
                                    <input type="date" name="tanggal_verifikasi" class="form-control" value="{{ $pembayaran->tanggal_verifikasi }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status Verifikasi</label>
                                    <input type="text" name="status_verifikasi" class="form-control text-capitalize" value="{{ $pembayaran->status_verifikasi }}" readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea name="keterangan" class="form-control" rows="4">{{ $pembayaran->keterangan }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. Bukti</label>
                                    <input type="text" name="no_bukti" class="form-control text-uppercase" value="{{ $pembayaran->no_bukti }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah Transfer</label>
                                    <input type="text" name="jumlah_bayar" class="form-control" value="{{ $pembayaran->jumlah_bayar }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Image Bukti Pembayaran</label>
                                    <input name="image_no_bukti" type="file">
                                    @if(is_null($pembayaran->image_no_bukti))
                                    <p class="text-red"><strong>Customer Tidak Mengunggah Bukti Pembayaran</strong></p>
                                    @else
                                    <img src="{{ asset($pembayaran->image_no_bukti) }}" class="img-thumbnail img-responsive col-md-12">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-left">
                    <a href="{{ route('indexPembayaranAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>   Kembali</a>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Detail Pemesanan</h3>
            </div>
            <div class="box-body">
                @include('pages.partials.pesanan.table_detail')
            </div>
        </div>
    </div>
</div>
@endsection
