@extends('layouts.admin')
@section('htmlheader_title','Verifikasi Pembayaran')
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                <div class="callout callout-info">
                    <h4>Informasi !</h4>

                    <p>Setelah Admin melakukan Verifikasi Pembayaran maka Data Pembayaran akan terisi secara otomatis dan status pembayaran menjadi Sudah Bayar.</p>
                    <p>Data Pembayaran yang sudah Anda Verifikasi bisa dilihat pada Halaman Pembayaran</p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail Pesanan</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID Pesan</th>
                                            <th>Tanggal Pesan</th>
                                            <th>Nama Customer</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $pesanan->id_pesan }}</td>
                                            <td>{{ $pesanan->tanggal_pesan }}</td>
                                            <td class="text-capitalize">{{ $pesanan->customer->name }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        @include('pages.partials.pesanan.table_detail')
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Verifikasi Pembayaran</h3>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('postVerifikasiPembayaranAdmin', $pesanan->id_pesan) }}" method="POST" role="form">
                            {{ csrf_field() }}
                            @if(is_null($pesanan->pembayaran))
                            <div class="form-group">
                                <label for="">Tanggal Verifikasi</label>
                                <input name="tanggal_verifikasi" type="date" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">No. Bukti</label>
                                <input name="no_bukti" type="text" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah Transfer</label>
                                <input name="jumlah_bayar" type="number" class="form-control" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" class="form-control"></textarea>
                            </div>
                            @else
                            <div class="form-group">
                                <label for="">Tanggal Verifikasi</label>
                                <input name="tanggal_verifikasi" type="date" class="form-control" value="{{ $pesanan->pembayaran->tanggal_verifikasi }}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">No. Bukti</label>
                                <input name="no_bukti" type="text" class="form-control" value="{{ $pesanan->pembayaran->no_bukti }}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">Gambar Bukti Pembayaran</label>
                                @if(is_null($pesanan->pembayaran->image_no_bukti))
                                <p class="text-red"><strong>Customer Tidak Mengunggah Bukti Pembayaran</strong></p>
                                @else
                                <img src="{{ asset($pesanan->pembayaran->image_no_bukti) }}" class="img-thumbnail img-responsive col-md-12">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah Transfer</label>
                                <input name="jumlah_bayar" type="number" class="form-control" value="{{ $pesanan->pembayaran->jumlah_bayar }}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" class="form-control">{{ $pesanan->pembayaran->keterangan }}</textarea>
                            </div>
                            @endif
                            <button type="submit" class="btn btn-success pull-right btn-flat">Verifikasi Pembayaran</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@endsection
