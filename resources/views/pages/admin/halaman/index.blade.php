@extends('layouts.admin')
@section('htmlheader_title','Data Halaman')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <form action="{{ route('updateHalamanAdmin', $halaman->id_halaman) }}" method="POST">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title text-capitalize">Ubah Halaman - <b>{{ $halaman->halaman }}</b></h3>
                    </div>
                    <div class="box-body">
                        <textarea name="konten" id="summernote" required="required">{{ $halaman->konten }}</textarea>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-flat btn-primary pull-right">Simpan</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            @foreach($halamans as $halaman)
                            <a href="{{ route('indexHalamanAdmin', $halaman->slug) }}" class="btn btn-default btn-flat text-capitalize btn-sm {{ Request::is('admin/asdasd') ? 'active' : '' }}">Halaman {{ $halaman->halaman }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/summernote/summernote.css') }}">
@endpush

@push('script')
<script type="text/javascript" src="{{ asset('plugins/summernote/summernote.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        placeholder: 'Edit Halaman',
        height: 400
      });
    });

</script>
@endpush