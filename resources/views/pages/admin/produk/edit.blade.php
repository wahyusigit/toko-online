@extends('layouts.admin')
@section('htmlheader_title','Ubah Produk')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Data Produk</h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" role="form" method="POST" action="{{ route('updateProdukAdmin', $produk->id_produk) }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">    
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>User Toko</label>
                                                <select name="id_toko" class="form-control text-capitalize" required="required">  
                                                    <option selected="selected" class="text-capitalize" value="{{ $produk->toko->id }}">{{ $produk->toko->name }}</option>
                                                    <optgroup label="Semua User Toko">
                                                    @foreach($tokos as $toko)
                                                    <option class="text-capitalize" value="{{ $toko->id }}">{{ $toko->name }}</option>
                                                    @endforeach
                                                    </optgroup>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pilih Jenis Produk</label>
                                                <select name="id_jenis" class="form-control text-capitalize">  
                                                    <option selected="selected" value="{{ $produk->jenis->id_jenis }}">{{ $produk->jenis->nama_jenis }}</option>
                                                    <optgroup label="Semua Jenis Produk">
                                                    @foreach($produk_jenis as $jenis)
                                                    <option class="text-capitalize" value="{{ $produk->jenis->id_jenis }}">{{ $jenis->nama_jenis }}</option>
                                                    @endforeach
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                              <label>Nama Produk</label>
                                              <input name="nama_prod" type="text" class="form-control text-capitalize" required="required" value="{{ $produk->nama_prod }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                              <label>Harga</label>
                                              <input name="harga_prod" type="number" class="form-control text-lowercase" required="required" value="{{ $produk->harga_prod }}">
                                            </div>
                                            <div class="form-group col-md-6">
                                              <label>Berat (Gram)</label>
                                              <input name="berat_prod" type="number" class="form-control" required="required" value="{{ $produk->berat_prod }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                              <label>Keterangan / Deskripsi</label>
                                              <textarea class="form-control" name="keterangan_prod" rows="3" required="required">{{ $produk->keterangan_prod }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Status</label>
                                                <select name="status_prod" class="form-control" required="required">  
                                                    <option value="ada">Ada</option>
                                                    <option value="tidak">Tidak Ada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input id="image_prod" name="image_prod" type="file" class="form-control" disabled="disabled">
                                                    <span class="input-group-btn">
                                                      <button id="enable_new_image_prod" type="button" class="btn btn-info btn-flat">Ubah Gambar</button>
                                                    </span>
                                            </div>
                                            <img class="img img-responsive img-thumbnail" src="{{ asset($produk->image_prod) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexProdukAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addProdukAdmin') }}" class="btn btn-default btn-flat btn-sm {{ Request::is('*produk/add') ? 'disabled' : '' }}">Tambah Data Produk</a>
                            <a href="{{ route('indexProdukJenisAdmin') }}" class="btn btn-default btn-flat btn-sm">Olah Data Jenis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script')
<script type="text/javascript">
    $('body').on('click','#enable_new_image_prod', function(){
        if ($("#image_prod").prop('disabled')) {
            $("#image_prod").prop('disabled', false);    
        } else {
            $("#image_prod").prop('disabled', true);
        }
        
    });
</script>
@endpush