@extends('layouts.admin')
@section('htmlheader_title','Tambah Produk')
@section('main-content')    
        <div class="row">
            @if(is_null($jenis->first()))
            <div class="col-md-12">
                <div class="callout callout-info">
                    <h4>Informasi</h4>
                    <p>Jika Data Produk Jenis tidak ada silahkan buka Halaman <b>Olah Data Jenis</b> untuk menabahkan Jenis Produk Baru</p>
                </div>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Data Produk</h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" role="form" method="POST" action="{{ route('postAddProdukAdmin') }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    @if(Auth::user()->hasRole('admin'))
                                    <div class="form-group col-md-3">
                                        <label>Pilih User Toko</label>
                                        <select name="id_toko" class="form-control text-capitalize" required="required">  
                                            <option value="">Pilih Toko</option>
                                            @foreach($tokos as $toko)
                                                <option class="text-capitalize" value="{{ $toko->id }}">{{ $toko->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    <div class="form-group col-md-3">
                                        <label>Pilih Jenis Produk</label>
                                        <select name="id_jenis" class="form-control text-capitalize" required="required">  
                                            <option value="">Pilih</option>
                                            @foreach($jenis as $jns)
                                                <option class="text-capitalize"  value="{{ $jns->id_jenis }}">{{ $jns->nama_jenis }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group col-md-6">
                                      <label>Nama Produk</label>
                                      <input name="nama_prod" type="text" class="form-control text-capitalize" required="required">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                      <label>Harga</label>
                                      <input name="harga_prod" type="number" class="form-control text-lowercase" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>Berat (Gram)</label>
                                      <input name="berat_prod" type="number" class="form-control" required="required">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                      <label>Keterangan / Deskripsi</label>
                                      <textarea class="form-control text-capitalize" name="keterangan_prod" rows="3" required="required"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <label>Gambar Produk</label>
                                        <input name="image_prod" type="file" class="form-control" required="required">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Status</label>
                                        <select name="status_prod" class="form-control" required="required">  
                                            <option value="ada">Ada</option>
                                            <option value="tidak">Tidak Ada</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexProdukAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addProdukAdmin') }}" class="btn btn-default btn-flat btn-sm {{ Request::is('*produk/add') ? 'disabled' : '' }}">Tambah Data Produk</a>
                            <a href="{{ route('indexProdukJenisAdmin') }}" class="btn btn-default btn-flat btn-sm">Olah Data Jenis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
