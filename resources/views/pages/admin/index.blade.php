@extends('layouts.admin')
@section('htmlheader_title','Home')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="callout callout-info">
                    <h4>Informasi!</h4>
                    <p><b>Selamat Datang</b> di halaman pengolahan data khusus Administrator.</p>
                </div>
                <div class="callout callout-info">
                    <h4>Informasi!</h4>
                    <p>Gunakan menu pada Navigasi sebelah kiri untuk memilih halaman pengelolaan data.</p>
                </div>
                <div class="callout callout-info">
                    <h4>Informasi!</h4>
                    <p>Gunakan menu pada Control Panel sebelah kanan untuk melakukan pengolahan data.</p>
                </div>
                <div class="callout callout-info">
                    <h4>Informasi!</h4>
                    <p>Selalu gunakan menu Logout untuk keluar dari sistem.</p>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
        </div>
@endsection
