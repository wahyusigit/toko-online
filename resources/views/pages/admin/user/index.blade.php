@extends('layouts.admin')
@section('htmlheader_title', 'Data ' . ucfirst($slug) )
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
                {{-- <script>
                    $('#flash-overlay-modal').modal();
                </script> --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data {{ ucfirst($slug) }}</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-3 text-center">Nama Admin</th>
                                    <th class="col-md-3 text-center">Username</th>
                                    <th class="col-md-3 text-center">E-Mail</th>
                                    <th class="col-md-2 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                <tr>
                                    <td class="text-center">{{ (($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration }}</td>
                                    <td class="text-capitalize">{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('editUserAdmin',['slug'=>$slug,'id'=>$user->id]) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('deleteUserAdmin',['slug'=>$slug,'id'=>$user->id]) }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addUserAdmin', $slug) }}" class="btn btn-default btn-flat btn-sm">Tambah {{ ucfirst($slug) }}</a>
                            {{-- <button type="button" class="btn btn-default btn-flat btn-sm">Ganti Password</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
