@extends('layouts.admin')
@section('htmlheader_title','Testimoni')
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Testimoni</h3>
                        <div class="box-tools pull-right">
                            <span class="label label-info">Testimoni Terbaru ({{ $belum_dibaca }})</span>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th class="col-md-2">Nama Customer</th>
                                    <th class="col-md-2">Nama Toko</th>
                                    <th class="col-md-3">Nama Produk</th>
                                    <th class="col-md-1 text-center">Tanggal</th>
                                    <th class="">Tingkat Kepuasan</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($testimonis as $no => $testimoni)
                                @if($testimoni->status_baca == 'belum')
                                <tr class="bg-info">
                                @else
                                <tr>
                                @endif
                                    <td class="text-center">{{ $no + 1 }}</td>
                                    <td class="text-capitalize"> {{ $testimoni->customer->name }} </td>
                                    <td class="text-capitalize"> {{ $testimoni->produk->toko->name }} </td>
                                    <td class="text-capitalize"> {{ $testimoni->produk->nama_prod }} </td>
                                    <td class="text-center"> {{ date_format(date_create($testimoni->tanggal),"d-m-Y") }} </td>
                                    <td>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-green" style="width: {{ $testimoni->tingkat_kepuasan * 2 }}0%;"></div>
                                        </div>
                  </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('showTestimoniAdmin', $testimoni->id_testimoni) }}" class="btn btn-default btn-sm btn-flat"><i class="fa fa-search"></i></a>
                                            <a href="{{ route('editTestimoniAdmin', $testimoni->id_testimoni) }}" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('deleteTestimoniAdmin', $testimoni->id_testimoni) }}" class="btn btn-danger btn-sm btn-flat"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $testimonis->links() }}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
        </div>
@endsection
