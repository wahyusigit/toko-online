@extends('layouts.admin')
@section('htmlheader_title','Ubah Testimoni')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <form action="{{ route('updateTestimoniAdmin',$testimoni->id_testimoni) }}" method="POST">
                        {{ csrf_field() }}
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Testimoni</h3>
                    </div>
                    <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Name Customer</label>
                                                <input readonly="readonly" class="form-control" type="text" value="{{ $testimoni->customer->name }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label>Nama Toko</label>
                                                <input readonly="readonly" class="form-control" type="text" value="{{ $testimoni->produk->toko->name }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Produk</label>
                                                <input readonly="readonly" class="form-control" type="text" value="{{ $testimoni->produk->nama_prod }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Gambar Produk</label>
                                    <br>
                                    <img src="{{ asset($testimoni->produk->image_prod) }}" class="img img-responsive img-thumbnail">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Ulasan Produk</label>
                                        <textarea class="form-control" name="ulasan" rows="5">{{ $testimoni->ulasan }}</textarea>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="{{ route('indexTestimoniAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
            <div class="col-md-3">
                
            </div>
        </div>
@endsection
