@extends('layouts.admin')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Data Toko</h3>
                    </div>
                    <div class="box-body">
                        <form role="form" method="POST" action="{{ route('updateDataTokoAdmin', $toko->id) }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="form-group col-md-5">
                                      <label>Nama lengkap</label>
                                      <input name="name" type="text" class="form-control text-capitalize" value="{{ $toko->name }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3">
                                      <label>Username</label>
                                      <input name="username" type="text" class="form-control text-lowercase" value="{{ $toko->username }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                      <label>E-Mail</label>
                                      <input name="email" type="email" class="form-control text-lowercase" value="{{ $toko->email }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-5">
                                        <div class="input-group">
                                            <input id="password" type="password" class="form-control" disabled="disabled">
                                                <span class="input-group-btn">
                                                  <button id="enable_new_password" type="button" class="btn btn-info btn-flat">Password Baru</button>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexDataTokoAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addDataTokoAdmin') }}" class="btn btn-default btn-flat btn-sm">Tambah Toko</a>
                            {{-- <button type="button" class="btn btn-default btn-flat btn-sm">Ganti Password</button> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script')
<script type="text/javascript">
    $('body').on('click','#enable_new_password', function(){
        $("#password").prop('disabled', false);
    });
</script>
@endpush