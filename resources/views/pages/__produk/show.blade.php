@extends('layouts.admin')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lihat Data Produk</h3>
                    </div>
                    <div class="box-body">
                        <form enctype="multipart/form-data" role="form" method="POST" action="{{ route('postAddProdukAdmin') }}">
                        {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">    
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>User Toko</label>
                                                <select name="id_user" class="form-control" disabled="disabled">  
                                                    <option class="text-capitalize" value="{{ $produk->user->id }}">{{ $produk->user->nama }}</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pilih Jenis Produk</label>
                                                <select name="id_jenis" class="form-control"  disabled="disabled">  
                                                    <option value="{{ $produk->jenis->id_jenis }}">{{ $produk->jenis->nama_jenis }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                              <label>Nama Produk</label>
                                              <input name="nama_prod" type="text" class="form-control text-lowercase" disabled="disabled" value="{{ $produk->nama_prod }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                              <label>Harga</label>
                                              <input name="harga_prod" type="number" class="form-control text-lowercase" disabled="disabled" value="{{ $produk->harga_prod }}">
                                            </div>
                                            <div class="form-group col-md-6">
                                              <label>Berat (Gram)</label>
                                              <input name="berat_prod" type="number" class="form-control" disabled="disabled" value="{{ $produk->berat_prod }}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                              <label>Keterangan / Deskripsi</label>
                                              <textarea class="form-control" name="keterangan_prod" rows="3" disabled="disabled">{{ $produk->keterangan_prod }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Status</label>
                                                <select name="status_prod" class="form-control" disabled="disabled">  
                                                    <option value="ada">Ada</option>
                                                    <option value="tidak">Tidak Ada</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gambar Produk</label>    
                                            <img class="img img-responsive img-thumbnail" src="{{ asset($produk->image_prod) }}">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="{{ route('indexProdukAdmin') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('editProdukAdmin', $produk->id_produk) }}" class="btn btn-success btn-flat"><i class="fa fa-edit"></i>  Edit</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addProdukAdmin') }}" class="btn btn-default btn-flat btn-sm {{ Request::is('*produk/add') ? 'disabled' : '' }}">Tambah Data Produk</a>
                            <a href="{{ route('indexProdukJenisAdmin') }}" class="btn btn-default btn-flat btn-sm">Olah Data Jenis</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
