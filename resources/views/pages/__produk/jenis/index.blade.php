@extends('layouts.admin')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Jenis Produk</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-9 text-center">Nama Jenis</th>
                                    <th class="col-md-2 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($jenis as $no => $jns)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-capitalize">{{ $jns->nama_jenis }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('editProdukJenisAdmin',$jns->id_jenis) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('deleteProdukJenisAdmin',$jns->id_jenis) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <a href="{{ route('addProdukJenisAdmin') }}" class="btn btn-default btn-flat btn-sm">Tambah Jenis Produk</a>
                            <a href="{{ route('indexProdukAdmin') }}" class="btn btn-primary btn-flat btn-sm">Kembali ke Produk</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
