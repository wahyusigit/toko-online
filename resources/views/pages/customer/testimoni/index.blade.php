@extends('layouts.customer')
@section('htmlheader_title','Testimoni')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Testimoni</h3>
                    </div>
                    <div class="box-body">
                        @foreach($testimonis as $testimoni)
                        <div class="media">
                            <div class="media-left">
                                <a href="">
                                    <img src="{{ asset($testimoni->produk->image_prod) }}" class="media-object" style="width: 150px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="clearfix">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h4 style="margin-top: 0">{{ $testimoni->produk->nama_prod }}</h4>
                                            <p>{{ $testimoni->ulasan }}</p>
                                            <p style="margin-bottom: 0">
                                                <!-- Progress bars -->
                                                <div class="clearfix">
                                                    <span class="pull-left">Tingkat Kepuasan</span>
                                                    <small class=""> - {{ $testimoni->tingkat_kepuasan * 2 }}0%</small>
                                                </div>
                                                <div class="progress xs">
                                                    <div class="progress-bar progress-bar-green" style="width: {{ $testimoni->tingkat_kepuasan * 2 }}0%;"></div>
                                                </div>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                                <a href="{{ route('editTestimoniCustomer', $testimoni->id_testimoni) }}" class="btn btn-default btn-flat btn-sm pull-right"><i class="fa fa-edit"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                        {{ $testimonis->links() }}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Testimoni</h3>
                    </div>
                    <div class="box-body">
                        <a href="{{ route('addTestimoniCustomer') }}" class="btn btn-block btn-success btn-flat btn-sm">Tambah Testimoni</a>
                    </div>
                </div>
            </div>
        </div>
@endsection
