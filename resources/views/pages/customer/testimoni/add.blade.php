@extends('layouts.customer')
@section('htmlheader_title','Tambah Testimoni')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Testimoni</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-2 text-center">Nama Produk</th>
                                    <th class="col-md-3 text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pesanans as $no => $pesanan)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-left col-md-10"> {{ $pesanan->nama_prod }} </td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a href="{{ route('add2TestimoniCustomer', $pesanan->id_det) }}" class="btn btn-flat btn-success btn-sm"><i class="fa fa-plus">  Tambah Testimoni</i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- {{ $pesanans->links() }} --}}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Testimoni</h3>
                    </div>
                    <div class="box-body">
                        <a href="" class="btn btn-block btn-success btn-flat btn-sm">Tambah Testimoni</a>
                    </div>
                </div>
            </div>
        </div>
@endsection
