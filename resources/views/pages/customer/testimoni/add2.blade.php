@extends('layouts.customer')
@section('htmlheader_title','Add Testimoni')
@section('main-content')
        <div class="row">
            <div class="col-md-12">
                @include('flash::message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <form action="{{ route('postAdd2TestimoniCustomer',$testimoni->id_det) }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_produk" value="{{ $testimoni->id_produk }}">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Testimoni</h3>
                    </div>
                    <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Nama Produk</label>
                                                <input readonly="readonly" class="form-control" type="text" value="{{ $testimoni->produk->nama_prod }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label>Nama Toko</label>
                                                <input readonly="readonly" class="form-control" type="text" value="{{ $testimoni->produk->toko->name }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Tingkat Kepuasan</label>
                                                <select class="form-control" name="tingkat_kepuasan">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option selected="selected" value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Gambar Produk</label>
                                    <br>
                                    <img src="{{ asset($testimoni->produk->image_prod) }}" class="img img-responsive img-thumbnail">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Ulasan Produk</label>
                                        <textarea required="required" class="form-control" name="ulasan" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-left">
                            <a href="{{ route('indexTestimoniCustomer') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                        </div>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
            <div class="col-md-3">
                
            </div>
        </div>
@endsection
