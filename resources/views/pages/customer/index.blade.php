@extends('layouts.customer')
@section('htmlheader_title','Customer')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="callout callout-info">
                    <h4>Informasi!</h4>
                    <p>Selamat Datang <b class="text-capitalize">{{ Auth::user()->name }}</b></p>
                    <p>Untuk mengetahui Prosedur Pemesanan dan Pembayaran Silahkan menuju Halaman <a href="{{ route('caraPemesanan') }}">Cara Pemasanan</a> dan <a href="{{ route('caraPembayaran') }}">Cara Pembayaran</a></p>
                    <p>Jika masih belum jelas Anda bisa menghubungi kami. Silahkah menuju Halaman <a href="{{ route('hubungiKami') }}">Hubungi Kami</a> </p>
                </div>
            </div>
        </div>
@endsection
