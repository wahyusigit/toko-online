@extends('layouts.customer')
@section('htmlheader_title','Pesanan')
@section('main-content')
<div class="row">
    <div class="col-md-12">
        @include('flash::message')
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Halaman Pemesanan</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="col-md-2 text-center">ID Pesan</th>
                            <th class="col-md-2 text-center">Tanggal Pesan</th>
                            <th class="col-md-2 text-center">Status<br>Pembayaran</th>
                            <th class="col-md-1 text-center">Ongkos<br>Kirim</th>
                            <th class="col-md-1 text-center">Total<br>Pembelian</th>
                            <th class="col-md-1 text-center">Total<br>Akhir</th>
                            <th class="col-md-3 text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pesanans as $no => $pesanan)
                        <tr>
                            <td class="text-center"> {{ $no+1 }} </td>
                            <td class="text-left"> {{ $pesanan->id_pesan }} </td>
                            <td class="text-center"> {{ $pesanan->tanggal_pesan }} </td>
                            <td class="text-center text-capitalize"> 
                                @if(is_null($pesanan->pembayaran) == true)
                                <span class="label label-danger">Belum</span>
                                @elseif($pesanan->pembayaran->status_verifikasi == 'sudah')
                                <span class="label label-success">Sudah</span>
                                @elseif($pesanan->pembayaran->status_verifikasi == 'belum')
                                <span class="label label-warning">Dalam Proses Verifikasi</span>
                                @endif
                            </td>
                            <td class="text-center"> {{ number_format($pesanan->ongkos_kirim,0,',','.') }} </td>
                            <td class="text-center"> {{ number_format($pesanan->detail->sum('subtotal'),0,',','.') }} </td>
                            <td class="text-center"> {{ number_format($pesanan->total_bayar,0,',','.') }} </td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('konfirmasiCustomer', $pesanan->id_pesan) }}" class="btn btn-flat btn-success btn-sm {{ $pesanan->status_bayar == 'sudah' ? 'disabled' : '' }}"><i class="fa fa-check"></i>  Konfirmasi Pembayaran</a>
                                </div>

                                <div class="btn-group">
                                    <a href="{{ route('pesananDetailCustomer', $pesanan->id_pesan) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search"></i></a>
                                    <a href="{{ route('deletePesananCustomer', $pesanan->id_pesan) }}" class="btn btn-flat btn-danger btn-sm {{ $pesanan->status_bayar == 'sudah' ? 'disabled' : '' }}"><i class="fa fa-trash"></i></a>
                                    
                                    {{-- <button class="btn btn-flat btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                                    <button class="btn btn-flat btn-danger btn-sm"><i class="fa fa-trash"></i></button> --}}
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
