@extends('layouts.customer')
@section('htmlheader_title','Konfirmasi Pembayaran')
@section('main-content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="callout callout-info">
            <h4>Informasi !</h4>
            <p>Jika Anda ingin membatalkan salah satu produk yang anda pesan silahkan menuju halaman <a href="{{ route('pesananDetailCustomer', $pesanan->id_pesan) }}">Pemesanan</a>
            </p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('flash::message')
        <script>
            $('#flash-overlay-modal').modal();
        </script>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Halaman Pemesanan</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Kode Pesan</th>
                                    <th>Tanggal Pesan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $pesanan->id_pesan }}</td>
                                    <td>{{ $pesanan->tanggal_pesan }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                @include('pages.partials.pesanan.table_detail_show')
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Konfirmasi Pembayaran</h3>
            </div>
            <div class="box-body">
                <form enctype="multipart/form-data" action="{{ route('postKonfirmasiCustomer', $pesanan->id_pesan) }}" method="POST" role="form">
                    {{ csrf_field() }}
                    @if(is_null($pesanan->pembayaran))
                    <div class="form-group">
                        <label for="">Tanggal Transfer</label>
                        <input name="tanggal_transfer" type="date" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">No. Bukti</label>
                        <input name="no_bukti" type="text" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Upload Bukti Pembayaran</label>
                        <input name="image_no_bukti" type="file" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Jumlah Transfer</label>
                        <input name="jumlah_bayar" type="number" class="form-control" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" class="form-control" placeholder="Contoh: Transfer dari Rekening BCA atas nama Rudi Prasetyo"></textarea>
                    </div>
                    @else
                    <div class="form-group">
                        <label for="">Tanggal Transfer</label>
                        <input name="tanggal_transfer" type="date" class="form-control" value="{{ $pesanan->pembayaran->tanggal_transfer }}" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">No. Bukti</label>
                        <input name="no_bukti" type="text" class="form-control" value="{{ $pesanan->pembayaran->no_bukti }}" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Upload Bukti Pembayaran</label>
                        <input name="image_no_bukti" type="file" class="form-control">
                        <hr>
                        <img src="{{ asset($pesanan->pembayaran->image_no_bukti) }}" class="img-thumbnail img-responsive col-md-12">
                    </div>
                    <div class="form-group">
                        <label for="">Jumlah Transfer</label>
                        <input name="jumlah_bayar" type="number" class="form-control" value="{{ $pesanan->pembayaran->jumlah_bayar }}" required="required">
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" class="form-control">{{ $pesanan->pembayaran->keterangan }}</textarea>
                    </div>
                    @endif
                    <button type="submit" class="btn btn-primary pull-right btn-flat">Konfirmasi Pembayaran</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
