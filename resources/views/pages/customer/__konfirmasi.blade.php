@extends('layouts.customer')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                
            </div>
            <div class="col-md-3">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Control Panel</h3>
                    </div>
                    <div class="box-body">
                        <div class="btn-group-vertical btn-block">
                            <button type="button" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-sign-out"></i> Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
