@extends('layouts.customer')
@section('htmlheader_title','Profile Customer')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Profile <b class="text-capitalize">{{ Auth::user()->name }}</b></h3>
                    </div>
                    <form role="form" method="POST" action="{{ route('updateProfileCustomer') }}">
                    {{ csrf_field() }}
                        <div class="box-body">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <label>Nama lengkap</label>
                                  <input name="name" type="text" class="form-control text-capitalize" value="{{ $customer->name }}">
                                </div>
                                <div class="form-group">
                                  <label>Username</label>
                                  <input name="username" type="text" class="form-control text-lowercase" value="{{ $customer->username }}">
                                </div>
                                <div class="form-group">
                                  <label>E-Mail</label>
                                  <input name="email" type="email" class="form-control text-lowercase" value="{{ $customer->email }}">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input id="password" type="password" class="form-control" disabled="disabled">
                                            <span class="input-group-btn">
                                              <button id="enable_new_password" type="button" class="btn btn-info btn-flat">Password Baru</button>
                                            </span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Alamat</label>
                                    <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat">{{ $customer->alamat }}</textarea>
                                </div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Kota</label>
                                    <input id="kota" name="kota" type="text" class="form-control" value="{{ $customer->kota }}" placeholder="Kota">
                                </div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Provinsi</label>
                                    <input id="provinsi" name="provinsi" type="text" class="form-control" value="{{ $customer->provinsi }}" placeholder="Provinsi">
                                </div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Kode POS</label>
                                    <input id="kode_pos" name="kode_pos" type="text" class="form-control" value="{{ $customer->kode_pos }}" placeholder="Kode POS">
                                </div>
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label>Telepon</label>
                                    <input id="telepon" name="telepon" type="text" class="form-control" value="{{ $customer->telepon }}" placeholder="Telepon">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="pull-left">
                                <a href="{{ route('indexCustomer') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
                            </div>
                            <div class="pull-right">
                                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i>  Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
@push('script')
<script type="text/javascript">
    $('body').on('click','#enable_new_password', function(){
        console.log($("#password").prop('disabled'));
        if ($("#password").prop('disabled') === true) {
            $("#password").prop('disabled', false);
        } else if($("#password").prop('disabled') === false) {
            $("#password").prop('disabled', true);
        }
    });
</script>
@endpush