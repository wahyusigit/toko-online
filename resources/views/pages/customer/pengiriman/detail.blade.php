@extends('layouts.customer')
@section('htmlheader_title','Detail Pengiriman')
@section('main-content')
        <div class="row">
            <div class="col-md-9">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Halaman Pemesanan</h3>
                    </div>
                    <div class="box-body">
                        
                        <table class="table table-bordered table-condensed table-hover">
                            <thead>
                                <tr>
                                    <th class="col-md-1 text-center">No</th>
                                    <th class="col-md-5 text-center">Nama Produk</th>
                                    <th class="col-md-2 text-center">Jumlah<br>Beli</th>
                                    <th class="col-md-1 text-center">Harga<br>Satuan</th>
                                    <th class="col-md-2 text-center">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pengiriman->pesanantoko->detail as $no => $detail)
                                <tr>
                                    <td class="text-center"> {{ $no+1 }} </td>
                                    <td class="text-left"> {{ $detail->detail->produk->nama_prod }} </td>
                                    <td class="text-center text-capitalize"> {{ $detail->detail->jumlah_barang }} </td>
                                    <td class="text-right"> {{ number_format($detail->detail->harga_satuan,0,',','.') }} </td>
                                    <td class="text-right"> {{ number_format($detail->detail->subtotal,0,',','.') }} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <form action="{{ route('konfirmasiPengirimanCustomer', $pesanan_toko->id_pesanan_toko) }}" method="POST">
                        {{ csrf_field() }}
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Penerimaan</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Tanggal Diterima</label>
                            <input type="date" name="tanggal_diterima" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-flat">Konfirmasi Penerimaan </button>
                        </div>
                        
                    </div>
                </div>
                {{-- </form> --}}
            </div>
        </div>
@endsection
