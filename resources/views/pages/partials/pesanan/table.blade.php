<table class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th class="col-md-1 text-center">No</th>
            <th class="col-md-2 text-center">ID Pesan</th>
            <th class="col-md-2 text-center">Nama Customer</th>
            <th class="col-md-2 text-center">Tanggal Pesan</th>
            <th class="col-md-1 text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pesanans as $no => $pesanan)
        <tr>
            <td class="text-center"> {{ $no+1 }} </td>
            <td class="text-left"> {{ $pesanan->id_pesan }} </td>
            <td class="text-left text-capitalize"> {{ $pesanan->customer->name }} </td>
            <td class="text-center"> {{ date_format(date_create($pesanan->tanggal_pesan),"d-m-Y") }} </td>
            {{-- <td> {{ $pesanan->keterangan_pesan }} </td> --}}
            <td class="text-center">
                <div class="btn-group">
                    <a href="{{ route('pesananDetailAdmin', $pesanan->id_pesan) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search"></i></a>
                    <a href="{{ route('deletePesananAdmin', $pesanan->id_pesan) }}" class="btn btn-flat btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    {{-- <button class="btn btn-flat btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-flat btn-danger btn-sm"><i class="fa fa-trash"></i></button> --}}
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $pesanans->links() }}