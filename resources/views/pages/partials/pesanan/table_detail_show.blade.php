<table class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th class="col-md-1 text-center">No</th>
            <th class="col-md-6 text-center">Nama Produk</th>
            <th class="col-md-2 text-center">Jumlah<br>Beli</th>
            <th class="col-md-1 text-center">Harga<br>Satuan</th>
            <th class="col-md-2 text-center">Subtotal</th>
        </tr>
    </thead>
    <tbody>
        @foreach($pesanan->detail as $no => $detail)
        <tr>
            <td class="text-center"> {{ $no+1 }} </td>
            <td class="text-left"> {{ $detail->produk->nama_prod }} </td>
            <td class="text-center text-capitalize"> {{ $detail->jumlah_barang }} </td>
            <td class="text-right"> {{ number_format($detail->harga_satuan,0,',','.') }} </td>
            <td class="text-right"> {{ number_format($detail->subtotal,0,',','.') }} </td>
        </tr>
        @endforeach
        <tr>
            <td colspan="4" class="text-right"><b>Total Beli</b></td>
            <td class="text-right"><b>{{ number_format($pesanan->detail->sum('subtotal'),0,',','.') }}</b></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4" class="text-right"><b>Ongkos Kirim</b></td>
            <td class="text-right"><b>{{ number_format($pesanan->ongkos_kirim,0,',','.') }}</b></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4" class="text-right"><b>Total Akhir</b></td>
            <td class="text-right"><b>{{ number_format($pesanan->total_bayar,0,',','.') }}</b></td>
            <td></td>
        </tr>
    </tbody>
</table>