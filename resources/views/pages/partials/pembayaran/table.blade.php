<table class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th class="col-md-1 text-center">No</th>
            <th class="col-md-1 text-center">ID Bayar</th>
            <th class="col-md-1 text-center">ID Pesan</th>
            <th class="col-md-1 text-center">Tanggal Bayar</th>
            <th class="col-md-1 text-center">No Bukti</th>
            <th class="col-md-1 text-center">Jumlah Bayar</th>
            {{-- <th class="col-md-1 text-center">Action</th> --}}
        </tr>
    </thead>
    <tbody>
        @foreach($pembayarans as $no => $pembayaran)
        <tr>
            <td class="text-center">{{ $no+1 }}</td>
            <td class="text-uppercase text-left">{{ $pembayaran->id_bayar }}</td>
            <td class="text-uppercase text-left">{{ $pembayaran->id_pesan }}</td>
            <td class="text-center">{{ date_format(date_create($pembayaran->tanggal_bayar),"d-m-Y") }}</td>
            <td class="text-uppercase text-left">{{ $pembayaran->no_bukti }}</td>
            <td class="text-right">{{ number_format($pembayaran->jumlah_bayar,0,',','.') }}</td>
            {{-- <td class="text-center">
                <div class="btn-group">
                    <button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                </div>
            </td> --}}
        </tr>
        @endforeach
    </tbody>
</table>
{{ $pembayarans->links() }}