<table class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th class="col-md-1 text-center">No</th>
            <th class="col-md-2 text-center">ID Pesan</th>
            <th class="col-md-2 text-center">Nama User</th>
            <th class="col-md-2 text-center">Tanggal<br>Konfirmasi</th>
            <th class="col-md-2 text-center">No. Bukti</th>
            <th class="col-md-2 text-center">Jumlah<br>Bayar</th>
            <th class="col-md-3 text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($konfirmasis as $no => $konfirmasi)
        <tr>
            <td class="text-center"> {{ $no+1 }} </td>
            <td class="text-left"> {{ $konfirmasi->id_pesan }} </td>
            <td class="text-left text-capitalize"> {{ $konfirmasi->pesanan->customer->name }} </td>
            <td class="text-center"> {{ date_format(date_create($konfirmasi->tanggal_konfirmasi),"d-m-Y") }} </td>
            <td class="text-left"> {{ $konfirmasi->no_bukti }} </td>
            <td class="text-right"> {{ number_format($konfirmasi->jumlah_bayar,0,',','.') }} </td>
            <td class="text-center">
                <div class="btn-group">
                    @if(Auth::user()->hasRole('admin'))
                    <a href="{{ route('verifikasiPembayaranAdmin', $konfirmasi->id_pesan) }}" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search">
                    </i></a>
                    @else
                    <a href="" class="btn btn-flat btn-default btn-sm"><i class="fa fa-search">
                    </i></a>
                    @endif
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $konfirmasis->links() }}