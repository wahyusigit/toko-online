@extends('layouts.landing')
@section('htmlheader_title', 'Checkout')
@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-12">
                @include('flash::message')
            </div>
		</div>
		<section class="invoice">
			<div class="row">
		        <div class="col-xs-12 table-responsive">
					@include('partials.table_cart')
				</div>
			<!-- /.col -->
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<legend>Sudah Punya Akun ?</legend>
					<form class="form-horizontal" method="POST" action="{{ route('login') }}">
                	   {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-flat">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
				</div>
				<div class="col-md-6">
					<legend>Belum Punya Akun ?</legend>
					<button class="btn btn-flat btn-primary" data-toggle="modal" data-target="#daftar">Buat Akun</button>
				</div>
			</div>
		</section>
		<div class="row">
			
		</div>
	</div>



    <div id="daftar" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="{{ route('register') }}" method="POST">
                        {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Default Modal</h4>
                    </div>
                <div class="modal-body">
                    <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}" placeholder="Nama lengkap">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="E-Mail">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Ulangi password" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat">{{ old('alamat') }}</textarea>
                    @if ($errors->has('alamat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="kota" name="kota" type="text" class="form-control" value="{{ old('kota') }}" placeholder="Kota">
                    @if ($errors->has('kota'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kota') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="provinsi" name="provinsi" type="text" class="form-control" value="{{ old('provinsi') }}" placeholder="Provinsi">
                    @if ($errors->has('provinsi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('provinsi') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="kode_pos" name="kode_pos" type="text" class="form-control" value="{{ old('kode_pos') }}" placeholder="Kode POS">
                    @if ($errors->has('kode_pos'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kode_pos') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="telepon" name="telepon" type="text" class="form-control" value="{{ old('telepon') }}" placeholder="Telepon">
                    @if ($errors->has('telepon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telepon') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-flat pull-right">Daftar</button>
                </div>

                    </form>
            </div>
        <!-- /.modal-content -->
        </div>
      <!-- /.modal-dialog -->
    </div>
@endsection

