@extends('layouts.landing')
@section('htmlheader_title', ucfirst($halaman->halaman))
@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<div class="box-title text-capitalize">
							{!! $halaman->halaman !!}
						</div>
					</div>
					<div class="box-body">
						{!! $halaman->konten !!}
					</div>
					<div class="box-footer">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<style type="text/css">
		/* */

	</style>
@endsection

