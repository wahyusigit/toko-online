@extends('layouts.landing')
@section('htmlheader_title', 'Complete Checkout')
@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-12">
                @include('flash::message')
            </div>
		</div>
		<section class="invoice">
			<div class="row">
		        <div class="col-xs-12 table-responsive">
					@include('partials.table_cart')
				</div>
			<!-- /.col -->
			</div>
			<hr>
			<div class="row">
                <div class="col-sm-6 col-md-offset-1">
                    <form method="POST" action="{{ route('postCompleteCheckout') }}">
                    	{{ csrf_field() }}
                        <input type="hidden" name="id_pesan">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="" onchange="alamatSama(this)">
                                Alamat Pengiriman Sama
                            </label>
                        </div>
                        <div class="form-group">
                            <label>Alamat tujuan</label>
                            <textarea id="tujuan" name="tujuan" class="form-control" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Kota tujuan</label>
                            <input type="text" id="kota" name="kota" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Provinsi</label>
                            <input type="text" id="provinsi" name="provinsi" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Kode POS</label>
                            <input type="text" id="kode_pos" name="kode_pos" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Telepon</label>
                            <input type="text" id="telepon" name="telepon" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Jarak kirim</label>
                            <select name="id_jarak" class="form-control">
                            	@foreach($jaraks as $no => $jarak)
                            	<option class="text-capitalize" value="{{ $no+1 }}. {{ $jarak->id }}">{{ $jarak->tujuan }} - {{ $jarak->ongkos }}</option>
                            	@endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="keterangan_pesan" class="form-control" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-flat">Kembali</button>
                                <button type="button" class="btn btn-default btn-flat">Batal</button>
                                <button type="submit" class="btn btn-primary btn-flat">Proses</button>
                            </div>
                        </div>
                    </form>
                </div>
			</div>
		</section>
		<div class="row">
			
		</div>
	</div>

@endsection
@push('script')
<script type="text/javascript">
	$('body').on('click','#test', function(){
		$.ajax({
        	url: "{{ route('ajaxAlamatSama') }}", 
        	success: function(result){
        		console.log(result);
    		}
		});
	});
</script>

{{-- Untuk Alamat Pengiriman Sama Sesuai dengan Profil Customer --}}
<script type="text/javascript">
	function alamatSama(checkboxElem) {
		if (checkboxElem.checked) {
			$.ajax({
				url : '{{ route('ajaxAlamatSama') }}',
				success : function(response){
					$('#tujuan').val(response.alamat);
					$('#kota').val(response.kota);
					$('#provinsi').val(response.provinsi);
					$('#kode_pos').val(response.kode_pos);
					$('#telepon').val(response.telepon);
				}
			});
		} else {
			$('#tujuan').val('');
			$('#kota').val('');
			$('#provinsi').val('');
			$('#kode_pos').val('');
			$('#telepon').val('');
		}
	}
</script>
@endpush
