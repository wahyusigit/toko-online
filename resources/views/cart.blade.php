@extends('layouts.landing')
@section('htmlheader_title', 'Keranjang')
@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-12">
                @include('flash::message')
            </div>
		</div>
		<section class="invoice">
			<div class="row">
		        <div class="col-xs-12 table-responsive">
					@include('partials.table_cart')
				</div>
			<!-- /.col -->
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-12">
					<div class="btn-group pull-right">
						<a href="{{ route('indexHomepage') }}" class="btn btn-default btn-flat"><i class="fa fa-shopping-cart"></i>  Beli lagi</a>
						<a href="{{ route('destroyCart') }}" class="btn btn-default btn-flat"><i class="fa fa-times"></i>  Batal</a>
						<a href="{{ route('checkout') }}" class="btn btn-default btn-flat"><i class="fa fa-check-circle-o"></i>  Selesai</a>
                    </div>
				</div>
			</div>

		</section>
	</div>
@endsection

