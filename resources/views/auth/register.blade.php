<!DOCTYPE html>
<html>
  @include('layouts.partials.htmlheader')
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
  <body class="hold-transition register-page">
    <div class="register-box" style="width: 900px">
      <div class="register-logo">
        <a href="{{ route('indexHomepage') }}"><b>Magjoss</a>
      </div>
      <div class="register-box-body">
        <form action="{{ route('register') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" name="name" type="text" class="form-control" value="{{ old('name') }}" placeholder="Nama lengkap">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="E-Mail">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Ulangi password" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <textarea id="alamat" name="alamat" class="form-control" rows="3" placeholder="Alamat">{{ old('alamat') }}</textarea>
                    @if ($errors->has('alamat'))
                        <span class="help-block">
                            <strong>{{ $errors->first('alamat') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="kota" name="kota" type="text" class="form-control" value="{{ old('kota') }}" placeholder="Kota">
                    @if ($errors->has('kota'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kota') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="provinsi" name="provinsi" type="text" class="form-control" value="{{ old('provinsi') }}" placeholder="Provinsi">
                    @if ($errors->has('provinsi'))
                        <span class="help-block">
                            <strong>{{ $errors->first('provinsi') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="kode_pos" name="kode_pos" type="text" class="form-control" value="{{ old('kode_pos') }}" placeholder="Kode POS">
                    @if ($errors->has('kode_pos'))
                        <span class="help-block">
                            <strong>{{ $errors->first('kode_pos') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="telepon" name="telepon" type="text" class="form-control" value="{{ old('telepon') }}" placeholder="Telepon">
                    @if ($errors->has('telepon'))
                        <span class="help-block">
                            <strong>{{ $errors->first('telepon') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-flat pull-right">Register</button>
            </div>
        </div>
    </form>
      </div>
    </div>
    @include('layouts.partials.scripts')
  </body>
</html>