<!DOCTYPE html>
<html>
  @include('layouts.partials.htmlheader')
  <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{ route('indexHomepage') }}">Magjoss</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <form action="{{ route('login') }}" method="POST">
        {{ csrf_field() }}
      <div class="form-group has-feedback">
        <input name="email" type="email" class="form-control" placeholder="Email">
        @if ($errors->has('email'))
          <span class="help-block">
              <strong class="text-red">{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        @if ($errors->has('password'))
          <span class="help-block">
              <strong class="text-red">{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-8">
          <p>Belum punya Akun ? <a href="{{ route('register') }}"" class="">Daftar</a></p>
        </div>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
        </div>
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
@include('layouts.partials.scripts')
</body>
</html>