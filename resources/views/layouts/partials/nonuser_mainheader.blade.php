<header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="{{ route('indexHomepage') }}" class="navbar-brand">Magjoss</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            {{-- <li><a href="{{ route('indexHomepage') }}"><i class="fa fa-home"></i>  Beranda</a></li> --}}
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-list"></i>  Kategori <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                @foreach($produk_jenis as $jenis)
                @if($jenis->produk->isEmpty())
                @else
                <li><a href="{{ route('indexProdukCategoryHomepage', $jenis->id_jenis) }}"><i class="fa fa-circle-thin"></i>   {{ $jenis->nama_jenis }}</a></li>
                @endif
                @endforeach
              </ul>
            </li>
            <li><a href="{{ route('tentangKami') }}"><i class="fa fa-user"></i>   Tentang Kami</a></li>
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-info-circle"></i>   Bantuan <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('caraPemesanan') }}"><i class="fa fa-circle-thin"></i>   Cara Pemesanan</a></li>
                <li><a href="{{ route('caraPembayaran') }}"><i class="fa fa-circle-thin"></i>   Cara Pembayaran</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('hubungiKami') }}"><i class="fa fa-circle-thin"></i>   Hubungi Kami</a></li>
              </ul>
            </li>
            
          </ul>
        </div>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li><a href="{{ route('login') }}"><i class="fa fa-id-card-o"></i>   Akun Saya</a></li>
            <li><a href="{{ route('indexCart') }}"><i class="fa fa-shopping-cart fa-lg"></i>   <b> {{ Cart::count() == 0 ? '' : '(' . Cart::count() . ')'}}</b>   Keranjang</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
</header>