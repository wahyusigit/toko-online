<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            {{-- <li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{ route('indexAdmin') }}"><i class='fa fa-link'></i> <span> Home </span></a></li> --}}
            <li class="{{ Request::is('admin/slider*') ? 'active' : '' }}"><a href="{{ route('indexSliderAdmin') }}"><i class='fa fa-sliders'></i> <span> Slider </span></a></li>
            <li class="{{ Request::is('admin/halaman*') ? 'active' : '' }}"><a href="{{ route('indexHalamanAdmin') }}"><i class='fa fa-link'></i> <span> Halaman </span></a></li>
            <li class="{{ Request::is('admin/data/admin*') ? 'active' : '' }}"><a href="{{ route('indexUserAdmin','admin') }}"><i class='fa fa-user-circle'></i> <span> Admin </span></a></li>
            <li class="{{ Request::is('admin/data/toko*') ? 'active' : '' }}"><a href="{{ route('indexUserAdmin','toko') }}"><i class='fa fa-building-o'></i> <span>Toko </span></a></li>
            <li class="{{ Request::is('admin/data/customer*') ? 'active' : '' }}"><a href="{{ route('indexUserAdmin','customer') }}"><i class='fa fa-user-o'></i> <span> Customer</span></a></li>
            <li class="{{ Request::is('admin/produk*') ? 'active' : '' }}"><a href="{{ route('indexProdukAdmin') }}"><i class='fa fa-product-hunt'></i> <span> Produk </span></a></li>
            <li class="{{ Request::is('admin/jarak*') ? 'active' : '' }}"><a href="{{ route('indexJarakAdmin') }}"><i class='fa fa-map-marker '></i> <span> Jarak </span></a></li>
            <li class="{{ Request::is('admin/pesan*') ? 'active' : '' }}"><a href="{{ route('indexPesanAdmin') }}"><i class='fa fa-envelope'></i> <span> Pesan </span></a></li>
            <li class="{{ Request::is('admin/testimoni*') ? 'active' : '' }}"><a href="{{ route('indexTestimoniAdmin') }}"><i class='fa fa-address-book'></i> <span> Testimoni </span></a></li>
            <li><a href="{{ route('logout') }}"><i class='fa fa-sign-out'></i> <span> Logout </span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
