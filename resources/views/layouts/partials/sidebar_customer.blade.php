<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            {{-- <li><a href="{{ route('indexHomepage') }}"><i class='fa fa-link'></i> <span> Beranda </span></a></li> --}}
            {{-- <li class="{{ Request::is('customer') ? 'active' : '' }}"><a href="{{ route('indexCustomer') }}"><i class='fa fa-home'></i> <span> Home </span></a></li> --}}
            <li class="{{ Request::is('customer/profile*') ? 'active' : '' }}"><a href="{{ route('profileCustomer') }}"><i class='fa fa-user'></i> <span> Profil </span></a></li>
            <li class="{{ Request::is('customer/pesanan*') ? 'active' : '' }}"><a href="{{ route('indexPesananCustomer') }}"><i class='fa fa-link'></i> <span> Pesanan </span></a></li>
            <li class="{{ Request::is('customer/pengiriman*') ? 'active' : '' }}"><a href="{{ route('indexPengirimanCustomer') }}"><i class='fa fa-truck'></i> <span> Pengiriman </span></a></li>
            <li class="{{ Request::is('customer/testimoni*') ? 'active' : '' }}"><a href="{{ route('indexTestimoniCustomer') }}"><i class='fa fa-quote-left'></i> <span> Testimoni </span></a></li>
            <li><a href="{{ route('logout') }}"><i class='fa fa-sign-out'></i> <span> Logout </span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
