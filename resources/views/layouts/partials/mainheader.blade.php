<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    @if(Auth::user()->hasRole('admin'))
    <a href="{{ route('indexAdmin') }}" class="logo">
    @elseif(Auth::user()->hasRole('toko'))
    <a href="{{ route('indexToko') }}" class="logo">
    @elseif(Auth::user()->hasRole('customer'))      
    <a href="{{ route('indexCustomer') }}" class="logo">
    @endif

      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>M</b>J</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Magjoss</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
    </nav>
</header>
