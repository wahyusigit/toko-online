<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            {{-- <li class="{{ Request::is('toko') ? 'active' : '' }}"><a href="{{ route('indexToko') }}"><i class='fa fa-link'></i> <span> Home </span></a></li> --}}
            <li class="{{ Request::is('toko/profile') ? 'active' : '' }}"><a href="{{ route('profileToko') }}"><i class='fa fa-sliders'></i> <span> Profil </span></a></li>
            <li class="{{ Request::is('toko/produk') ? 'active' : '' }}"><a href="{{ route('indexProdukToko') }}"><i class='fa fa-link'></i> <span> Produk </span></a></li>
            <li class="{{ Request::is('toko/pesanan') ? 'active' : '' }}"><a href="{{ route('indexPesananToko') }}"><i class='fa fa-link'></i> <span> Pemesanan </span></a></li>
            {{-- <li class="{{ Request::is('toko/konfirmasi') ? 'active' : '' }}"><a href="{{ route('indexKonfirmasiToko') }}"><i class='fa fa-user-circle'></i> <span> Konfirmasi Bayar </span></a></li> --}}
            <li class="{{ Request::is('toko/pengiriman') ? 'active' : '' }}"><a href="{{ route('indexPengirimanToko') }}"><i class='fa fa-user-circle'></i> <span> Pengiriman </span></a></li>
            <li><a href="{{ route('logout') }}"><i class='fa fa-sign-out'></i> <span> Logout </span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
