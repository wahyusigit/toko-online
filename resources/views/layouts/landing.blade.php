<!DOCTYPE html>
<html>
@include('layouts.partials.htmlheader')
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  @include('layouts.partials.nonuser_mainheader')
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Main content -->
      <section class="content">
        @yield('main-content')
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  @include('layouts.partials.nonuser_footer')
</div>
<!-- ./wrapper -->

@include('layouts.partials.scripts')
</html>