@extends('layouts.landing')

@section('main-content')
	<div class="container spark-screen">
		<div class="row">
			<div class="col-md-12">
                @include('flash::message')
            </div>
		</div>
		<section class="invoice">
			<div class="row">
		        <div class="col-xs-12 table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="col-md-4">Nama Produk</th>
								<th class="text-center">Berat (Gram)</th>
								<th class="text-center col-md-1">Qty</th>
								<th class="text-right col-md-2">Harga</th>
								<th class="text-right col-md-2">Subtotal</th>
								<th class="text-center col-md-1">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($carts as $no => $cart)
							<tr>
								<td class="text-center">{{ $no+1 }}</td>
								<td class="text-left text-capitalize">{{ $cart->name }}</td>
								<td class="text-center">{{ $cart->options->berat_prod }}</td>
								<td class="text-center">{{ $cart->qty }}</td>
								<td class="text-right">{{ $cart->price }}</td>
								<td class="text-right">{{ $cart->subtotal }}</td>
								<td class="text-center">
									<a href="{{ route('removeCart', $cart->rowId) }}" class="btn btn-danger btn-flat btn-sm"><i class="fa fa-trash"></i></a>
								</td>

							</tr>
							@endforeach
							<tr>
								<td colspan="4"></td>
								<td class="text-right"><b>Total Pembelian</b></td>
								<td class="text-right"><b>{{ Cart::total() }}</b></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			<!-- /.col -->
			</div>
			<hr>
			<div class="row">
				<div class="col-md-6">
					<legend>Sudah Punya Akun ?</legend>
					<form class="form-horizontal" method="POST" action="{{ route('login') }}">
                	   {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-flat">
                                    Login
                                </button>
                            </div>
                        </div>
                    </form>
				</div>
				<div class="col-md-6">
					<legend>Belum Punya Akun ?</legend>
					<form class="form-horizontal" method="POST" action="{{ route('register') }}">
                	{{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
		</section>
		<div class="row">
			
		</div>
	</div>
@endsection

