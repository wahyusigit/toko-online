@extends('layouts.errors')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.pagenotfound') }}
@endsection

@section('main-content')

    <div class="error-page">
        <h2 class="headline text-yellow"> 401</h2>
        <div class="error-content">
            <h1>Maaf, Anda Tidak mempunyai ijin untuk membuka Halaman ini.<br>Silahkan hubungi Admin Kami. Terimakasih.</h1>
            <h3><i class="fa fa-warning text-yellow"></i> Oops! {{ trans('adminlte_lang::message.pagenotfound') }}.</h3>
            <p>
                
            </p>
        </div><!-- /.error-content -->
    </div><!-- /.error-page -->
@endsection