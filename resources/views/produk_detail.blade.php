@extends('layouts.landing')
@section('htmlheader_title', ucfirst($produk->nama_prod))
@section('main-content')
	<div class="container spark-screen">
		<div class="row" style="margin-bottom: 30px">
			<div class="col-md-4">
				<img src="{{ asset($produk->image_prod) }}" class="img-thumbnail">
				<hr>
				<div class="box">
					<div class="box-body">
						<p class="text-capitalize"><b>Nama Toko : {{ $produk->toko->name }}</b></p>
						<p>Deskripsi Toko : {{ $produk->toko->deskripsi_toko }}</p>

					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="box">
					<div class="box-body">
						<div class="row" style="margin-bottom: 20px">
							<div class="col-md-12">
								<label>Nama Produk</label>
								<hr>
								<h4>{{ $produk->nama_prod }}</h4>
							</div>
						</div>

						<div class="row" style="margin-bottom: 20px">
							<div class="col-md-12">
								<label>Harga</label>
								<hr>
								<h3 style="margin:0px"><b class="text-red">Rp. {{ number_format($produk->harga_prod,0,",",".") }},-</b></h3>
							</div>
						</div>
						<div class="row" style="margin-bottom: 20px">
							<div class="col-md-6">
								<label>Rating</label>
								<div class="rating hidden-sm">
		                            @for($i=1;$i<=5;$i++)
		                            	@if($i <= $produk->rating_prod)
		                                <i class="price-text-color fa fa-star"></i>
		                                @else
		                                <i class="fa fa-star"></i>
		                                @endif
		                            @endfor
		                        </div>
	                    	</div>
	                        <div class="col-md-6">
	                        	<a href="{{ route('addCart', $produk->id_produk) }}" class="btn btn-lg btn-success btn-flat pull-right"><i class="fa fa-shopping-cart"></i>   Beli</a>
	                        </div>
	                    </div>
					</div>
				</div>

				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab_1" data-toggle="tab">Deskripsi Produk</a></li>
						<li><a href="#tab_2" data-toggle="tab">Ulasan</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_1">
							{{ $produk->keterangan_prod }}
						</div>

						<div class="tab-pane" id="tab_2">
							@foreach($ulasans as $ulasan)
							<div class="post">
								<div class="user-block">
									<span class="username text-capitalize" style="margin-left: 0px">{{ $ulasan->customer->name }}</span>
									<span class="description" style="margin-left: 0px">
										<div class="rating hidden-sm">
				                            @for($i=1;$i<=5;$i++)
				                            	@if($i <= $ulasan->tingkat_kepuasan)
				                                <i class="price-text-color fa fa-star"></i>
				                                @else
				                                <i class="fa fa-star"></i>
				                                @endif
				                            @endfor
				                            - - {{ date_format(date_create($ulasan->tanggal), "d F Y") }}
				                        </div>
		                        	</span>
								</div>
								<p>{{ $ulasan->ulasan }}</p>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-md-offset-4">
				<div class="box">
					<div class="box-header">
						<div class="box-title">
							Rekomendasi Produk
						</div>
					</div>
					<div class="box-body">
						@foreach($rekomendasi as $rek)
						<div class="col-md-3">
							<a href="{{ route('indexProdukDetailHomepage', $rek->id_produk) }}">
							<img src="{{ asset($rek->image_prod) }}" class="img-thumbnail">
							<h5>{{ $rek->nama_prod }}</h5>
							<h5 class="text-red">Rp. {{ number_format($rek->harga_prod,0,",",".") }}</h5>
							</a>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	<style type="text/css">
		.price-text-color
		{
		    color: #219FD1;
		}

		.col-item .rating
		{
		    /*width: 50%;*/
		    float: left;
		    font-size: 17px;
		    text-align: right;
		    line-height: 52px;
		    margin-bottom: 10px;
		    height: 52px;
		}

	</style>

@endsection

