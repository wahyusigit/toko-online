@extends('layouts.landing')
@section('htmlheader_title', 'Selamat Datang')
@section('main-content')
	<div class="container spark-screen">
		@if($sliders->isEmpty())
		@else
		<div class="row center-block">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					@foreach($sliders as $no => $slider)
					<li data-target="#myCarousel" data-slide-to="{{ $no }}" class="{{  $no == 0 ? 'active' : ''}}"></li>
					@endforeach
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					@foreach($sliders as $no => $slider)
					<div class="item {{  $no == 0 ? 'active' : ''}}">
					  <img class="center-block" src="{{ asset($slider->image_slider) }}" alt="Chania">
					  <div class="carousel-caption">
					    <h3>{{ $slider->judul_slider }}</h3>
					    <p>{{ $slider->deskripsi_slider }}</p>
					    <p><a href="{{ route('addCart', $slider->id_produk) }}" class="btn btn-success btn-flat"><i class="fa fa-shopping-cart"></i> Beli</a></p>
					  </div>
					</div>
					@endforeach

				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<hr>
		@endif
		@if($produk_jenis->isEmpty())
		{{dd('as')}}
		@else
        <div class="row" style="margin: 20px 0px;padding: 10px 0px;border-top: 1px solid #ddd">
        	<div class="col-md-12">
        		@foreach($produk_jenis as $jenis)
        		@if($jenis->produk->isEmpty())
        		@else
				<div class="row">
					<div class="box">
						<div class="box-header with-border">
							<div class="box-title">Produk {{ $jenis->nama_jenis }}</div>
							<div class="box-tools pull-right">
			                    <a href="{{ route('indexProdukCategoryHomepage', $jenis->id_jenis) }}" class="btn btn-default btn-flat btn-block btn-sm"><i class="fa fa-list"></i> Lihat Semua ({{ $jenis->produk->count() }})</a>
			                </div>
						</div>
						<div class="box-body">
							@foreach($jenis->produk->slice(0,4) as $produk)
							<div class="col-md-3">
				                @include('partials.produk_item')
				            </div>
				        	@endforeach
			        	</div>
			        </div>
				</div>
				@endif
	        	@endforeach
			</div>
        </div>
        @endif
    </div>

<style type="text/css">
.col-item
{
    border: 1px solid #E1E1E1;
    border-radius: 5px;
    background: #FFF;
    margin-top: 10px;
    margin-bottom: 10px;
}
.col-item .photo img
{
    margin: 0 auto;
    width: 100%;
}

.col-item .info
{
    padding: 10px;
    border-radius: 0 0 5px 5px;
    margin-top: 1px;
}

.col-item:hover .info {
    background-color: #F5F5DC;
}
.col-item .price
{
    /*width: 50%;*/
    float: left;
    margin-top: 5px;
}

.col-item .price h5
{
    line-height: 20px;
    margin: 0;
}

.price-text-color
{
    color: #219FD1;
}

.col-item .info .rating
{
    color: #777;
}

.col-item .rating
{
    /*width: 50%;*/
    float: left;
    font-size: 17px;
    text-align: right;
    line-height: 52px;
    margin-bottom: 10px;
    height: 52px;
}

.col-item .separator
{
    border-top: 1px solid #E1E1E1;
}

.clear-left
{
    clear: left;
}

.col-item .separator p
{
    line-height: 20px;
    margin-bottom: 0;
    margin-top: 10px;
    text-align: center;
}

.col-item .separator p i
{
    margin-right: 5px;
}
.col-item .btn-add
{
    width: 50%;
    float: left;
}

.col-item .btn-add
{
    border-right: 1px solid #E1E1E1;
}

.col-item .btn-details
{
    width: 50%;
    float: left;
    padding-left: 10px;
}
.controls
{
    margin-top: 20px;
}
[data-slide="prev"]
{
    margin-right: 10px;
}
		

</style>
@endsection

